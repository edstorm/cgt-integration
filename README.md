# README #

PPS Integration Library

### What is this repository for? ###

* This set of maven modules builds libraries for integration with the PPS data available.
* Version 1.0.0-SNAPSHOT


### How do I get set up? ###

* Fork the repository
* Clone a local copy 
* Import project into eclipse.  You may need to perform individual maven project imports for pps-data-api, pps-data, and pps-mq as well.

### Contribution guidelines ###
