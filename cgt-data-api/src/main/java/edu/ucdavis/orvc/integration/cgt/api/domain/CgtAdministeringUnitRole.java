package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtAdministeringUnitRoleDTO;

public interface CgtAdministeringUnitRole extends CgtBaseInterface<CgtAdministeringUnitRoleDTO> {

	int getAurUnitRoleKey();

	void setAurUnitRoleKey(int aurUnitRoleKey);

	String getAurName();

	void setAurName(String aurName);

	String getAurShortName();

	void setAurShortName(String aurShortName);

	Integer getAurSortOrder();

	void setAurSortOrder(Integer aurSortOrder);

	boolean isAurIsActive();

	void setAurIsActive(boolean aurIsActive);

	String getAurUpdatedBy();

	void setAurUpdatedBy(String aurUpdatedBy);

	LocalDateTime getAurDateCreated();

	void setAurDateCreated(LocalDateTime aurDateCreated);

	LocalDateTime getAurDateUpdated();

	void setAurDateUpdated(LocalDateTime aurDateUpdated);

}