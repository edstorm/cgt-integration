package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtTransactionSubmissionTypeDTO;

public interface CgtTransactionSubmissionType extends CgtBaseInterface<CgtTransactionSubmissionTypeDTO> {

	CgtTransactionSubmissionTypeId getId();

	void setId(CgtTransactionSubmissionTypeId id);

	String getTstName();

	void setTstName(String tstName);

	String getTstShortName();

	void setTstShortName(String tstShortName);

	Integer getTstSortOrder();

	void setTstSortOrder(Integer tstSortOrder);

	Boolean getTstIsVisible();

	void setTstIsVisible(Boolean tstIsVisible);

	Boolean getTstIsClosingType();

	void setTstIsClosingType(Boolean tstIsClosingType);

	Boolean getTstIsAutoCloseType();

	void setTstIsAutoCloseType(Boolean tstIsAutoCloseType);

	Boolean getTstIsEmailTriggerType();

	void setTstIsEmailTriggerType(Boolean tstIsEmailTriggerType);

	Boolean getTstIsSurveyTriggerType();

	void setTstIsSurveyTriggerType(Boolean tstIsSurveyTriggerType);

	boolean isTstValidateTransaction();

	void setTstValidateTransaction(boolean tstValidateTransaction);

	boolean isTstIsActive();

	void setTstIsActive(boolean tstIsActive);

	String getTstUpdatedBy();

	void setTstUpdatedBy(String tstUpdatedBy);

	LocalDateTime getTstDateUpdated();

	void setTstDateUpdated(LocalDateTime tstDateUpdated);

	LocalDateTime getTstDateCreated();

	void setTstDateCreated(LocalDateTime tstDateCreated);
	
	CgtTransactionSubmissionTypeDTO toDTO();

}