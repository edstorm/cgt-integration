package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtTransactionsKeyPersonnelsDTO;

public interface CgtTransactionsKeyPersonnels extends CgtBaseInterface<CgtTransactionsKeyPersonnelsDTO> { 

	int getTkpTransactionKeyPersonnelKey();

	void setTkpTransactionKeyPersonnelKey(int tkpTransactionKeyPersonnelKey);

	int getTpkTransactionKey();

	void setTpkTransactionKey(int tpkTransactionKey);

	int getTpkKeyPersonnelKey();

	void setTpkKeyPersonnelKey(int tpkKeyPersonnelKey);

	long getTpkAllocation();

	void setTpkAllocation(long tpkAllocation);

	boolean isTpkIsActive();

	void setTpkIsActive(boolean tpkIsActive);

	String getTpkUpdatedBy();

	void setTpkUpdatedBy(String tpkUpdatedBy);

	LocalDateTime getTpkDateCreated();

	void setTpkDateCreated(LocalDateTime tpkDateCreated);

	LocalDateTime getTpkDateUpdated();

	void setTpkDateUpdated(LocalDateTime tpkDateUpdated);

}