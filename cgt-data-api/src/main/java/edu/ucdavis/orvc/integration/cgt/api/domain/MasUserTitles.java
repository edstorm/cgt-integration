package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.MasUserTitlesDTO;

public interface MasUserTitles extends CgtBaseInterface<MasUserTitlesDTO> { 

	String getUstUserTitleKey();

	void setUstUserTitleKey(String ustUserTitleKey);

	String getUstName();

	void setUstName(String ustName);

	String getUstDataSourceKey();

	void setUstDataSourceKey(String ustDataSourceKey);

	boolean isUstIsActive();

	void setUstIsActive(boolean ustIsActive);

	LocalDateTime getUstDateCreated();

	void setUstDateCreated(LocalDateTime ustDateCreated);

	LocalDateTime getUstDateUpdated();

	void setUstDateUpdated(LocalDateTime ustDateUpdated);

	String getUstUpdatedBy();

	void setUstUpdatedBy(String ustUpdatedBy);

}