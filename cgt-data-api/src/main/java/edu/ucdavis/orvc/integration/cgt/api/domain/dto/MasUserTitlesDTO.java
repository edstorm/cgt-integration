package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.MasUserTitles;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class MasUserTitlesDTO implements MasUserTitles {

	private static final long serialVersionUID = -6829758701505929440L;
	private String ustUserTitleKey;
	private String ustName;
	private String ustDataSourceKey;
	private boolean ustIsActive;
	private LocalDateTime ustDateCreated;
	private LocalDateTime ustDateUpdated;
	private String ustUpdatedBy;

	public MasUserTitlesDTO(final MasUserTitles fromObj) {



	}

	public MasUserTitlesDTO() {
	}

	public MasUserTitlesDTO(final String ustUserTitleKey, final String ustName, final boolean ustIsActive, final LocalDateTime ustDateCreated,
			final LocalDateTime ustDateUpdated, final String ustUpdatedBy) {
		this.ustUserTitleKey = ustUserTitleKey;
		this.ustName = ustName;
		this.ustIsActive = ustIsActive;
		this.ustDateCreated = ustDateCreated;
		this.ustDateUpdated = ustDateUpdated;
		this.ustUpdatedBy = ustUpdatedBy;
	}

	public MasUserTitlesDTO(final String ustUserTitleKey, final String ustName, final String ustDataSourceKey, final boolean ustIsActive,
			final LocalDateTime ustDateCreated, final LocalDateTime ustDateUpdated, final String ustUpdatedBy) {
		this.ustUserTitleKey = ustUserTitleKey;
		this.ustName = ustName;
		this.ustDataSourceKey = ustDataSourceKey;
		this.ustIsActive = ustIsActive;
		this.ustDateCreated = ustDateCreated;
		this.ustDateUpdated = ustDateUpdated;
		this.ustUpdatedBy = ustUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserTitles#getUstUserTitleKey()
	 */
	@Override
	

	
	public String getUstUserTitleKey() {
		return this.ustUserTitleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserTitles#setUstUserTitleKey(java.lang.String)
	 */
	@Override
	public void setUstUserTitleKey(final String ustUserTitleKey) {
		this.ustUserTitleKey = ustUserTitleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserTitles#getUstName()
	 */
	@Override
	
	public String getUstName() {
		return this.ustName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserTitles#setUstName(java.lang.String)
	 */
	@Override
	public void setUstName(final String ustName) {
		this.ustName = ustName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserTitles#getUstDataSourceKey()
	 */
	@Override
	
	public String getUstDataSourceKey() {
		return this.ustDataSourceKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserTitles#setUstDataSourceKey(java.lang.String)
	 */
	@Override
	public void setUstDataSourceKey(final String ustDataSourceKey) {
		this.ustDataSourceKey = ustDataSourceKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserTitles#isUstIsActive()
	 */
	@Override
	
	public boolean isUstIsActive() {
		return this.ustIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserTitles#setUstIsActive(boolean)
	 */
	@Override
	public void setUstIsActive(final boolean ustIsActive) {
		this.ustIsActive = ustIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserTitles#getUstDateCreated()
	 */
	@Override
	
	
	public LocalDateTime getUstDateCreated() {
		return this.ustDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserTitles#setUstDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setUstDateCreated(final LocalDateTime ustDateCreated) {
		this.ustDateCreated = ustDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserTitles#getUstDateUpdated()
	 */
	@Override
	
	
	public LocalDateTime getUstDateUpdated() {
		return this.ustDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserTitles#setUstDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setUstDateUpdated(final LocalDateTime ustDateUpdated) {
		this.ustDateUpdated = ustDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserTitles#getUstUpdatedBy()
	 */
	@Override
	
	public String getUstUpdatedBy() {
		return this.ustUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserTitles#setUstUpdatedBy(java.lang.String)
	 */
	@Override
	public void setUstUpdatedBy(final String ustUpdatedBy) {
		this.ustUpdatedBy = ustUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((ustDataSourceKey == null) ? 0 : ustDataSourceKey.hashCode());
		result = prime * result + ((ustDateCreated == null) ? 0 : ustDateCreated.hashCode());
		result = prime * result + ((ustDateUpdated == null) ? 0 : ustDateUpdated.hashCode());
		result = prime * result + (ustIsActive ? 1231 : 1237);
		result = prime * result + ((ustName == null) ? 0 : ustName.hashCode());
		result = prime * result + ((ustUpdatedBy == null) ? 0 : ustUpdatedBy.hashCode());
		result = prime * result + ((ustUserTitleKey == null) ? 0 : ustUserTitleKey.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof MasUserTitlesDTO))
			return false;
		final MasUserTitlesDTO other = (MasUserTitlesDTO) obj;
		if (ustDataSourceKey == null) {
			if (other.ustDataSourceKey != null)
				return false;
		} else if (!ustDataSourceKey.equals(other.ustDataSourceKey))
			return false;
		if (ustDateCreated == null) {
			if (other.ustDateCreated != null)
				return false;
		} else if (!ustDateCreated.equals(other.ustDateCreated))
			return false;
		if (ustDateUpdated == null) {
			if (other.ustDateUpdated != null)
				return false;
		} else if (!ustDateUpdated.equals(other.ustDateUpdated))
			return false;
		if (ustIsActive != other.ustIsActive)
			return false;
		if (ustName == null) {
			if (other.ustName != null)
				return false;
		} else if (!ustName.equals(other.ustName))
			return false;
		if (ustUpdatedBy == null) {
			if (other.ustUpdatedBy != null)
				return false;
		} else if (!ustUpdatedBy.equals(other.ustUpdatedBy))
			return false;
		if (ustUserTitleKey == null) {
			if (other.ustUserTitleKey != null)
				return false;
		} else if (!ustUserTitleKey.equals(other.ustUserTitleKey))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"MasUserTitles [ustUserTitleKey=%s, ustName=%s, ustDataSourceKey=%s, ustIsActive=%s, ustDateCreated=%s, ustDateUpdated=%s, ustUpdatedBy=%s]",
				ustUserTitleKey, ustName, ustDataSourceKey, ustIsActive, ustDateCreated, ustDateUpdated, ustUpdatedBy);
	}

	public MasUserTitlesDTO toDTO() {
		return new MasUserTitlesDTO(this);
	}
}
