package edu.ucdavis.orvc.integration.cgt.api.domain;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtEDocsMessagesToFilesDTO;

public interface CgtEDocsMessagesToFiles extends CgtBaseInterface<CgtEDocsMessagesToFilesDTO> { 

	CgtEDocsMessagesToFilesId getId();

	void setId(CgtEDocsMessagesToFilesId id);

}