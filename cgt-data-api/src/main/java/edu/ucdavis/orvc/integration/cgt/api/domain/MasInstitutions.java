package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.MasInstitutionsDTO;

public interface MasInstitutions extends CgtBaseInterface<MasInstitutionsDTO> { 

	String getInsInstitutionKey();

	void setInsInstitutionKey(String insInstitutionKey);

	String getInsName();

	void setInsName(String insName);

	String getInsShortName();

	void setInsShortName(String insShortName);

	String getInsStateKey();

	void setInsStateKey(String insStateKey);

	String getInsZipCode();

	void setInsZipCode(String insZipCode);

	String getInsCity();

	void setInsCity(String insCity);

	boolean isInsIsActive();

	void setInsIsActive(boolean insIsActive);

	String getInsDataSourceKey();

	void setInsDataSourceKey(String insDataSourceKey);

	LocalDateTime getInsDateCreated();

	void setInsDateCreated(LocalDateTime insDateCreated);

	LocalDateTime getInsDateUpdated();

	void setInsDateUpdated(LocalDateTime insDateUpdated);

	String getInsUpdatedBy();

	void setInsUpdatedBy(String insUpdatedBy);

	Boolean getInsIsopss();

	void setInsIsopss(Boolean insIsopss);

}