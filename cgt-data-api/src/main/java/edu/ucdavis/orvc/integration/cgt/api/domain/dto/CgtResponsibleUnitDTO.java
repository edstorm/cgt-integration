package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtResponsibleUnit;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtResponsibleUnitDTO implements CgtResponsibleUnit {

	private static final long serialVersionUID = -4214253436961812176L;
	private String reuResponsibleUnitKey;
	private String reuName;
	private String reuShortName;
	private boolean reuIsActive;
	private String reuUpdatedBy;
	private LocalDateTime reuDateCreated;
	private LocalDateTime reuDateUpdated;

	public CgtResponsibleUnitDTO(final CgtResponsibleUnit fromObj) {



	}

	public CgtResponsibleUnitDTO() {
	}

	public CgtResponsibleUnitDTO(final String reuResponsibleUnitKey, final String reuName, final boolean reuIsActive, final String reuUpdatedBy,
			final LocalDateTime reuDateCreated, final LocalDateTime reuDateUpdated) {
		this.reuResponsibleUnitKey = reuResponsibleUnitKey;
		this.reuName = reuName;
		this.reuIsActive = reuIsActive;
		this.reuUpdatedBy = reuUpdatedBy;
		this.reuDateCreated = reuDateCreated;
		this.reuDateUpdated = reuDateUpdated;
	}

	public CgtResponsibleUnitDTO(final String reuResponsibleUnitKey, final String reuName, final String reuShortName, final boolean reuIsActive,
			final String reuUpdatedBy, final LocalDateTime reuDateCreated, final LocalDateTime reuDateUpdated) {
		this.reuResponsibleUnitKey = reuResponsibleUnitKey;
		this.reuName = reuName;
		this.reuShortName = reuShortName;
		this.reuIsActive = reuIsActive;
		this.reuUpdatedBy = reuUpdatedBy;
		this.reuDateCreated = reuDateCreated;
		this.reuDateUpdated = reuDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtResponsibleUnit#getReuResponsibleUnitKey()
	 */
	@Override
	

	
	public String getReuResponsibleUnitKey() {
		return this.reuResponsibleUnitKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtResponsibleUnit#setReuResponsibleUnitKey(java.lang.String)
	 */
	@Override
	public void setReuResponsibleUnitKey(final String reuResponsibleUnitKey) {
		this.reuResponsibleUnitKey = reuResponsibleUnitKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtResponsibleUnit#getReuName()
	 */
	@Override
	
	public String getReuName() {
		return this.reuName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtResponsibleUnit#setReuName(java.lang.String)
	 */
	@Override
	public void setReuName(final String reuName) {
		this.reuName = reuName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtResponsibleUnit#getReuShortName()
	 */
	@Override
	
	public String getReuShortName() {
		return this.reuShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtResponsibleUnit#setReuShortName(java.lang.String)
	 */
	@Override
	public void setReuShortName(final String reuShortName) {
		this.reuShortName = reuShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtResponsibleUnit#isReuIsActive()
	 */
	@Override
	
	public boolean isReuIsActive() {
		return this.reuIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtResponsibleUnit#setReuIsActive(boolean)
	 */
	@Override
	public void setReuIsActive(final boolean reuIsActive) {
		this.reuIsActive = reuIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtResponsibleUnit#getReuUpdatedBy()
	 */
	@Override
	
	public String getReuUpdatedBy() {
		return this.reuUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtResponsibleUnit#setReuUpdatedBy(java.lang.String)
	 */
	@Override
	public void setReuUpdatedBy(final String reuUpdatedBy) {
		this.reuUpdatedBy = reuUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtResponsibleUnit#getReuDateCreated()
	 */
	@Override
	
	
	public LocalDateTime getReuDateCreated() {
		return this.reuDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtResponsibleUnit#setReuDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setReuDateCreated(final LocalDateTime reuDateCreated) {
		this.reuDateCreated = reuDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtResponsibleUnit#getReuDateUpdated()
	 */
	@Override
	
	
	public LocalDateTime getReuDateUpdated() {
		return this.reuDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtResponsibleUnit#setReuDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setReuDateUpdated(final LocalDateTime reuDateUpdated) {
		this.reuDateUpdated = reuDateUpdated;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((reuDateCreated == null) ? 0 : reuDateCreated.hashCode());
		result = prime * result + ((reuDateUpdated == null) ? 0 : reuDateUpdated.hashCode());
		result = prime * result + (reuIsActive ? 1231 : 1237);
		result = prime * result + ((reuName == null) ? 0 : reuName.hashCode());
		result = prime * result + ((reuResponsibleUnitKey == null) ? 0 : reuResponsibleUnitKey.hashCode());
		result = prime * result + ((reuShortName == null) ? 0 : reuShortName.hashCode());
		result = prime * result + ((reuUpdatedBy == null) ? 0 : reuUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtResponsibleUnitDTO)) {
			return false;
		}
		CgtResponsibleUnitDTO other = (CgtResponsibleUnitDTO) obj;
		if (reuDateCreated == null) {
			if (other.reuDateCreated != null) {
				return false;
			}
		} else if (!reuDateCreated.equals(other.reuDateCreated)) {
			return false;
		}
		if (reuDateUpdated == null) {
			if (other.reuDateUpdated != null) {
				return false;
			}
		} else if (!reuDateUpdated.equals(other.reuDateUpdated)) {
			return false;
		}
		if (reuIsActive != other.reuIsActive) {
			return false;
		}
		if (reuName == null) {
			if (other.reuName != null) {
				return false;
			}
		} else if (!reuName.equals(other.reuName)) {
			return false;
		}
		if (reuResponsibleUnitKey == null) {
			if (other.reuResponsibleUnitKey != null) {
				return false;
			}
		} else if (!reuResponsibleUnitKey.equals(other.reuResponsibleUnitKey)) {
			return false;
		}
		if (reuShortName == null) {
			if (other.reuShortName != null) {
				return false;
			}
		} else if (!reuShortName.equals(other.reuShortName)) {
			return false;
		}
		if (reuUpdatedBy == null) {
			if (other.reuUpdatedBy != null) {
				return false;
			}
		} else if (!reuUpdatedBy.equals(other.reuUpdatedBy)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtResponsibleUnit [reuResponsibleUnitKey=%s, reuName=%s, reuShortName=%s, reuIsActive=%s, reuUpdatedBy=%s, reuDateCreated=%s, reuDateUpdated=%s]",
				reuResponsibleUnitKey, reuName, reuShortName, reuIsActive, reuUpdatedBy, reuDateCreated,
				reuDateUpdated);
	}

	public CgtResponsibleUnitDTO toDTO() {
		return new CgtResponsibleUnitDTO(this);
	}
}
