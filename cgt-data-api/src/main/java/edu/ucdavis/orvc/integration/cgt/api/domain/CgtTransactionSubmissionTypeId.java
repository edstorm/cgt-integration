package edu.ucdavis.orvc.integration.cgt.api.domain;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtTransactionSubmissionTypeIdDTO;

public interface CgtTransactionSubmissionTypeId extends CgtBaseInterface<CgtTransactionSubmissionTypeIdDTO> {

	int getTstTransactionSubmissionTypeKey();

	void setTstTransactionSubmissionTypeKey(int tstTransactionSubmissionTypeKey);

	int getTstTransactionTypeKey();

	void setTstTransactionTypeKey(int tstTransactionTypeKey);
	
	CgtTransactionSubmissionTypeIdDTO toDTO();

}