package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtEDocsTypesDTO;

public interface CgtEDocsTypes extends CgtBaseInterface<CgtEDocsTypesDTO> { 

	int getEdtEDocsTypeKey();

	void setEdtEDocsTypeKey(int edtEDocsTypeKey);

	String getEdtName();

	void setEdtName(String edtName);

	boolean isEdtIsPhaseSpecific();

	void setEdtIsPhaseSpecific(boolean edtIsPhaseSpecific);

	boolean isEdtCaptureText();

	void setEdtCaptureText(boolean edtCaptureText);

	int getEdtSortOrder();

	void setEdtSortOrder(int edtSortOrder);

	boolean isEdtIsActive();

	void setEdtIsActive(boolean edtIsActive);

	LocalDateTime getEdtDateCreated();

	void setEdtDateCreated(LocalDateTime edtDateCreated);

	LocalDateTime getEdtDateUpdated();

	void setEdtDateUpdated(LocalDateTime edtDateUpdated);

	String getEdtUpdatedBy();

	void setEdtUpdatedBy(String edtUpdatedBy);

}