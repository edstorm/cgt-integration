package edu.ucdavis.orvc.integration.cgt.api.domain;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtTransactionGroupTypeMisspellingsDTO;

public interface CgtTransactionGroupTypeMisspellings extends CgtBaseInterface<CgtTransactionGroupTypeMisspellingsDTO> { 

	int getId();

	void setId(int id);

	int getTransactionTypeKey();

	void setTransactionTypeKey(int transactionTypeKey);

	String getMisspelling();

	void setMisspelling(String misspelling);

	int getNewTransactionGroupTypeKey();

	void setNewTransactionGroupTypeKey(int newTransactionGroupTypeKey);

	String getNewTransactionGroupName();

	void setNewTransactionGroupName(String newTransactionGroupName);

	Integer getNewTransactionGroupSequenceNumber();

	void setNewTransactionGroupSequenceNumber(Integer newTransactionGroupSequenceNumber);

	String getNewTransactionGroupSubContractorKey();

	void setNewTransactionGroupSubContractorKey(String newTransactionGroupSubContractorKey);

	boolean isNeedsReview();

	void setNeedsReview(boolean needsReview);

}