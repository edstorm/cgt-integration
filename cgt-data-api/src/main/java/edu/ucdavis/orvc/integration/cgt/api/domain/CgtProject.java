package edu.ucdavis.orvc.integration.cgt.api.domain;

import java.util.List;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtProjectDTO;

public interface CgtProject extends CgtBaseInterface<CgtProjectDTO> { 

	String getProProjectKey();

	void setProProjectKey(String proProjectKey);

	Integer getProProjectTypeKey();

	void setProProjectTypeKey(Integer proProjectTypeKey);

	String getProParentProjectKey();

	void setProParentProjectKey(String proParentProjectKey);

	LocalDateTime getProSponsorSubmissionDate();

	void setProSponsorSubmissionDate(LocalDateTime proSponsorSubmissionDate);

	LocalDateTime getProProcessingDueDate();

	void setProProcessingDueDate(LocalDateTime proProcessingDueDate);

	Integer getProProjectReceivedTypeKey();

	void setProProjectReceivedTypeKey(Integer proProjectReceivedTypeKey);

	String getProReceivedFrom();

	void setProReceivedFrom(String proReceivedFrom);

	LocalDateTime getProDateTimeReceived();

	void setProDateTimeReceived(LocalDateTime proDateTimeReceived);

	String getProTitle();

	void setProTitle(String proTitle);

	int getProResponsibleUnitKey();

	void setProResponsibleUnitKey(int proResponsibleUnitKey);

	Integer getProTeamKey();

	void setProTeamKey(Integer proTeamKey);

	LocalDateTime getProDateSigned();

	void setProDateSigned(LocalDateTime proDateSigned);

	LocalDate getProAwardStart();

	void setProAwardStart(LocalDate proAwardStart);

	LocalDate getProAwardEnd();

	void setProAwardEnd(LocalDate proAwardEnd);

	LocalDate getProProposedStart();

	void setProProposedStart(LocalDate proProposedStart);

	LocalDate getProProposedEnd();

	void setProProposedEnd(LocalDate proProposedEnd);

	String getProProjectLeadUserKey();

	void setProProjectLeadUserKey(String proProjectLeadUserKey);

	String getProAwardLeadUserKey();

	void setProAwardLeadUserKey(String proAwardLeadUserKey);

	Integer getProInstrumentTypeKey();

	void setProInstrumentTypeKey(Integer proInstrumentTypeKey);

	String getProCloseoutbox();

	void setProCloseoutbox(String proCloseoutbox);

	LocalDate getProCloseoutDate();

	void setProCloseoutDate(LocalDate proCloseoutDate);

	Integer getProCostSharingTypeKey();

	void setProCostSharingTypeKey(Integer proCostSharingTypeKey);

	Integer getProDfitypeKey();

	void setProDfitypeKey(Integer proDfitypeKey);

	String getProRootAwardNumber();

	void setProRootAwardNumber(String proRootAwardNumber);

	String getProOrderNumber();

	void setProOrderNumber(String proOrderNumber);

	String getProFundingAgencyCode();

	void setProFundingAgencyCode(String proFundingAgencyCode);

	String getProAwardingAgencyCode();

	void setProAwardingAgencyCode(String proAwardingAgencyCode);

	String getProTas();

	void setProTas(String proTas);

	String getProPrimeDuns();

	void setProPrimeDuns(String proPrimeDuns);

	String getProPrimeAwardNumber();

	void setProPrimeAwardNumber(String proPrimeAwardNumber);

	Integer getProPrimeInstrumentType();

	void setProPrimeInstrumentType(Integer proPrimeInstrumentType);

	Boolean getProIsDelegatedtoUcd();

	void setProIsDelegatedtoUcd(Boolean proIsDelegatedtoUcd);

	Boolean getProIsProjectAccepted();

	void setProIsProjectAccepted(Boolean proIsProjectAccepted);

	boolean isProIsActive();

	void setProIsActive(Boolean proIsActive);

	String getProUpdatedBy();

	void setProUpdatedBy(String proUpdatedBy);

	LocalDateTime getProDateCreated();

	void setProDateCreated(LocalDateTime proDateCreated);

	LocalDateTime getProDateUpdated();

	void setProDateUpdated(LocalDateTime proDateUpdated);

	CgtProjectType getProjectType();

	/**
	 * @param projectType the projectType to set
	 */
	void setProjectType(CgtProjectType projectType);

	/**
	 * @return the team
	 */
	CgtTeam getTeam();

	/**
	 * @param team the team to set
	 */
	void setTeam(CgtTeam team);

	/**
	 * @return the transactions
	 */

	List<CgtTransaction> getTransactions();

	/**
	 * @param transactions the transactions to set
	 */
	void setTransactions(List<CgtTransaction> transactions);

	/**
	 * @return the administeringUnit
	 */

	//@OrderBy("administeringUnitRole.aurSortOrder")
	List<CgtAdministeringUnit> getAdministeringUnits();

	/**
	 * @param administeringUnits the administeringUnit to set
	 */
	void setAdministeringUnits(List<CgtAdministeringUnit> administeringUnits);

	/**
	 * @return the projectSponsor
	 */

	List<CgtSponsor> getProjectSponsors();

	/**
	 * @param projectSponsor the projectSponsor to set
	 */
	void setProjectSponsors(List<CgtSponsor> projectSponsor);

	/**
	 * @return the instrumentType
	 */
	CgtInstrumentType getInstrumentType();

	/**
	 * @param instrumentType the instrumentType to set
	 */
	void setInstrumentType(CgtInstrumentType instrumentType);

	/**
	 * @return the primeInstrumentType
	 */
	CgtInstrumentType getPrimeInstrumentType();

	/**
	 * @param primeInstrumentType the primeInstrumentType to set
	 */
	void setPrimeInstrumentType(CgtInstrumentType primeInstrumentType);

	/**
	 * @return the keyPersonnel
	 */
	List<CgtKeyPersonnel> getKeyPersonnel();

	/**
	 * @param keyPersonnel the keyPersonnel to set
	 */
	void setKeyPersonnel(List<CgtKeyPersonnel> keyPersonnel);

	//calculated members
	/**
	 * @return The primary project sponsor. Every project should have one.  This method returns
	 * the first active CgtProjectSponsor found.  If no such record is found null is returned.
	 */
	CgtSponsor getPrimaryProjectSponsor();

	/**
	 * @return The Originating project sponsor. Not all projects have one of these. This method returns
	 * the first active CgtProjectSponsor found. If no such record exist, then null is returned.
	 */
	CgtSponsor getOriginatingProjectSponsor();

	CgtSponsor getProjectSponsorByType(CgtSponsorRoleTypesEnum type);

	/**
	 * @param type
	 * @return The AdministeringUnit record for the Primary|Alternate admin unit. If no such active admin unit exists, null is returned. 
	 */
	CgtAdministeringUnit getAdminsteringUnitByType(CgtAdministeringUnitRoleTypesEnum type);

	/**
	 * @return The first active CgtAdministeringUnit with role type of PRIMARY. Every project should
	 * have exactly one. This method returns the first active such record. If none exist, then null is returned.
	 */
	CgtAdministeringUnit getPrimaryAdminsteringUnit();

	/**
	 * @return The first active CgtAdministeringUnit record with role type ALTERNATE. Not every project has one. 
	 * If no active record is found with type ALTERNATE then null is returned.
	 */
	CgtAdministeringUnit getAlternateAdminsteringUnit();

}