package edu.ucdavis.orvc.integration.cgt.api.domain;

import java.util.Map;

import com.google.common.collect.ImmutableMap;

public enum CgtKeyPersonnelRoleTypeEnum {

	 PI(1,"Principal Investigator")
	,CO_I(2,"Co-Investigator")
	,COLLABORATOR(3,"Collaborator")
	,PROJECT_DIRECTOR(4,"Project Director")
	,OTHER(5,"Other")
	,ADMINISTRATOR(6,"Administrator");
	
	public int typeKey;
	public String name;
	
	public static final Map<Integer,CgtKeyPersonnelRoleTypeEnum> TYPE_MAP
		= new ImmutableMap.Builder<Integer,CgtKeyPersonnelRoleTypeEnum>()
		.put(1,PI)
		.put(2,CO_I)
		.put(3,COLLABORATOR)
		.put(4,PROJECT_DIRECTOR)
		.put(5,OTHER)
		.put(6,ADMINISTRATOR)
		.build();
	
	private CgtKeyPersonnelRoleTypeEnum(int typeKey,String name) {
		this.typeKey = typeKey;
		this.name = name;
	}

	public static CgtKeyPersonnelRoleTypeEnum getByTypeKey(Integer key) {
		return TYPE_MAP.get(key);
	}

}
