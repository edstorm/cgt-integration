package edu.ucdavis.orvc.integration.cgt.api.domain;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtWrapUpCloseRulesToSponsorCategoriesDTO;

public interface CgtWrapUpCloseRulesToSponsorCategories extends CgtBaseInterface<CgtWrapUpCloseRulesToSponsorCategoriesDTO> { 

	CgtWrapUpCloseRulesToSponsorCategoriesId getId();

	void setId(CgtWrapUpCloseRulesToSponsorCategoriesId id);

	int getW2cResultSubmissionTypeKey();

	void setW2cResultSubmissionTypeKey(int w2cResultSubmissionTypeKey);

}