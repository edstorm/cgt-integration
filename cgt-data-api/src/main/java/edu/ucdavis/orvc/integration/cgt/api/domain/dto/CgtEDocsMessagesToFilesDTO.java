package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEDocsMessagesToFiles;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEDocsMessagesToFilesId;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtEDocsMessagesToFilesDTO implements CgtEDocsMessagesToFiles {

	private static final long serialVersionUID = 2664705161278878749L;
	private CgtEDocsMessagesToFilesIdDTO id;

	public CgtEDocsMessagesToFilesDTO(final CgtEDocsMessagesToFiles fromObj) {



	}

	public CgtEDocsMessagesToFilesDTO() {
	}

	public CgtEDocsMessagesToFilesDTO(final CgtEDocsMessagesToFilesIdDTO id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsMessagesToFiles#getId()
	 */

	
	public CgtEDocsMessagesToFilesIdDTO getIdImpl() {
		return this.id;
	}

	public void setIdImpl(final CgtEDocsMessagesToFilesIdDTO id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtEDocsMessagesToFilesDTO)) {
			return false;
		}
		CgtEDocsMessagesToFilesDTO other = (CgtEDocsMessagesToFilesDTO) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("CgtEDocsMessagesToFilesImpl [id=%s]", id);
	}

	@Override
	
	public CgtEDocsMessagesToFilesId getId() {
		return id;
	}

	@Override
	public void setId(CgtEDocsMessagesToFilesId id) {
		this.id = (CgtEDocsMessagesToFilesIdDTO) id;
	}

	public CgtEDocsMessagesToFilesDTO toDTO() {
		return new CgtEDocsMessagesToFilesDTO(this);
	}
}
