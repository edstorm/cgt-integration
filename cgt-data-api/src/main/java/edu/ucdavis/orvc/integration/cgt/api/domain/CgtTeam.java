package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtTeamDTO;

public interface CgtTeam extends CgtBaseInterface<CgtTeamDTO> { 

	int getPtmTeamKey();

	void setPtmTeamKey(int ptmTeamKey);

	String getPtmNumber();

	void setPtmNumber(String ptmNumber);

	String getPtmName();

	void setPtmName(String ptmName);

	String getPtmShortName();

	void setPtmShortName(String ptmShortName);

	String getPtmEmail();

	void setPtmEmail(String ptmEmail);

	Integer getPtmSortOrder();

	void setPtmSortOrder(Integer ptmSortOrder);

	boolean isPtmIsActive();

	void setPtmIsActive(boolean ptmIsActive);

	LocalDateTime getPtmDateCreated();

	void setPtmDateCreated(LocalDateTime ptmDateCreated);

	LocalDateTime getPtmDateUpdated();

	void setPtmDateUpdated(LocalDateTime ptmDateUpdated);

	String getPtmUpdatedBy();

	void setPtmUpdatedBy(String ptmUpdatedBy);

}