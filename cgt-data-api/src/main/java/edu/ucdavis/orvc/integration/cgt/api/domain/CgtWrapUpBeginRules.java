package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtWrapUpBeginRulesDTO;

public interface CgtWrapUpBeginRules extends CgtBaseInterface<CgtWrapUpBeginRulesDTO> { 

	int getWubBeginRuleKey();

	void setWubBeginRuleKey(int wubBeginRuleKey);

	boolean isWubIsActive();

	void setWubIsActive(boolean wubIsActive);

	LocalDateTime getWubDateCreated();

	void setWubDateCreated(LocalDateTime wubDateCreated);

	LocalDateTime getWubDateUpdated();

	void setWubDateUpdated(LocalDateTime wubDateUpdated);

	String getWubUpdatedBy();

	void setWubUpdatedBy(String wubUpdatedBy);

}