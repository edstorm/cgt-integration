package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import edu.ucdavis.orvc.integration.cgt.api.domain.CgtContactInformationTypes;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtContactInformationTypesDTO implements CgtContactInformationTypes {

	private static final long serialVersionUID = -7037977855168041090L;

	private int cntContactInfoTypeKey;
	private String cntName;

	public CgtContactInformationTypesDTO() {
		super();
	}

	public CgtContactInformationTypesDTO(final int cntContactInfoTypeKey, final String cntName) {
		super();
		this.cntContactInfoTypeKey = cntContactInfoTypeKey;
		this.cntName = cntName;
	}

	public CgtContactInformationTypesDTO(final CgtContactInformationTypes fromObj) {
		super();
		this.cntContactInfoTypeKey = fromObj.getCntContactInfoTypeKey();
		this.cntName = fromObj.getCntName();
	}
	
	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformationTypes#getCntContactInfoTypeKey()
	 */
	@Override
	public int getCntContactInfoTypeKey() {
		return this.cntContactInfoTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformationTypes#setCntContactInfoTypeKey(int)
	 */
	@Override
	public void setCntContactInfoTypeKey(final int cntContactInfoTypeKey) {
		this.cntContactInfoTypeKey = cntContactInfoTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformationTypes#getCntName()
	 */
	@Override
	
	public String getCntName() {
		return this.cntName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformationTypes#setCntName(java.lang.String)
	 */
	@Override
	public void setCntName(final String cntName) {
		this.cntName = cntName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cntContactInfoTypeKey;
		result = prime * result + ((cntName == null) ? 0 : cntName.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof CgtContactInformationTypesDTO))
			return false;
		final CgtContactInformationTypesDTO other = (CgtContactInformationTypesDTO) obj;
		if (cntContactInfoTypeKey != other.cntContactInfoTypeKey)
			return false;
		if (cntName == null) {
			if (other.cntName != null)
				return false;
		} else if (!cntName.equals(other.cntName))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("CgtContactInformationTypes [cntContactInfoTypeKey=%s, cntName=%s]", cntContactInfoTypeKey,
				cntName);
	}

	public CgtContactInformationTypesDTO toDTO() {
		return new CgtContactInformationTypesDTO(this);
	}
}
