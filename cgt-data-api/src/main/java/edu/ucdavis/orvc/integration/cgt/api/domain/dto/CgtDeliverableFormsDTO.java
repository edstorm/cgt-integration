package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtDeliverableForms;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtDeliverableFormsDTO implements CgtDeliverableForms {

	private static final long serialVersionUID = 8525683332683401251L;

	private int defDeliverableFormKey;
	private String defName;
	private String defShortName;
	private Integer defSortOrder;
	private Boolean defIsActive;
	private String defUpdatedBy;
	private LocalDateTime defDateUpdated;
	private LocalDateTime defDateCreated;


	public CgtDeliverableFormsDTO() {
	}

	public CgtDeliverableFormsDTO(final int defDeliverableFormKey, final String defName) {
		this.defDeliverableFormKey = defDeliverableFormKey;
		this.defName = defName;
	}

	public CgtDeliverableFormsDTO(final int defDeliverableFormKey, final String defName, final String defShortName, final Integer defSortOrder,
			final Boolean defIsActive, final String defUpdatedBy, final LocalDateTime defDateUpdated, final LocalDateTime defDateCreated) {
		this.defDeliverableFormKey = defDeliverableFormKey;
		this.defName = defName;
		this.defShortName = defShortName;
		this.defSortOrder = defSortOrder;
		this.defIsActive = defIsActive;
		this.defUpdatedBy = defUpdatedBy;
		this.defDateUpdated = defDateUpdated;
		this.defDateCreated = defDateCreated;
	}

	public CgtDeliverableFormsDTO(final CgtDeliverableForms fromObj) {
		this.defDeliverableFormKey = fromObj.getDefDeliverableFormKey();
		this.defName = fromObj.getDefName();
		this.defShortName = fromObj.getDefShortName();
		this.defSortOrder = fromObj.getDefSortOrder();
		this.defIsActive = fromObj.getDefIsActive();
		this.defUpdatedBy = fromObj.getDefUpdatedBy();
		this.defDateUpdated = fromObj.getDefDateUpdated();
		this.defDateCreated = fromObj.getDefDateCreated();
	}

	
	
	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableForms#getDefDeliverableFormKey()
	 */
	@Override
	

	
	public int getDefDeliverableFormKey() {
		return this.defDeliverableFormKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableForms#setDefDeliverableFormKey(int)
	 */
	@Override
	public void setDefDeliverableFormKey(final int defDeliverableFormKey) {
		this.defDeliverableFormKey = defDeliverableFormKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableForms#getDefName()
	 */
	@Override
	
	public String getDefName() {
		return this.defName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableForms#setDefName(java.lang.String)
	 */
	@Override
	public void setDefName(final String defName) {
		this.defName = defName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableForms#getDefShortName()
	 */
	@Override
	
	public String getDefShortName() {
		return this.defShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableForms#setDefShortName(java.lang.String)
	 */
	@Override
	public void setDefShortName(final String defShortName) {
		this.defShortName = defShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableForms#getDefSortOrder()
	 */
	@Override
	
	public Integer getDefSortOrder() {
		return this.defSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableForms#setDefSortOrder(java.lang.Integer)
	 */
	@Override
	public void setDefSortOrder(final Integer defSortOrder) {
		this.defSortOrder = defSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableForms#getDefIsActive()
	 */
	@Override
	
	public Boolean getDefIsActive() {
		return this.defIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableForms#setDefIsActive(java.lang.Boolean)
	 */
	@Override
	public void setDefIsActive(final Boolean defIsActive) {
		this.defIsActive = defIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableForms#getDefUpdatedBy()
	 */
	@Override
	
	public String getDefUpdatedBy() {
		return this.defUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableForms#setDefUpdatedBy(java.lang.String)
	 */
	@Override
	public void setDefUpdatedBy(final String defUpdatedBy) {
		this.defUpdatedBy = defUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableForms#getDefDateUpdated()
	 */
	@Override
	
	
	public LocalDateTime getDefDateUpdated() {
		return this.defDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableForms#setDefDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setDefDateUpdated(final LocalDateTime defDateUpdated) {
		this.defDateUpdated = defDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableForms#getDefDateCreated()
	 */
	@Override
	
	
	public LocalDateTime getDefDateCreated() {
		return this.defDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtDeliverableForms#setDefDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setDefDateCreated(final LocalDateTime defDateCreated) {
		this.defDateCreated = defDateCreated;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((defDateCreated == null) ? 0 : defDateCreated.hashCode());
		result = prime * result + ((defDateUpdated == null) ? 0 : defDateUpdated.hashCode());
		result = prime * result + defDeliverableFormKey;
		result = prime * result + ((defIsActive == null) ? 0 : defIsActive.hashCode());
		result = prime * result + ((defName == null) ? 0 : defName.hashCode());
		result = prime * result + ((defShortName == null) ? 0 : defShortName.hashCode());
		result = prime * result + ((defSortOrder == null) ? 0 : defSortOrder.hashCode());
		result = prime * result + ((defUpdatedBy == null) ? 0 : defUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CgtDeliverableFormsDTO))
			return false;
		final CgtDeliverableFormsDTO other = (CgtDeliverableFormsDTO) obj;
		if (defDateCreated == null) {
			if (other.defDateCreated != null)
				return false;
		} else if (!defDateCreated.equals(other.defDateCreated))
			return false;
		if (defDateUpdated == null) {
			if (other.defDateUpdated != null)
				return false;
		} else if (!defDateUpdated.equals(other.defDateUpdated))
			return false;
		if (defDeliverableFormKey != other.defDeliverableFormKey)
			return false;
		if (defIsActive == null) {
			if (other.defIsActive != null)
				return false;
		} else if (!defIsActive.equals(other.defIsActive))
			return false;
		if (defName == null) {
			if (other.defName != null)
				return false;
		} else if (!defName.equals(other.defName))
			return false;
		if (defShortName == null) {
			if (other.defShortName != null)
				return false;
		} else if (!defShortName.equals(other.defShortName))
			return false;
		if (defSortOrder == null) {
			if (other.defSortOrder != null)
				return false;
		} else if (!defSortOrder.equals(other.defSortOrder))
			return false;
		if (defUpdatedBy == null) {
			if (other.defUpdatedBy != null)
				return false;
		} else if (!defUpdatedBy.equals(other.defUpdatedBy))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtDeliverableForms [defDeliverableFormKey=%s, defName=%s, defShortName=%s, defSortOrder=%s, defIsActive=%s, defUpdatedBy=%s, defDateUpdated=%s, defDateCreated=%s]",
				defDeliverableFormKey, defName, defShortName, defSortOrder, defIsActive, defUpdatedBy, defDateUpdated,
				defDateCreated);
	}

	public CgtDeliverableFormsDTO toDTO() {
		return new CgtDeliverableFormsDTO(this);
	}
}
