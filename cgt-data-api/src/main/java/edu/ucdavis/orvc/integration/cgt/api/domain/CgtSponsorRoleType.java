package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtSponsorRoleTypeDTO;

public interface CgtSponsorRoleType extends CgtBaseInterface<CgtSponsorRoleTypeDTO> { 

	int getSrtSponsorRoleTypeKey();

	void setSrtSponsorRoleTypeKey(int srtSponsorRoleTypeKey);

	String getSrtName();

	void setSrtName(String srtName);

	String getSrtShortName();

	void setSrtShortName(String srtShortName);

	Integer getSrtSortOrder();

	void setSrtSortOrder(Integer srtSortOrder);

	boolean isSrtIsActive();

	void setSrtIsActive(boolean srtIsActive);

	String getSrtUpdatedBy();

	void setSrtUpdatedBy(String srtUpdatedBy);

	LocalDateTime getSrtDateCreated();

	void setSrtDateCreated(LocalDateTime srtDateCreated);

	LocalDateTime getSrtDateUpdated();

	void setSrtDateUpdated(LocalDateTime srtDateUpdated);

}