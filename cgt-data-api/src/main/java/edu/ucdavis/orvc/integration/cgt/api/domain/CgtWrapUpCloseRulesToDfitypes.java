package edu.ucdavis.orvc.integration.cgt.api.domain;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtWrapUpCloseRulesToDfitypesDTO;

public interface CgtWrapUpCloseRulesToDfitypes extends CgtBaseInterface<CgtWrapUpCloseRulesToDfitypesDTO> { 

	CgtWrapUpCloseRulesToDfitypesId getId();

	void setId(CgtWrapUpCloseRulesToDfitypesId id);

	int getW2dResultSubmissionTypeKey();

	void setW2dResultSubmissionTypeKey(int w2dResultSubmissionTypeKey);

}