package edu.ucdavis.orvc.integration.cgt.api.domain.service;

import java.util.List;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEDocsFiles;

public interface CgtFilesDao {

	List<CgtEDocsFiles> getDTOByCriteria(String fileExtension, LocalDateTime startCreateDate,
			LocalDateTime endCreateDate);

	CgtEDocsFiles findByKey(Integer key);

}