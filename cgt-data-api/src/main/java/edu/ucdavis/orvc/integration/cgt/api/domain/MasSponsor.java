package edu.ucdavis.orvc.integration.cgt.api.domain;

import java.util.List;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.MasSponsorDTO;

public interface MasSponsor extends CgtBaseInterface<MasSponsorDTO> { 

	String getSpoSponsorKey();

	void setSpoSponsorKey(String spoSponsorKey);

	String getSpoSponsorCategoryKey();

	void setSpoSponsorCategoryKey(String spoSponsorCategoryKey);

	String getSpoAgencyKey();

	void setSpoAgencyKey(String spoAgencyKey);

	String getSpoSubagencyKey();

	void setSpoSubagencyKey(String spoSubagencyKey);

	String getSpoName();

	void setSpoName(String spoName);

	char getSpoIsForeign();

	void setSpoIsForeign(char spoIsForeign);

	String getSpoUcopDated();

	void setSpoUcopDated(String spoUcopDated);

	String getSpoDataSourceKey();

	void setSpoDataSourceKey(String spoDataSourceKey);

	Boolean getSpoIspermanent();

	void setSpoIspermanent(Boolean spoIspermanent);

	String getSpoSubmittedby();

	void setSpoSubmittedby(String spoSubmittedby);

	String getSpoApprovedby();

	void setSpoApprovedby(String spoApprovedby);

	LocalDateTime getSpoDateapproved();

	void setSpoDateapproved(LocalDateTime spoDateapproved);

	String getSpoDeniedby();

	void setSpoDeniedby(String spoDeniedby);

	LocalDateTime getSpoDatedenied();

	void setSpoDatedenied(LocalDateTime spoDatedenied);

	String getSpoNotes();

	void setSpoNotes(String spoNotes);

	Boolean getSpoIsActive();

	void setSpoIsActive(Boolean spoIsActive);

	LocalDateTime getSpoDateCreated();

	void setSpoDateCreated(LocalDateTime spoDateCreated);

	LocalDateTime getSpoDateUpdated();

	void setSpoDateUpdated(LocalDateTime spoDateUpdated);

	String getSpoUpdatedBy();

	void setSpoUpdatedBy(String spoUpdatedBy);

	/**
	 * @return the sponsorAgency
	 */
	MasSponsorAgency getSponsorAgency();

	/**
	 * @param sponsorAgency the sponsorAgency to set
	 */
	void setSponsorAgency(MasSponsorAgency sponsorAgency);

	/**
	 * @return the sponsorSubAgency
	 */
	MasSponsorSubAgency getSponsorSubAgency();

	/**
	 * @param sponsorSubAgency the sponsorSubAgency to set
	 */
	void setSponsorSubAgency(MasSponsorSubAgency sponsorSubAgency);

	/**
	 * @return the sponsorCategory
	 */

	MasSponsorCategory getSponsorCategory();

	/**
	 * @param sponsorCategory the sponsorCategory to set
	 */
	void setSponsorCategory(MasSponsorCategory sponsorCategory);

	/**
	 * @return the sponsor
	 */
	List<CgtSponsor> getSponsors();

	/**
	 * @param sponsor the sponsor to set
	 */
	void setSponsors(List<CgtSponsor> sponsor);

}