package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.MasDepartmentDTO;

public interface MasDepartment extends CgtBaseInterface<MasDepartmentDTO> { 

	String getDepDepartmentKey();

	void setDepDepartmentKey(String depDepartmentKey);

	String getDepBoucCode();

	void setDepBoucCode(String depBoucCode);

	String getDepName();

	void setDepName(String depName);

	Boolean getDepIsHide();

	void setDepIsHide(Boolean depIsHide);

	Character getDepUsedIndicator();

	void setDepUsedIndicator(Character depUsedIndicator);

	boolean isDepPrimaryIndicator();

	void setDepPrimaryIndicator(boolean depPrimaryIndicator);

	String getDepDataSourceKey();

	void setDepDataSourceKey(String depDataSourceKey);

	String getDepNotes();

	void setDepNotes(String depNotes);

	boolean isDepIsActive();

	void setDepIsActive(boolean depIsActive);

	LocalDateTime getDepDateCreated();

	void setDepDateCreated(LocalDateTime depDateCreated);

	LocalDateTime getDepDateUpdated();

	void setDepDateUpdated(LocalDateTime depDateUpdated);

	String getDepUpdatedBy();

	void setDepUpdatedBy(String depUpdatedBy);

}