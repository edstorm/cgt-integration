package edu.ucdavis.orvc.integration.cgt.api.domain;

import java.io.Serializable;

public interface CgtBaseInterface<T> extends Serializable {
	public T toDTO();
}
