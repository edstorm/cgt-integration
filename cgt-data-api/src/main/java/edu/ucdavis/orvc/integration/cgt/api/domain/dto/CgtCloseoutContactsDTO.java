package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import edu.ucdavis.orvc.integration.cgt.api.domain.CgtCloseoutContacts;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtCloseoutContactsId;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtCloseoutContactsDTO implements CgtCloseoutContacts {

	private static final long serialVersionUID = 1792447582136987245L;
	private CgtCloseoutContactsIdDTO id;

	public CgtCloseoutContactsDTO() {
	}

	public CgtCloseoutContactsDTO(final CgtCloseoutContactsIdDTO id) {
		this.id = id;
	}

	public CgtCloseoutContactsDTO(final CgtCloseoutContacts fromObj) {
		this.id = fromObj.getId() == null ? null:fromObj.getId().toDTO();
	}

	
	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtCloseoutContacts#getId()
	 */
	public CgtCloseoutContactsIdDTO getIdImpl() {
		return this.id;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtCloseoutContacts#setId(edu.ucdavis.orvc.integration.cgt.domain.CgtCloseoutContactsId)
	 */

	public void setIdImpl(final CgtCloseoutContactsIdDTO id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CgtCloseoutContactsDTO))
			return false;
		final CgtCloseoutContactsDTO other = (CgtCloseoutContactsDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	
	public CgtCloseoutContactsId getId() {
		return id;
	}

	@Override
	public void setId(CgtCloseoutContactsId id) {
		this.id = (CgtCloseoutContactsIdDTO) id;
	}

	public CgtCloseoutContactsDTO toDTO() {
		return new CgtCloseoutContactsDTO(this);
	}
}
