package edu.ucdavis.orvc.integration.cgt.api.domain;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtContactInformationDTO;

public interface CgtContactInformation extends CgtBaseInterface<CgtContactInformationDTO> { 

	int getContactInfoKey();

	void setContactInfoKey(int contactInfoKey);

	String getProjectKey();

	void setProjectKey(String projectKey);

	int getContactInfoTypeKey();

	void setContactInfoTypeKey(int contactInfoTypeKey);

	String getFirstName();

	void setFirstName(String firstName);

	String getLastName();

	void setLastName(String lastName);

	String getEmail();

	void setEmail(String email);

	String getPhone();

	void setPhone(String phone);

	String getFax();

	void setFax(String fax);

	String getStreet1();

	void setStreet1(String street1);

	String getStreet2();

	void setStreet2(String street2);

	String getCity();

	void setCity(String city);

	String getState();

	void setState(String state);

	String getPostalCode();

	void setPostalCode(String postalCode);

	boolean isIsActive();

	void setIsActive(boolean isActive);

}