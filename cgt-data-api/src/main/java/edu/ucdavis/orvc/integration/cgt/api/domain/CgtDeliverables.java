package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtDeliverablesDTO;

public interface CgtDeliverables extends CgtBaseInterface<CgtDeliverablesDTO> { 

	int getDelDeliverableKey();

	void setDelDeliverableKey(int delDeliverableKey);

	int getDelDeliverableTypeKey();

	void setDelDeliverableTypeKey(int delDeliverableTypeKey);

	String getDelProjectKey();

	void setDelProjectKey(String delProjectKey);

	int getDelDeliverableFormKey();

	void setDelDeliverableFormKey(int delDeliverableFormKey);

	int getDelDaysDue();

	void setDelDaysDue(int delDaysDue);

	LocalDateTime getDelDateReceived();

	void setDelDateReceived(LocalDateTime delDateReceived);

	String getDelReceivedBy();

	void setDelReceivedBy(String delReceivedBy);

	boolean isDelIsActive();

	void setDelIsActive(boolean delIsActive);

	String getDelUpdatedBy();

	void setDelUpdatedBy(String delUpdatedBy);

	LocalDateTime getDelDateUpdated();

	void setDelDateUpdated(LocalDateTime delDateUpdated);

	LocalDateTime getDelDateCreated();

	void setDelDateCreated(LocalDateTime delDateCreated);

}