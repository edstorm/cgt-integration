package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTransactionGroupType;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtTransactionGroupTypeDTO implements CgtTransactionGroupType {

	private static final long serialVersionUID = -5088013425782259734L;
	private int tgtTransactionGroupTypeKey;
	private int tgtTransactionTypeKey;
	private String tgtName;
	private String tgtDisplayText;
	private boolean tgtRequireSequenceNumber;

	public CgtTransactionGroupTypeDTO(final CgtTransactionGroupType fromObj) {



	}

	public CgtTransactionGroupTypeDTO() {
	}

	public CgtTransactionGroupTypeDTO(final int tgtTransactionGroupTypeKey, final int tgtTransactionTypeKey, final String tgtName,
			final boolean tgtRequireSequenceNumber) {
		this.tgtTransactionGroupTypeKey = tgtTransactionGroupTypeKey;
		this.tgtTransactionTypeKey = tgtTransactionTypeKey;
		this.tgtName = tgtName;
		this.tgtRequireSequenceNumber = tgtRequireSequenceNumber;
	}

	public CgtTransactionGroupTypeDTO(final int tgtTransactionGroupTypeKey, final int tgtTransactionTypeKey, final String tgtName,
			final String tgtDisplayText, final boolean tgtRequireSequenceNumber) {
		this.tgtTransactionGroupTypeKey = tgtTransactionGroupTypeKey;
		this.tgtTransactionTypeKey = tgtTransactionTypeKey;
		this.tgtName = tgtName;
		this.tgtDisplayText = tgtDisplayText;
		this.tgtRequireSequenceNumber = tgtRequireSequenceNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionGroupType#getTgtTransactionGroupTypeKey()
	 */
	@Override
	

	
	public int getTgtTransactionGroupTypeKey() {
		return this.tgtTransactionGroupTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionGroupType#setTgtTransactionGroupTypeKey(int)
	 */
	@Override
	public void setTgtTransactionGroupTypeKey(final int tgtTransactionGroupTypeKey) {
		this.tgtTransactionGroupTypeKey = tgtTransactionGroupTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionGroupType#getTgtTransactionTypeKey()
	 */
	@Override
	
	public int getTgtTransactionTypeKey() {
		return this.tgtTransactionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionGroupType#setTgtTransactionTypeKey(int)
	 */
	@Override
	public void setTgtTransactionTypeKey(final int tgtTransactionTypeKey) {
		this.tgtTransactionTypeKey = tgtTransactionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionGroupType#getTgtName()
	 */
	@Override
	
	public String getTgtName() {
		return this.tgtName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionGroupType#setTgtName(java.lang.String)
	 */
	@Override
	public void setTgtName(final String tgtName) {
		this.tgtName = tgtName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionGroupType#getTgtDisplayText()
	 */
	@Override
	
	public String getTgtDisplayText() {
		return this.tgtDisplayText;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionGroupType#setTgtDisplayText(java.lang.String)
	 */
	@Override
	public void setTgtDisplayText(final String tgtDisplayText) {
		this.tgtDisplayText = tgtDisplayText;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionGroupType#isTgtRequireSequenceNumber()
	 */
	@Override
	
	public boolean isTgtRequireSequenceNumber() {
		return this.tgtRequireSequenceNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionGroupType#setTgtRequireSequenceNumber(boolean)
	 */
	@Override
	public void setTgtRequireSequenceNumber(final boolean tgtRequireSequenceNumber) {
		this.tgtRequireSequenceNumber = tgtRequireSequenceNumber;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((tgtDisplayText == null) ? 0 : tgtDisplayText.hashCode());
		result = prime * result + ((tgtName == null) ? 0 : tgtName.hashCode());
		result = prime * result + (tgtRequireSequenceNumber ? 1231 : 1237);
		result = prime * result + tgtTransactionGroupTypeKey;
		result = prime * result + tgtTransactionTypeKey;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CgtTransactionGroupTypeDTO))
			return false;
		final CgtTransactionGroupTypeDTO other = (CgtTransactionGroupTypeDTO) obj;
		if (tgtDisplayText == null) {
			if (other.tgtDisplayText != null)
				return false;
		} else if (!tgtDisplayText.equals(other.tgtDisplayText))
			return false;
		if (tgtName == null) {
			if (other.tgtName != null)
				return false;
		} else if (!tgtName.equals(other.tgtName))
			return false;
		if (tgtRequireSequenceNumber != other.tgtRequireSequenceNumber)
			return false;
		if (tgtTransactionGroupTypeKey != other.tgtTransactionGroupTypeKey)
			return false;
		if (tgtTransactionTypeKey != other.tgtTransactionTypeKey)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtTransactionGroupTypes [tgtTransactionGroupTypeKey=%s, tgtTransactionTypeKey=%s, tgtName=%s, tgtDisplayText=%s, tgtRequireSequenceNumber=%s]",
				tgtTransactionGroupTypeKey, tgtTransactionTypeKey, tgtName, tgtDisplayText, tgtRequireSequenceNumber);
	}

	public CgtTransactionGroupTypeDTO toDTO() {
		return new CgtTransactionGroupTypeDTO(this);
	}
}
