package edu.ucdavis.orvc.integration.cgt.api.domain;

import java.math.BigDecimal;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtKeyPersonnelDTO;

public interface CgtKeyPersonnel extends CgtBaseInterface<CgtKeyPersonnelDTO> { 

	int getKepKeyPersonnelKey();

	void setKepKeyPersonnelKey(int kepKeyPersonnelKey);

	String getKepProjectKey();

	void setKepProjectKey(String kepProjectKey);

	String getKepUserKey();

	void setKepUserKey(String kepUserKey);

	int getKepKeyPersonnelRoleKey();

	void setKepKeyPersonnelRoleKey(int kepKeyPersonnelRoleKey);

	BigDecimal getKepEffortCharged();

	void setKepEffortCharged(BigDecimal kepEffortCharged);

	BigDecimal getKepEffortCostShared();

	void setKepEffortCostShared(BigDecimal kepEffortCostShared);

	BigDecimal getKepEffortCommitted();

	void setKepEffortCommitted(BigDecimal kepEffortCommitted);

	Boolean getKepIsCommitmentLetter();

	void setKepIsCommitmentLetter(Boolean kepIsCommitmentLetter);

	Boolean getKepIsSameUnit();

	void setKepIsSameUnit(Boolean kepIsSameUnit);

	boolean isKepIsActive();

	void setKepIsActive(boolean kepIsActive);

	String getKepUpdatedBy();

	void setKepUpdatedBy(String kepUpdatedBy);

	LocalDateTime getKepDateCreated();

	void setKepDateCreated(LocalDateTime kepDateCreated);

	LocalDateTime getKepDateUpdated();

	void setKepDateUpdated(LocalDateTime kepDateUpdated);

	/**
	 * @return the personnelRole
	 */
	CgtKeyPersonnelRole getPersonnelRole();

	/**
	 * @param personnelRole the personnelRole to set
	 */
	void setPersonnelRole(CgtKeyPersonnelRole personnelRole);

	/**
	 * @return the user
	 */
	MasUser getUser();

	/**
	 * @param user the user to set
	 */
	void setUser(MasUser user);

}