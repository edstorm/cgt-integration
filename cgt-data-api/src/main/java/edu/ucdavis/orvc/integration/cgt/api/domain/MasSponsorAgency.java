package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.MasSponsorAgencyDTO;

public interface MasSponsorAgency extends CgtBaseInterface<MasSponsorAgencyDTO> { 

	String getSpaSponsorAgencyKey();

	void setSpaSponsorAgencyKey(String spaSponsorAgencyKey);

	String getSpaLongName();

	void setSpaLongName(String spaLongName);

	String getSpaShortName();

	void setSpaShortName(String spaShortName);

	Boolean getSpaIsActive();

	void setSpaIsActive(Boolean spaIsActive);

	LocalDateTime getSpaDateCreated();

	void setSpaDateCreated(LocalDateTime spaDateCreated);

	LocalDateTime getSpaDateUpdated();

	void setSpaDateUpdated(LocalDateTime spaDateUpdated);

	String getSpaUpdatedBy();

	void setSpaUpdatedBy(String spaUpdatedBy);

}