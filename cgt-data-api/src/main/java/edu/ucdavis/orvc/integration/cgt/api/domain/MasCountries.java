package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.MasCountriesDTO;

public interface MasCountries extends CgtBaseInterface<MasCountriesDTO> { 

	int getCouCountryKey();

	void setCouCountryKey(int couCountryKey);

	String getCouName();

	void setCouName(String couName);

	String getCouA2();

	void setCouA2(String couA2);

	String getCouA3();

	void setCouA3(String couA3);

	Boolean getCouIsActive();

	void setCouIsActive(Boolean couIsActive);

	LocalDateTime getCouDateCreated();

	void setCouDateCreated(LocalDateTime couDateCreated);

	LocalDateTime getCouDateUpdated();

	void setCouDateUpdated(LocalDateTime couDateUpdated);

	String getCouUpdatedBy();

	void setCouUpdatedBy(String couUpdatedBy);

}