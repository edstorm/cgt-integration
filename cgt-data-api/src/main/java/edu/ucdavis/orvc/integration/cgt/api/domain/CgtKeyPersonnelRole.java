package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtKeyPersonnelRoleDTO;

public interface CgtKeyPersonnelRole extends CgtBaseInterface<CgtKeyPersonnelRoleDTO> { 

	int getKprKeyPersonnelRoleKey();

	void setKprKeyPersonnelRoleKey(int kprKeyPersonnelRoleKey);

	String getKprName();

	void setKprName(String kprName);

	String getKprShortName();

	void setKprShortName(String kprShortName);

	Integer getKprSortOrder();

	void setKprSortOrder(Integer kprSortOrder);

	boolean isKprIsActive();

	void setKprIsActive(boolean kprIsActive);

	String getKprUpdatedBy();

	void setKprUpdatedBy(String kprUpdatedBy);

	LocalDateTime getKprDateUpdated();

	void setKprDateUpdated(LocalDateTime kprDateUpdated);

	LocalDateTime getKprDateCreated();

	void setKprDateCreated(LocalDateTime kprDateCreated);

	//calculated
	CgtKeyPersonnelRoleTypeEnum getRoleTypeEnum();

}