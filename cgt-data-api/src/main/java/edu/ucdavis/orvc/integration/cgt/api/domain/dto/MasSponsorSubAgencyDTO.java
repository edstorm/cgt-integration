package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.MasSponsorSubAgency;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class MasSponsorSubAgencyDTO implements MasSponsorSubAgency {

	private static final long serialVersionUID = -4262950319374037544L;
	private String suaCode;
	private String suaName;
	private String suaShortName;
	private Boolean suaIsActive;
	private LocalDateTime suaDateCreated;
	private LocalDateTime suaDateUpdated;
	private String suaUpdatedBy;

	public MasSponsorSubAgencyDTO(final MasSponsorSubAgency fromObj) {



	}

	public MasSponsorSubAgencyDTO() {
	}

	public MasSponsorSubAgencyDTO(final String suaCode, final String suaName, final String suaShortName) {
		this.suaCode = suaCode;
		this.suaName = suaName;
		this.suaShortName = suaShortName;
	}

	public MasSponsorSubAgencyDTO(final String suaCode, final String suaName, final String suaShortName, final Boolean suaIsActive,
			final LocalDateTime suaDateCreated, final LocalDateTime suaDateUpdated, final String suaUpdatedBy) {
		this.suaCode = suaCode;
		this.suaName = suaName;
		this.suaShortName = suaShortName;
		this.suaIsActive = suaIsActive;
		this.suaDateCreated = suaDateCreated;
		this.suaDateUpdated = suaDateUpdated;
		this.suaUpdatedBy = suaUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorSubAgency#getSuaCode()
	 */
	@Override
	
	
	public String getSuaCode() {
		return this.suaCode;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorSubAgency#setSuaCode(java.lang.String)
	 */
	@Override
	public void setSuaCode(final String suaCode) {
		this.suaCode = suaCode;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorSubAgency#getSuaName()
	 */
	@Override
	
	public String getSuaName() {
		return this.suaName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorSubAgency#setSuaName(java.lang.String)
	 */
	@Override
	public void setSuaName(final String suaName) {
		this.suaName = suaName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorSubAgency#getSuaShortName()
	 */
	@Override
	
	public String getSuaShortName() {
		return this.suaShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorSubAgency#setSuaShortName(java.lang.String)
	 */
	@Override
	public void setSuaShortName(final String suaShortName) {
		this.suaShortName = suaShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorSubAgency#getSuaIsActive()
	 */
	@Override
	
	public Boolean getSuaIsActive() {
		return this.suaIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorSubAgency#setSuaIsActive(java.lang.Boolean)
	 */
	@Override
	public void setSuaIsActive(final Boolean suaIsActive) {
		this.suaIsActive = suaIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorSubAgency#getSuaDateCreated()
	 */
	@Override
	
	
	public LocalDateTime getSuaDateCreated() {
		return this.suaDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorSubAgency#setSuaDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setSuaDateCreated(final LocalDateTime suaDateCreated) {
		this.suaDateCreated = suaDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorSubAgency#getSuaDateUpdated()
	 */
	@Override
	
	
	public LocalDateTime getSuaDateUpdated() {
		return this.suaDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorSubAgency#setSuaDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setSuaDateUpdated(final LocalDateTime suaDateUpdated) {
		this.suaDateUpdated = suaDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorSubAgency#getSuaUpdatedBy()
	 */
	@Override
	
	public String getSuaUpdatedBy() {
		return this.suaUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorSubAgency#setSuaUpdatedBy(java.lang.String)
	 */
	@Override
	public void setSuaUpdatedBy(final String suaUpdatedBy) {
		this.suaUpdatedBy = suaUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((suaCode == null) ? 0 : suaCode.hashCode());
		result = prime * result + ((suaDateCreated == null) ? 0 : suaDateCreated.hashCode());
		result = prime * result + ((suaDateUpdated == null) ? 0 : suaDateUpdated.hashCode());
		result = prime * result + ((suaIsActive == null) ? 0 : suaIsActive.hashCode());
		result = prime * result + ((suaName == null) ? 0 : suaName.hashCode());
		result = prime * result + ((suaShortName == null) ? 0 : suaShortName.hashCode());
		result = prime * result + ((suaUpdatedBy == null) ? 0 : suaUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof MasSponsorSubAgencyDTO))
			return false;
		final MasSponsorSubAgencyDTO other = (MasSponsorSubAgencyDTO) obj;
		if (suaCode == null) {
			if (other.suaCode != null)
				return false;
		} else if (!suaCode.equals(other.suaCode))
			return false;
		if (suaDateCreated == null) {
			if (other.suaDateCreated != null)
				return false;
		} else if (!suaDateCreated.equals(other.suaDateCreated))
			return false;
		if (suaDateUpdated == null) {
			if (other.suaDateUpdated != null)
				return false;
		} else if (!suaDateUpdated.equals(other.suaDateUpdated))
			return false;
		if (suaIsActive == null) {
			if (other.suaIsActive != null)
				return false;
		} else if (!suaIsActive.equals(other.suaIsActive))
			return false;
		if (suaName == null) {
			if (other.suaName != null)
				return false;
		} else if (!suaName.equals(other.suaName))
			return false;
		if (suaShortName == null) {
			if (other.suaShortName != null)
				return false;
		} else if (!suaShortName.equals(other.suaShortName))
			return false;
		if (suaUpdatedBy == null) {
			if (other.suaUpdatedBy != null)
				return false;
		} else if (!suaUpdatedBy.equals(other.suaUpdatedBy))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"MasSponsorSubAgencys [suaCode=%s, suaName=%s, suaShortName=%s, suaIsActive=%s, suaDateCreated=%s, suaDateUpdated=%s, suaUpdatedBy=%s]",
				suaCode, suaName, suaShortName, suaIsActive, suaDateCreated, suaDateUpdated, suaUpdatedBy);
	}

	public MasSponsorSubAgencyDTO toDTO() {
		return new MasSponsorSubAgencyDTO(this);
	}
}
