package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtBaseRateTypesDTO;

public interface CgtBaseRateTypes extends CgtBaseInterface<CgtBaseRateTypesDTO> { 

	int getBrtBaseRateTypeKey();

	void setBrtBaseRateTypeKey(int brtBaseRateTypeKey);

	String getBrtName();

	void setBrtName(String brtName);

	String getBrtShortName();

	void setBrtShortName(String brtShortName);

	Integer getBrtSortOrder();

	void setBrtSortOrder(Integer brtSortOrder);

	boolean isBrtIsActive();

	void setBrtIsActive(boolean brtIsActive);

	String getBrtUpdatedBy();

	void setBrtUpdatedBy(String brtUpdatedBy);

	LocalDateTime getBrtDateCreated();

	void setBrtDateCreated(LocalDateTime brtDateCreated);

	LocalDateTime getBrtDateUpdated();

	void setBrtDateUpdated(LocalDateTime brtDateUpdated);

}