package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import edu.ucdavis.orvc.integration.cgt.api.domain.CgtWrapUpCloseRulesToDfitypesId;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/

public class CgtWrapUpCloseRulesToDfitypesIdDTO implements CgtWrapUpCloseRulesToDfitypesId {

	private static final long serialVersionUID = -6007294145098867767L;
	private int w2dCloseRuleKey;
	private int w2dDfiTypeKey;

	public CgtWrapUpCloseRulesToDfitypesIdDTO(final CgtWrapUpCloseRulesToDfitypesId fromObj) {



	}

	public CgtWrapUpCloseRulesToDfitypesIdDTO() {
	}

	public CgtWrapUpCloseRulesToDfitypesIdDTO(final int w2dCloseRuleKey, final int w2dDfiTypeKey) {
		this.w2dCloseRuleKey = w2dCloseRuleKey;
		this.w2dDfiTypeKey = w2dDfiTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToDfitypesId#getW2dCloseRuleKey()
	 */
	@Override
	
	public int getW2dCloseRuleKey() {
		return this.w2dCloseRuleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToDfitypesId#setW2dCloseRuleKey(int)
	 */
	@Override
	public void setW2dCloseRuleKey(final int w2dCloseRuleKey) {
		this.w2dCloseRuleKey = w2dCloseRuleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToDfitypesId#getW2dDfiTypeKey()
	 */
	@Override
	
	public int getW2dDfiTypeKey() {
		return this.w2dDfiTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToDfitypesId#setW2dDfiTypeKey(int)
	 */
	@Override
	public void setW2dDfiTypeKey(final int w2dDfiTypeKey) {
		this.w2dDfiTypeKey = w2dDfiTypeKey;
	}

	@Override
	public boolean equals(final Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof CgtWrapUpCloseRulesToDfitypesIdDTO))
			return false;
		final CgtWrapUpCloseRulesToDfitypesId castOther = (CgtWrapUpCloseRulesToDfitypesId) other;

		return (this.getW2dCloseRuleKey() == castOther.getW2dCloseRuleKey())
				&& (this.getW2dDfiTypeKey() == castOther.getW2dDfiTypeKey());
	}

	@Override
	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getW2dCloseRuleKey();
		result = 37 * result + this.getW2dDfiTypeKey();
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("CgtWrapUpCloseRulesToDfitypesId [w2dCloseRuleKey=%s, w2dDfiTypeKey=%s]", w2dCloseRuleKey,
				w2dDfiTypeKey);
	}

	public CgtWrapUpCloseRulesToDfitypesIdDTO toDTO() {
		return new CgtWrapUpCloseRulesToDfitypesIdDTO(this);
	}
}
