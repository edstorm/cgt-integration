package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import edu.ucdavis.orvc.integration.cgt.api.domain.CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesId;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/

public class CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesIdDTO implements CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesId {

	private static final long serialVersionUID = 4484959909870281268L;
	private int w2iBeginRuleKey;
	private int w2iInstrumentTypeKey;
	private int w2iTransactionTypeKey;
	private int w2iSubmissionTypeKey;

	public CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesIdDTO(final CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesId fromObj) {



	}

	public CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesIdDTO() {
	}

	public CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesIdDTO(final int w2iBeginRuleKey, final int w2iInstrumentTypeKey,
			final int w2iTransactionTypeKey, final int w2iSubmissionTypeKey) {
		this.w2iBeginRuleKey = w2iBeginRuleKey;
		this.w2iInstrumentTypeKey = w2iInstrumentTypeKey;
		this.w2iTransactionTypeKey = w2iTransactionTypeKey;
		this.w2iSubmissionTypeKey = w2iSubmissionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesId#getW2iBeginRuleKey()
	 */
	@Override
	
	public int getW2iBeginRuleKey() {
		return this.w2iBeginRuleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesId#setW2iBeginRuleKey(int)
	 */
	@Override
	public void setW2iBeginRuleKey(final int w2iBeginRuleKey) {
		this.w2iBeginRuleKey = w2iBeginRuleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesId#getW2iInstrumentTypeKey()
	 */
	@Override
	
	public int getW2iInstrumentTypeKey() {
		return this.w2iInstrumentTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesId#setW2iInstrumentTypeKey(int)
	 */
	@Override
	public void setW2iInstrumentTypeKey(final int w2iInstrumentTypeKey) {
		this.w2iInstrumentTypeKey = w2iInstrumentTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesId#getW2iTransactionTypeKey()
	 */
	@Override
	
	public int getW2iTransactionTypeKey() {
		return this.w2iTransactionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesId#setW2iTransactionTypeKey(int)
	 */
	@Override
	public void setW2iTransactionTypeKey(final int w2iTransactionTypeKey) {
		this.w2iTransactionTypeKey = w2iTransactionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesId#getW2iSubmissionTypeKey()
	 */
	@Override
	
	public int getW2iSubmissionTypeKey() {
		return this.w2iSubmissionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesId#setW2iSubmissionTypeKey(int)
	 */
	@Override
	public void setW2iSubmissionTypeKey(final int w2iSubmissionTypeKey) {
		this.w2iSubmissionTypeKey = w2iSubmissionTypeKey;
	}

	@Override
	public boolean equals(final Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesIdDTO))
			return false;
		final CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesId castOther = (CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesId) other;

		return (this.getW2iBeginRuleKey() == castOther.getW2iBeginRuleKey())
				&& (this.getW2iInstrumentTypeKey() == castOther.getW2iInstrumentTypeKey())
				&& (this.getW2iTransactionTypeKey() == castOther.getW2iTransactionTypeKey())
				&& (this.getW2iSubmissionTypeKey() == castOther.getW2iSubmissionTypeKey());
	}

	@Override
	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getW2iBeginRuleKey();
		result = 37 * result + this.getW2iInstrumentTypeKey();
		result = 37 * result + this.getW2iTransactionTypeKey();
		result = 37 * result + this.getW2iSubmissionTypeKey();
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesId [w2iBeginRuleKey=%s, w2iInstrumentTypeKey=%s, w2iTransactionTypeKey=%s, w2iSubmissionTypeKey=%s]",
				w2iBeginRuleKey, w2iInstrumentTypeKey, w2iTransactionTypeKey, w2iSubmissionTypeKey);
	}

	public CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesIdDTO toDTO() {
		return new CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesIdDTO(this);
	}
}
