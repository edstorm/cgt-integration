package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtKeyPersonnelRole;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtKeyPersonnelRoleTypeEnum;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtKeyPersonnelRoleDTO implements CgtKeyPersonnelRole {

	private static final long serialVersionUID = -4686204098549225737L;
	
	private int kprKeyPersonnelRoleKey;
	private String kprName;
	private String kprShortName;
	private Integer kprSortOrder;
	private boolean kprIsActive;
	private String kprUpdatedBy;
	private LocalDateTime kprDateUpdated;
	private LocalDateTime kprDateCreated;

	public CgtKeyPersonnelRoleDTO(final CgtKeyPersonnelRole fromObj) {



	}

	public CgtKeyPersonnelRoleDTO() {
	}

	public CgtKeyPersonnelRoleDTO(final int kprKeyPersonnelRoleKey, final String kprName, final boolean kprIsActive, final String kprUpdatedBy,
			final LocalDateTime kprDateUpdated, final LocalDateTime kprDateCreated) {
		this.kprKeyPersonnelRoleKey = kprKeyPersonnelRoleKey;
		this.kprName = kprName;
		this.kprIsActive = kprIsActive;
		this.kprUpdatedBy = kprUpdatedBy;
		this.kprDateUpdated = kprDateUpdated;
		this.kprDateCreated = kprDateCreated;
	}

	public CgtKeyPersonnelRoleDTO(final int kprKeyPersonnelRoleKey, final String kprName, final String kprShortName, final Integer kprSortOrder,
			final boolean kprIsActive, final String kprUpdatedBy, final LocalDateTime kprDateUpdated, final LocalDateTime kprDateCreated) {
		this.kprKeyPersonnelRoleKey = kprKeyPersonnelRoleKey;
		this.kprName = kprName;
		this.kprShortName = kprShortName;
		this.kprSortOrder = kprSortOrder;
		this.kprIsActive = kprIsActive;
		this.kprUpdatedBy = kprUpdatedBy;
		this.kprDateUpdated = kprDateUpdated;
		this.kprDateCreated = kprDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnelRole#getKprKeyPersonnelRoleKey()
	 */
	@Override
	

	
	public int getKprKeyPersonnelRoleKey() {
		return this.kprKeyPersonnelRoleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnelRole#setKprKeyPersonnelRoleKey(int)
	 */
	@Override
	public void setKprKeyPersonnelRoleKey(final int kprKeyPersonnelRoleKey) {
		this.kprKeyPersonnelRoleKey = kprKeyPersonnelRoleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnelRole#getKprName()
	 */
	@Override
	
	public String getKprName() {
		return this.kprName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnelRole#setKprName(java.lang.String)
	 */
	@Override
	public void setKprName(final String kprName) {
		this.kprName = kprName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnelRole#getKprShortName()
	 */
	@Override
	
	public String getKprShortName() {
		return this.kprShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnelRole#setKprShortName(java.lang.String)
	 */
	@Override
	public void setKprShortName(final String kprShortName) {
		this.kprShortName = kprShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnelRole#getKprSortOrder()
	 */
	@Override
	
	public Integer getKprSortOrder() {
		return this.kprSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnelRole#setKprSortOrder(java.lang.Integer)
	 */
	@Override
	public void setKprSortOrder(final Integer kprSortOrder) {
		this.kprSortOrder = kprSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnelRole#isKprIsActive()
	 */
	@Override
	
	public boolean isKprIsActive() {
		return this.kprIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnelRole#setKprIsActive(boolean)
	 */
	@Override
	public void setKprIsActive(final boolean kprIsActive) {
		this.kprIsActive = kprIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnelRole#getKprUpdatedBy()
	 */
	@Override
	
	public String getKprUpdatedBy() {
		return this.kprUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnelRole#setKprUpdatedBy(java.lang.String)
	 */
	@Override
	public void setKprUpdatedBy(final String kprUpdatedBy) {
		this.kprUpdatedBy = kprUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnelRole#getKprDateUpdated()
	 */
	@Override
	
	
	public LocalDateTime getKprDateUpdated() {
		return this.kprDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnelRole#setKprDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setKprDateUpdated(final LocalDateTime kprDateUpdated) {
		this.kprDateUpdated = kprDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnelRole#getKprDateCreated()
	 */
	@Override
	
	
	public LocalDateTime getKprDateCreated() {
		return this.kprDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnelRole#setKprDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setKprDateCreated(final LocalDateTime kprDateCreated) {
		this.kprDateCreated = kprDateCreated;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((kprDateCreated == null) ? 0 : kprDateCreated.hashCode());
		result = prime * result + ((kprDateUpdated == null) ? 0 : kprDateUpdated.hashCode());
		result = prime * result + (kprIsActive ? 1231 : 1237);
		result = prime * result + kprKeyPersonnelRoleKey;
		result = prime * result + ((kprName == null) ? 0 : kprName.hashCode());
		result = prime * result + ((kprShortName == null) ? 0 : kprShortName.hashCode());
		result = prime * result + ((kprSortOrder == null) ? 0 : kprSortOrder.hashCode());
		result = prime * result + ((kprUpdatedBy == null) ? 0 : kprUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtKeyPersonnelRoleDTO)) {
			return false;
		}
		CgtKeyPersonnelRoleDTO other = (CgtKeyPersonnelRoleDTO) obj;
		if (kprDateCreated == null) {
			if (other.kprDateCreated != null) {
				return false;
			}
		} else if (!kprDateCreated.equals(other.kprDateCreated)) {
			return false;
		}
		if (kprDateUpdated == null) {
			if (other.kprDateUpdated != null) {
				return false;
			}
		} else if (!kprDateUpdated.equals(other.kprDateUpdated)) {
			return false;
		}
		if (kprIsActive != other.kprIsActive) {
			return false;
		}
		if (kprKeyPersonnelRoleKey != other.kprKeyPersonnelRoleKey) {
			return false;
		}
		if (kprName == null) {
			if (other.kprName != null) {
				return false;
			}
		} else if (!kprName.equals(other.kprName)) {
			return false;
		}
		if (kprShortName == null) {
			if (other.kprShortName != null) {
				return false;
			}
		} else if (!kprShortName.equals(other.kprShortName)) {
			return false;
		}
		if (kprSortOrder == null) {
			if (other.kprSortOrder != null) {
				return false;
			}
		} else if (!kprSortOrder.equals(other.kprSortOrder)) {
			return false;
		}
		if (kprUpdatedBy == null) {
			if (other.kprUpdatedBy != null) {
				return false;
			}
		} else if (!kprUpdatedBy.equals(other.kprUpdatedBy)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtKeyPersonnelRole [kprKeyPersonnelRoleKey=%s, kprName=%s, kprShortName=%s, kprSortOrder=%s, kprIsActive=%s, kprUpdatedBy=%s, kprDateUpdated=%s, kprDateCreated=%s]",
				kprKeyPersonnelRoleKey, kprName, kprShortName, kprSortOrder, kprIsActive, kprUpdatedBy, kprDateUpdated,
				kprDateCreated);
	}
	
	
	//calculated
	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtKeyPersonnelRole#getRoleTypeEnum()
	 */
	@Override
	
	public CgtKeyPersonnelRoleTypeEnum getRoleTypeEnum() {
		return CgtKeyPersonnelRoleTypeEnum.getByTypeKey(getKprKeyPersonnelRoleKey());
	}
	

	public CgtKeyPersonnelRoleDTO toDTO() {
		return new CgtKeyPersonnelRoleDTO(this);
	}
}
