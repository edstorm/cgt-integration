package edu.ucdavis.orvc.integration.cgt.api.domain.service;

public interface PdfFileService {

	/**
	 * Get the number of pages in the document. 
	 * 
	 * @param relPathToSource  The relative path to the PDF file.
	 * @return The total number of pages.
	 */
	int getNumberOfPages(String relPathToSource);

}