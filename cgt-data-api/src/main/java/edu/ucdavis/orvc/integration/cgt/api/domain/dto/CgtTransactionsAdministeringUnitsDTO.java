package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTransactionsAdministeringUnits;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtTransactionsAdministeringUnitsDTO implements CgtTransactionsAdministeringUnits {

	private static final long serialVersionUID = -1945182388862078972L;
	private int tauTransactionAdministeringUnitKey;
	private int tauTransactionKey;
	private int tauAdministeringUnitKey;
	private long tauAllocation;
	private boolean tauIsActive;
	private String tauUpdatedBy;
	private LocalDateTime tauDateCreated;
	private LocalDateTime tauDateUpdated;

	public CgtTransactionsAdministeringUnitsDTO(final CgtTransactionsAdministeringUnits fromObj) {



	}

	public CgtTransactionsAdministeringUnitsDTO() {
	}

	public CgtTransactionsAdministeringUnitsDTO(final int tauTransactionAdministeringUnitKey, final int tauTransactionKey,
			final int tauAdministeringUnitKey, final long tauAllocation, final boolean tauIsActive, final String tauUpdatedBy,
			final LocalDateTime tauDateCreated, final LocalDateTime tauDateUpdated) {
		this.tauTransactionAdministeringUnitKey = tauTransactionAdministeringUnitKey;
		this.tauTransactionKey = tauTransactionKey;
		this.tauAdministeringUnitKey = tauAdministeringUnitKey;
		this.tauAllocation = tauAllocation;
		this.tauIsActive = tauIsActive;
		this.tauUpdatedBy = tauUpdatedBy;
		this.tauDateCreated = tauDateCreated;
		this.tauDateUpdated = tauDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsAdministeringUnits#getTauTransactionAdministeringUnitKey()
	 */
	@Override
	

	
	public int getTauTransactionAdministeringUnitKey() {
		return this.tauTransactionAdministeringUnitKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsAdministeringUnits#setTauTransactionAdministeringUnitKey(int)
	 */
	@Override
	public void setTauTransactionAdministeringUnitKey(final int tauTransactionAdministeringUnitKey) {
		this.tauTransactionAdministeringUnitKey = tauTransactionAdministeringUnitKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsAdministeringUnits#getTauTransactionKey()
	 */
	@Override
	
	public int getTauTransactionKey() {
		return this.tauTransactionKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsAdministeringUnits#setTauTransactionKey(int)
	 */
	@Override
	public void setTauTransactionKey(final int tauTransactionKey) {
		this.tauTransactionKey = tauTransactionKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsAdministeringUnits#getTauAdministeringUnitKey()
	 */
	@Override
	
	public int getTauAdministeringUnitKey() {
		return this.tauAdministeringUnitKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsAdministeringUnits#setTauAdministeringUnitKey(int)
	 */
	@Override
	public void setTauAdministeringUnitKey(final int tauAdministeringUnitKey) {
		this.tauAdministeringUnitKey = tauAdministeringUnitKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsAdministeringUnits#getTauAllocation()
	 */
	@Override
	
	public long getTauAllocation() {
		return this.tauAllocation;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsAdministeringUnits#setTauAllocation(long)
	 */
	@Override
	public void setTauAllocation(final long tauAllocation) {
		this.tauAllocation = tauAllocation;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsAdministeringUnits#isTauIsActive()
	 */
	@Override
	
	public boolean isTauIsActive() {
		return this.tauIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsAdministeringUnits#setTauIsActive(boolean)
	 */
	@Override
	public void setTauIsActive(final boolean tauIsActive) {
		this.tauIsActive = tauIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsAdministeringUnits#getTauUpdatedBy()
	 */
	@Override
	
	public String getTauUpdatedBy() {
		return this.tauUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsAdministeringUnits#setTauUpdatedBy(java.lang.String)
	 */
	@Override
	public void setTauUpdatedBy(final String tauUpdatedBy) {
		this.tauUpdatedBy = tauUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsAdministeringUnits#getTauDateCreated()
	 */
	@Override
	
	
	public LocalDateTime getTauDateCreated() {
		return this.tauDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsAdministeringUnits#setTauDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setTauDateCreated(final LocalDateTime tauDateCreated) {
		this.tauDateCreated = tauDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsAdministeringUnits#getTauDateUpdated()
	 */
	@Override
	
	
	public LocalDateTime getTauDateUpdated() {
		return this.tauDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsAdministeringUnits#setTauDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setTauDateUpdated(final LocalDateTime tauDateUpdated) {
		this.tauDateUpdated = tauDateUpdated;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + tauAdministeringUnitKey;
		result = prime * result + (int) (tauAllocation ^ (tauAllocation >>> 32));
		result = prime * result + ((tauDateCreated == null) ? 0 : tauDateCreated.hashCode());
		result = prime * result + ((tauDateUpdated == null) ? 0 : tauDateUpdated.hashCode());
		result = prime * result + (tauIsActive ? 1231 : 1237);
		result = prime * result + tauTransactionAdministeringUnitKey;
		result = prime * result + tauTransactionKey;
		result = prime * result + ((tauUpdatedBy == null) ? 0 : tauUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CgtTransactionsAdministeringUnitsDTO))
			return false;
		final CgtTransactionsAdministeringUnitsDTO other = (CgtTransactionsAdministeringUnitsDTO) obj;
		if (tauAdministeringUnitKey != other.tauAdministeringUnitKey)
			return false;
		if (tauAllocation != other.tauAllocation)
			return false;
		if (tauDateCreated == null) {
			if (other.tauDateCreated != null)
				return false;
		} else if (!tauDateCreated.equals(other.tauDateCreated))
			return false;
		if (tauDateUpdated == null) {
			if (other.tauDateUpdated != null)
				return false;
		} else if (!tauDateUpdated.equals(other.tauDateUpdated))
			return false;
		if (tauIsActive != other.tauIsActive)
			return false;
		if (tauTransactionAdministeringUnitKey != other.tauTransactionAdministeringUnitKey)
			return false;
		if (tauTransactionKey != other.tauTransactionKey)
			return false;
		if (tauUpdatedBy == null) {
			if (other.tauUpdatedBy != null)
				return false;
		} else if (!tauUpdatedBy.equals(other.tauUpdatedBy))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtTransactionsAdministeringUnits [tauTransactionAdministeringUnitKey=%s, tauTransactionKey=%s, tauAdministeringUnitKey=%s, tauAllocation=%s, tauIsActive=%s, tauUpdatedBy=%s, tauDateCreated=%s, tauDateUpdated=%s]",
				tauTransactionAdministeringUnitKey, tauTransactionKey, tauAdministeringUnitKey, tauAllocation,
				tauIsActive, tauUpdatedBy, tauDateCreated, tauDateUpdated);
	}

	public CgtTransactionsAdministeringUnitsDTO toDTO() {
		return new CgtTransactionsAdministeringUnitsDTO(this);
	}
}
