package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.MasStatesDTO;

public interface MasStates extends CgtBaseInterface<MasStatesDTO> { 

	int getStaStateKey();

	void setStaStateKey(int staStateKey);

	String getStaName();

	void setStaName(String staName);

	String getStaShortName();

	void setStaShortName(String staShortName);

	boolean isStaIsActive();

	void setStaIsActive(boolean staIsActive);

	LocalDateTime getStaDateCreated();

	void setStaDateCreated(LocalDateTime staDateCreated);

	LocalDateTime getStaDateUpdated();

	void setStaDateUpdated(LocalDateTime staDateUpdated);

	String getStaUpdatedBy();

	void setStaUpdatedBy(String staUpdatedBy);

}