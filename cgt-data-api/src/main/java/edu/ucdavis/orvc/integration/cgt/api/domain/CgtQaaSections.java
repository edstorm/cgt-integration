package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtQaaSectionsDTO;

public interface CgtQaaSections extends CgtBaseInterface<CgtQaaSectionsDTO> { 

	int getSecSectionKey();

	void setSecSectionKey(int secSectionKey);

	String getSecName();

	void setSecName(String secName);

	String getSecDescription();

	void setSecDescription(String secDescription);

	boolean isSecIsActive();

	void setSecIsActive(boolean secIsActive);

	LocalDateTime getSecDateCreated();

	void setSecDateCreated(LocalDateTime secDateCreated);

	LocalDateTime getSecDateUpdated();

	void setSecDateUpdated(LocalDateTime secDateUpdated);

	String getSecUpdatedBy();

	void setSecUpdatedBy(String secUpdatedBy);

}