package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtEDocsRecipientsDTO;

public interface CgtEDocsRecipients extends CgtBaseInterface<CgtEDocsRecipientsDTO> { 

	int getRecRecipientKey();

	void setRecRecipientKey(int recRecipientKey);

	String getRecEdocKey();

	void setRecEdocKey(String recEdocKey);

	String getRecEmail();

	void setRecEmail(String recEmail);

	String getRecEmailType();

	void setRecEmailType(String recEmailType);

	boolean isRecIsActive();

	void setRecIsActive(boolean recIsActive);

	LocalDateTime getRecDateCreated();

	void setRecDateCreated(LocalDateTime recDateCreated);

	LocalDateTime getRecDateUpdated();

	void setRecDateUpdated(LocalDateTime recDateUpdated);

	String getRecUpdatedBy();

	void setRecUpdatedBy(String recUpdatedBy);

}