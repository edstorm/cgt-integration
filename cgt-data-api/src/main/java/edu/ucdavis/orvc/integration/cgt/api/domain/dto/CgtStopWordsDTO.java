package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import edu.ucdavis.orvc.integration.cgt.api.domain.CgtStopWords;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtStopWordsDTO implements CgtStopWords {

	private static final long serialVersionUID = 4959276627523872881L;
	private String word;

	public CgtStopWordsDTO(final CgtStopWords fromObj) {



	}

	public CgtStopWordsDTO() {
	}

	public CgtStopWordsDTO(final String word) {
		this.word = word;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtStopWords#getWord()
	 */
	@Override
	

	
	public String getWord() {
		return this.word;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtStopWords#setWord(java.lang.String)
	 */
	@Override
	public void setWord(final String word) {
		this.word = word;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((word == null) ? 0 : word.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtStopWordsDTO)) {
			return false;
		}
		CgtStopWordsDTO other = (CgtStopWordsDTO) obj;
		if (word == null) {
			if (other.word != null) {
				return false;
			}
		} else if (!word.equals(other.word)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("CgtStopWordsImpl [word=%s]", word);
	}

	public CgtStopWordsDTO toDTO() {
		return new CgtStopWordsDTO(this);
	}
}
