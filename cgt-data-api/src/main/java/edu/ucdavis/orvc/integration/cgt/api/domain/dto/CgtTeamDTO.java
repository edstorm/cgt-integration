package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTeam;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtTeamDTO implements CgtTeam {

	private static final long serialVersionUID = 2273706437675119534L;
	private int ptmTeamKey;
	private String ptmNumber;
	private String ptmName;
	private String ptmShortName;
	private String ptmEmail;
	private Integer ptmSortOrder;
	private boolean ptmIsActive;
	private LocalDateTime ptmDateCreated;
	private LocalDateTime ptmDateUpdated;
	private String ptmUpdatedBy;

	public CgtTeamDTO(final CgtTeam fromObj) {



	}

	public CgtTeamDTO() {
	}

	public CgtTeamDTO(final int ptmTeamKey, final String ptmNumber, final String ptmName, final boolean ptmIsActive, final LocalDateTime ptmDateCreated,
			final LocalDateTime ptmDateUpdated, final String ptmUpdatedBy) {
		this.ptmTeamKey = ptmTeamKey;
		this.ptmNumber = ptmNumber;
		this.ptmName = ptmName;
		this.ptmIsActive = ptmIsActive;
		this.ptmDateCreated = ptmDateCreated;
		this.ptmDateUpdated = ptmDateUpdated;
		this.ptmUpdatedBy = ptmUpdatedBy;
	}
	
	public CgtTeamDTO(final int ptmTeamKey, final String ptmNumber, final String ptmName, final String ptmShortName, final String ptmEmail,
			final Integer ptmSortOrder, final boolean ptmIsActive, final LocalDateTime ptmDateCreated, final LocalDateTime ptmDateUpdated, final String ptmUpdatedBy) {
		this.ptmTeamKey = ptmTeamKey;
		this.ptmNumber = ptmNumber;
		this.ptmName = ptmName;
		this.ptmShortName = ptmShortName;
		this.ptmEmail = ptmEmail;
		this.ptmSortOrder = ptmSortOrder;
		this.ptmIsActive = ptmIsActive;
		this.ptmDateCreated = ptmDateCreated;
		this.ptmDateUpdated = ptmDateUpdated;
		this.ptmUpdatedBy = ptmUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeam#getPtmTeamKey()
	 */
	@Override
	

	
	public int getPtmTeamKey() {
		return this.ptmTeamKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeam#setPtmTeamKey(int)
	 */
	@Override
	public void setPtmTeamKey(final int ptmTeamKey) {
		this.ptmTeamKey = ptmTeamKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeam#getPtmNumber()
	 */
	@Override
	
	public String getPtmNumber() {
		return this.ptmNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeam#setPtmNumber(java.lang.String)
	 */
	@Override
	public void setPtmNumber(final String ptmNumber) {
		this.ptmNumber = ptmNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeam#getPtmName()
	 */
	@Override
	
	public String getPtmName() {
		return this.ptmName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeam#setPtmName(java.lang.String)
	 */
	@Override
	public void setPtmName(final String ptmName) {
		this.ptmName = ptmName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeam#getPtmShortName()
	 */
	@Override
	
	public String getPtmShortName() {
		return this.ptmShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeam#setPtmShortName(java.lang.String)
	 */
	@Override
	public void setPtmShortName(final String ptmShortName) {
		this.ptmShortName = ptmShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeam#getPtmEmail()
	 */
	@Override
	
	public String getPtmEmail() {
		return this.ptmEmail;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeam#setPtmEmail(java.lang.String)
	 */
	@Override
	public void setPtmEmail(final String ptmEmail) {
		this.ptmEmail = ptmEmail;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeam#getPtmSortOrder()
	 */
	@Override
	
	public Integer getPtmSortOrder() {
		return this.ptmSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeam#setPtmSortOrder(java.lang.Integer)
	 */
	@Override
	public void setPtmSortOrder(final Integer ptmSortOrder) {
		this.ptmSortOrder = ptmSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeam#isPtmIsActive()
	 */
	@Override
	
	public boolean isPtmIsActive() {
		return this.ptmIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeam#setPtmIsActive(boolean)
	 */
	@Override
	public void setPtmIsActive(final boolean ptmIsActive) {
		this.ptmIsActive = ptmIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeam#getPtmDateCreated()
	 */
	@Override
	
	
	public LocalDateTime getPtmDateCreated() {
		return this.ptmDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeam#setPtmDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setPtmDateCreated(final LocalDateTime ptmDateCreated) {
		this.ptmDateCreated = ptmDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeam#getPtmDateUpdated()
	 */
	@Override
	
	
	public LocalDateTime getPtmDateUpdated() {
		return this.ptmDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeam#setPtmDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setPtmDateUpdated(final LocalDateTime ptmDateUpdated) {
		this.ptmDateUpdated = ptmDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeam#getPtmUpdatedBy()
	 */
	@Override
	
	public String getPtmUpdatedBy() {
		return this.ptmUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeam#setPtmUpdatedBy(java.lang.String)
	 */
	@Override
	public void setPtmUpdatedBy(final String ptmUpdatedBy) {
		this.ptmUpdatedBy = ptmUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((ptmDateCreated == null) ? 0 : ptmDateCreated.hashCode());
		result = prime * result + ((ptmDateUpdated == null) ? 0 : ptmDateUpdated.hashCode());
		result = prime * result + ((ptmEmail == null) ? 0 : ptmEmail.hashCode());
		result = prime * result + (ptmIsActive ? 1231 : 1237);
		result = prime * result + ((ptmName == null) ? 0 : ptmName.hashCode());
		result = prime * result + ((ptmNumber == null) ? 0 : ptmNumber.hashCode());
		result = prime * result + ((ptmShortName == null) ? 0 : ptmShortName.hashCode());
		result = prime * result + ((ptmSortOrder == null) ? 0 : ptmSortOrder.hashCode());
		result = prime * result + ptmTeamKey;
		result = prime * result + ((ptmUpdatedBy == null) ? 0 : ptmUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CgtTeamDTO))
			return false;
		final CgtTeamDTO other = (CgtTeamDTO) obj;
		if (ptmDateCreated == null) {
			if (other.ptmDateCreated != null)
				return false;
		} else if (!ptmDateCreated.equals(other.ptmDateCreated))
			return false;
		if (ptmDateUpdated == null) {
			if (other.ptmDateUpdated != null)
				return false;
		} else if (!ptmDateUpdated.equals(other.ptmDateUpdated))
			return false;
		if (ptmEmail == null) {
			if (other.ptmEmail != null)
				return false;
		} else if (!ptmEmail.equals(other.ptmEmail))
			return false;
		if (ptmIsActive != other.ptmIsActive)
			return false;
		if (ptmName == null) {
			if (other.ptmName != null)
				return false;
		} else if (!ptmName.equals(other.ptmName))
			return false;
		if (ptmNumber == null) {
			if (other.ptmNumber != null)
				return false;
		} else if (!ptmNumber.equals(other.ptmNumber))
			return false;
		if (ptmShortName == null) {
			if (other.ptmShortName != null)
				return false;
		} else if (!ptmShortName.equals(other.ptmShortName))
			return false;
		if (ptmSortOrder == null) {
			if (other.ptmSortOrder != null)
				return false;
		} else if (!ptmSortOrder.equals(other.ptmSortOrder))
			return false;
		if (ptmTeamKey != other.ptmTeamKey)
			return false;
		if (ptmUpdatedBy == null) {
			if (other.ptmUpdatedBy != null)
				return false;
		} else if (!ptmUpdatedBy.equals(other.ptmUpdatedBy))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtTeams [ptmTeamKey=%s, ptmNumber=%s, ptmName=%s, ptmShortName=%s, ptmEmail=%s, ptmSortOrder=%s, ptmIsActive=%s, ptmDateCreated=%s, ptmDateUpdated=%s, ptmUpdatedBy=%s]",
				ptmTeamKey, ptmNumber, ptmName, ptmShortName, ptmEmail, ptmSortOrder, ptmIsActive, ptmDateCreated,
				ptmDateUpdated, ptmUpdatedBy);
	}

	public CgtTeamDTO toDTO() {
		return new CgtTeamDTO(this);
	}
}
