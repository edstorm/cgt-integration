package edu.ucdavis.orvc.integration.cgt.api.domain;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtWrapUpCloseRulesToQaaAnswersDTO;

public interface CgtWrapUpCloseRulesToQaaAnswers extends CgtBaseInterface<CgtWrapUpCloseRulesToQaaAnswersDTO> { 

	int getW2qRuleKey();

	void setW2qRuleKey(int w2qRuleKey);

	int getW2qCloseRuleKey();

	void setW2qCloseRuleKey(int w2qCloseRuleKey);

	int getW2qQuestionKey();

	void setW2qQuestionKey(int w2qQuestionKey);

	String getW2qAnswer();

	void setW2qAnswer(String w2qAnswer);

	String getW2qBouCode();

	void setW2qBouCode(String w2qBouCode);

	String getW2qNotBouCode();

	void setW2qNotBouCode(String w2qNotBouCode);

	Integer getW2qResultSubmissionTypeKey();

	void setW2qResultSubmissionTypeKey(Integer w2qResultSubmissionTypeKey);

}