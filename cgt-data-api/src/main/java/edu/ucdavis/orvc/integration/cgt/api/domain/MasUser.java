package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.MasUserDTO;

public interface MasUser extends CgtBaseInterface<MasUserDTO> { 

	String getUseUserKey();

	void setUseUserKey(String useUserKey);

	String getUseSsn();

	void setUseSsn(String useSsn);

	String getUseUcdlogin();

	void setUseUcdlogin(String useUcdlogin);

	String getUseUserName();

	void setUseUserName(String useUserName);

	String getUsePassword();

	void setUsePassword(String usePassword);

	String getUseEmployeeId();

	void setUseEmployeeId(String useEmployeeId);

	String getUseFirstName();

	void setUseFirstName(String useFirstName);

	String getUseMiddleName();

	void setUseMiddleName(String useMiddleName);

	String getUseLastName();

	void setUseLastName(String useLastName);

	String getUseMothraFname();

	void setUseMothraFname(String useMothraFname);

	String getUseMothraMname();

	void setUseMothraMname(String useMothraMname);

	String getUseMothraLname();

	void setUseMothraLname(String useMothraLname);

	String getUseMothraId();

	void setUseMothraId(String useMothraId);

	String getUseDisplayTitle();

	void setUseDisplayTitle(String useDisplayTitle);

	String getUseUserTitleKey();

	void setUseUserTitleKey(String useUserTitleKey);

	Integer getUseUserPrefixKey();

	void setUseUserPrefixKey(Integer useUserPrefixKey);

	Integer getUseUserSuffixKey();

	void setUseUserSuffixKey(Integer useUserSuffixKey);

	String getUseDepartmentKey();

	void setUseDepartmentKey(String useDepartmentKey);

	String getUseInstitutionKey();

	void setUseInstitutionKey(String useInstitutionKey);

	String getUsePhone();

	void setUsePhone(String usePhone);

	String getUseFax();

	void setUseFax(String useFax);

	String getUseEmail();

	void setUseEmail(String useEmail);

	LocalDateTime getUseMothraVerifyDate();

	void setUseMothraVerifyDate(LocalDateTime useMothraVerifyDate);

	boolean isUseIsActive();

	void setUseIsActive(boolean useIsActive);

	String getUseNotes();

	void setUseNotes(String useNotes);

	Boolean getUseIsPasswordReset();

	void setUseIsPasswordReset(Boolean useIsPasswordReset);

	LocalDateTime getUseDateCreated();

	void setUseDateCreated(LocalDateTime useDateCreated);

	LocalDateTime getUseDateUpdated();

	void setUseDateUpdated(LocalDateTime useDateUpdated);

	String getUseDataSourceKey();

	void setUseDataSourceKey(String useDataSourceKey);

	String getUseUpdatedBy();

	void setUseUpdatedBy(String useUpdatedBy);

	boolean isUseExistsInLdap();

	void setUseExistsInLdap(boolean useExistsInLdap);

}