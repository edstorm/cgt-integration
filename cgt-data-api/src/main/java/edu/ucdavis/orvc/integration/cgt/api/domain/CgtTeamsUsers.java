package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtTeamsUsersDTO;

public interface CgtTeamsUsers extends CgtBaseInterface<CgtTeamsUsersDTO> { 

	int getPtuTeamUserKey();

	void setPtuTeamUserKey(int ptuTeamUserKey);

	String getPtuUserKey();

	void setPtuUserKey(String ptuUserKey);

	int getPtuTeamKey();

	void setPtuTeamKey(int ptuTeamKey);

	LocalDateTime getPtuOutStartDate();

	void setPtuOutStartDate(LocalDateTime ptuOutStartDate);

	LocalDateTime getPtuOutEndDate();

	void setPtuOutEndDate(LocalDateTime ptuOutEndDate);

	boolean isPtuIsPrimary();

	void setPtuIsPrimary(boolean ptuIsPrimary);

	boolean isPtuIsActive();

	void setPtuIsActive(boolean ptuIsActive);

	String getPtuUpdatedBy();

	void setPtuUpdatedBy(String ptuUpdatedBy);

	LocalDateTime getPtuDateCreated();

	void setPtuDateCreated(LocalDateTime ptuDateCreated);

	LocalDateTime getPtuDateUpdated();

	void setPtuDateUpdated(LocalDateTime ptuDateUpdated);

}