package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTeamsUsers;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtTeamsUsersDTO implements CgtTeamsUsers {

	private static final long serialVersionUID = -5843070716475385360L;
	private int ptuTeamUserKey;
	private String ptuUserKey;
	private int ptuTeamKey;
	private LocalDateTime ptuOutStartDate;
	private LocalDateTime ptuOutEndDate;
	private boolean ptuIsPrimary;
	private boolean ptuIsActive;
	private String ptuUpdatedBy;
	private LocalDateTime ptuDateCreated;
	private LocalDateTime ptuDateUpdated;

	public CgtTeamsUsersDTO(final CgtTeamsUsers fromObj) {



	}

	public CgtTeamsUsersDTO() {
	}

	public CgtTeamsUsersDTO(final int ptuTeamUserKey, final String ptuUserKey, final int ptuTeamKey, final boolean ptuIsPrimary,
			final boolean ptuIsActive, final String ptuUpdatedBy, final LocalDateTime ptuDateCreated, final LocalDateTime ptuDateUpdated) {
		this.ptuTeamUserKey = ptuTeamUserKey;
		this.ptuUserKey = ptuUserKey;
		this.ptuTeamKey = ptuTeamKey;
		this.ptuIsPrimary = ptuIsPrimary;
		this.ptuIsActive = ptuIsActive;
		this.ptuUpdatedBy = ptuUpdatedBy;
		this.ptuDateCreated = ptuDateCreated;
		this.ptuDateUpdated = ptuDateUpdated;
	}

	public CgtTeamsUsersDTO(final int ptuTeamUserKey, final String ptuUserKey, final int ptuTeamKey, final LocalDateTime ptuOutStartDate,
			final LocalDateTime ptuOutEndDate, final boolean ptuIsPrimary, final boolean ptuIsActive, final String ptuUpdatedBy, final LocalDateTime ptuDateCreated,
			final LocalDateTime ptuDateUpdated) {
		this.ptuTeamUserKey = ptuTeamUserKey;
		this.ptuUserKey = ptuUserKey;
		this.ptuTeamKey = ptuTeamKey;
		this.ptuOutStartDate = ptuOutStartDate;
		this.ptuOutEndDate = ptuOutEndDate;
		this.ptuIsPrimary = ptuIsPrimary;
		this.ptuIsActive = ptuIsActive;
		this.ptuUpdatedBy = ptuUpdatedBy;
		this.ptuDateCreated = ptuDateCreated;
		this.ptuDateUpdated = ptuDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeamsUsers#getPtuTeamUserKey()
	 */
	@Override
	

	
	public int getPtuTeamUserKey() {
		return this.ptuTeamUserKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeamsUsers#setPtuTeamUserKey(int)
	 */
	@Override
	public void setPtuTeamUserKey(final int ptuTeamUserKey) {
		this.ptuTeamUserKey = ptuTeamUserKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeamsUsers#getPtuUserKey()
	 */
	@Override
	
	public String getPtuUserKey() {
		return this.ptuUserKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeamsUsers#setPtuUserKey(java.lang.String)
	 */
	@Override
	public void setPtuUserKey(final String ptuUserKey) {
		this.ptuUserKey = ptuUserKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeamsUsers#getPtuTeamKey()
	 */
	@Override
	
	public int getPtuTeamKey() {
		return this.ptuTeamKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeamsUsers#setPtuTeamKey(int)
	 */
	@Override
	public void setPtuTeamKey(final int ptuTeamKey) {
		this.ptuTeamKey = ptuTeamKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeamsUsers#getPtuOutStartDate()
	 */
	@Override
	
	
	public LocalDateTime getPtuOutStartDate() {
		return this.ptuOutStartDate;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeamsUsers#setPtuOutStartDate(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setPtuOutStartDate(final LocalDateTime ptuOutStartDate) {
		this.ptuOutStartDate = ptuOutStartDate;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeamsUsers#getPtuOutEndDate()
	 */
	@Override
	
	
	public LocalDateTime getPtuOutEndDate() {
		return this.ptuOutEndDate;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeamsUsers#setPtuOutEndDate(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setPtuOutEndDate(final LocalDateTime ptuOutEndDate) {
		this.ptuOutEndDate = ptuOutEndDate;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeamsUsers#isPtuIsPrimary()
	 */
	@Override
	
	public boolean isPtuIsPrimary() {
		return this.ptuIsPrimary;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeamsUsers#setPtuIsPrimary(boolean)
	 */
	@Override
	public void setPtuIsPrimary(final boolean ptuIsPrimary) {
		this.ptuIsPrimary = ptuIsPrimary;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeamsUsers#isPtuIsActive()
	 */
	@Override
	
	public boolean isPtuIsActive() {
		return this.ptuIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeamsUsers#setPtuIsActive(boolean)
	 */
	@Override
	public void setPtuIsActive(final boolean ptuIsActive) {
		this.ptuIsActive = ptuIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeamsUsers#getPtuUpdatedBy()
	 */
	@Override
	
	public String getPtuUpdatedBy() {
		return this.ptuUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeamsUsers#setPtuUpdatedBy(java.lang.String)
	 */
	@Override
	public void setPtuUpdatedBy(final String ptuUpdatedBy) {
		this.ptuUpdatedBy = ptuUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeamsUsers#getPtuDateCreated()
	 */
	@Override
	
	
	public LocalDateTime getPtuDateCreated() {
		return this.ptuDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeamsUsers#setPtuDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setPtuDateCreated(final LocalDateTime ptuDateCreated) {
		this.ptuDateCreated = ptuDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeamsUsers#getPtuDateUpdated()
	 */
	@Override
	
	
	public LocalDateTime getPtuDateUpdated() {
		return this.ptuDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTeamsUsers#setPtuDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setPtuDateUpdated(final LocalDateTime ptuDateUpdated) {
		this.ptuDateUpdated = ptuDateUpdated;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((ptuDateCreated == null) ? 0 : ptuDateCreated.hashCode());
		result = prime * result + ((ptuDateUpdated == null) ? 0 : ptuDateUpdated.hashCode());
		result = prime * result + (ptuIsActive ? 1231 : 1237);
		result = prime * result + (ptuIsPrimary ? 1231 : 1237);
		result = prime * result + ((ptuOutEndDate == null) ? 0 : ptuOutEndDate.hashCode());
		result = prime * result + ((ptuOutStartDate == null) ? 0 : ptuOutStartDate.hashCode());
		result = prime * result + ptuTeamKey;
		result = prime * result + ptuTeamUserKey;
		result = prime * result + ((ptuUpdatedBy == null) ? 0 : ptuUpdatedBy.hashCode());
		result = prime * result + ((ptuUserKey == null) ? 0 : ptuUserKey.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CgtTeamsUsersDTO))
			return false;
		final CgtTeamsUsersDTO other = (CgtTeamsUsersDTO) obj;
		if (ptuDateCreated == null) {
			if (other.ptuDateCreated != null)
				return false;
		} else if (!ptuDateCreated.equals(other.ptuDateCreated))
			return false;
		if (ptuDateUpdated == null) {
			if (other.ptuDateUpdated != null)
				return false;
		} else if (!ptuDateUpdated.equals(other.ptuDateUpdated))
			return false;
		if (ptuIsActive != other.ptuIsActive)
			return false;
		if (ptuIsPrimary != other.ptuIsPrimary)
			return false;
		if (ptuOutEndDate == null) {
			if (other.ptuOutEndDate != null)
				return false;
		} else if (!ptuOutEndDate.equals(other.ptuOutEndDate))
			return false;
		if (ptuOutStartDate == null) {
			if (other.ptuOutStartDate != null)
				return false;
		} else if (!ptuOutStartDate.equals(other.ptuOutStartDate))
			return false;
		if (ptuTeamKey != other.ptuTeamKey)
			return false;
		if (ptuTeamUserKey != other.ptuTeamUserKey)
			return false;
		if (ptuUpdatedBy == null) {
			if (other.ptuUpdatedBy != null)
				return false;
		} else if (!ptuUpdatedBy.equals(other.ptuUpdatedBy))
			return false;
		if (ptuUserKey == null) {
			if (other.ptuUserKey != null)
				return false;
		} else if (!ptuUserKey.equals(other.ptuUserKey))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtTeamsUsers [ptuTeamUserKey=%s, ptuUserKey=%s, ptuTeamKey=%s, ptuOutStartDate=%s, ptuOutEndDate=%s, ptuIsPrimary=%s, ptuIsActive=%s, ptuUpdatedBy=%s, ptuDateCreated=%s, ptuDateUpdated=%s]",
				ptuTeamUserKey, ptuUserKey, ptuTeamKey, ptuOutStartDate, ptuOutEndDate, ptuIsPrimary, ptuIsActive,
				ptuUpdatedBy, ptuDateCreated, ptuDateUpdated);
	}

	public CgtTeamsUsersDTO toDTO() {
		return new CgtTeamsUsersDTO(this);
	}
}
