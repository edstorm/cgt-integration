package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtWrapUpBeginRules;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtWrapUpBeginRulesDTO;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_WrapUpBeginRules")
public class CgtWrapUpBeginRulesImpl implements CgtWrapUpBeginRules {

	private static final long serialVersionUID = 2227814002457464586L;
	private int wubBeginRuleKey;
	private boolean wubIsActive;
	private LocalDateTime wubDateCreated;
	private LocalDateTime wubDateUpdated;
	private String wubUpdatedBy;

	public CgtWrapUpBeginRulesImpl() {
	}

	public CgtWrapUpBeginRulesImpl(final int wubBeginRuleKey, final boolean wubIsActive, final LocalDateTime wubDateCreated, final LocalDateTime wubDateUpdated,
			final String wubUpdatedBy) {
		this.wubBeginRuleKey = wubBeginRuleKey;
		this.wubIsActive = wubIsActive;
		this.wubDateCreated = wubDateCreated;
		this.wubDateUpdated = wubDateUpdated;
		this.wubUpdatedBy = wubUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpBeginRules#getWubBeginRuleKey()
	 */
	@Override
	@Id
	@Column(name = "wub_beginRuleKey", unique = true, nullable = false)
	public int getWubBeginRuleKey() {
		return this.wubBeginRuleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpBeginRules#setWubBeginRuleKey(int)
	 */
	@Override
	public void setWubBeginRuleKey(final int wubBeginRuleKey) {
		this.wubBeginRuleKey = wubBeginRuleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpBeginRules#isWubIsActive()
	 */
	@Override
	@Column(name = "wub_isActive", nullable = false)
	public boolean isWubIsActive() {
		return this.wubIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpBeginRules#setWubIsActive(boolean)
	 */
	@Override
	public void setWubIsActive(final boolean wubIsActive) {
		this.wubIsActive = wubIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpBeginRules#getWubDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "wub_dateCreated", nullable = false, length = 23)
	public LocalDateTime getWubDateCreated() {
		return this.wubDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpBeginRules#setWubDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setWubDateCreated(final LocalDateTime wubDateCreated) {
		this.wubDateCreated = wubDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpBeginRules#getWubDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "wub_dateUpdated", nullable = false, length = 23)
	public LocalDateTime getWubDateUpdated() {
		return this.wubDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpBeginRules#setWubDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setWubDateUpdated(final LocalDateTime wubDateUpdated) {
		this.wubDateUpdated = wubDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpBeginRules#getWubUpdatedBy()
	 */
	@Override
	@Column(name = "wub_updatedBy", nullable = false, length = 35, columnDefinition="char")
	public String getWubUpdatedBy() {
		return this.wubUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpBeginRules#setWubUpdatedBy(java.lang.String)
	 */
	@Override
	public void setWubUpdatedBy(final String wubUpdatedBy) {
		this.wubUpdatedBy = wubUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + wubBeginRuleKey;
		result = prime * result + ((wubDateCreated == null) ? 0 : wubDateCreated.hashCode());
		result = prime * result + ((wubDateUpdated == null) ? 0 : wubDateUpdated.hashCode());
		result = prime * result + (wubIsActive ? 1231 : 1237);
		result = prime * result + ((wubUpdatedBy == null) ? 0 : wubUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CgtWrapUpBeginRulesImpl))
			return false;
		final CgtWrapUpBeginRulesImpl other = (CgtWrapUpBeginRulesImpl) obj;
		if (wubBeginRuleKey != other.wubBeginRuleKey)
			return false;
		if (wubDateCreated == null) {
			if (other.wubDateCreated != null)
				return false;
		} else if (!wubDateCreated.equals(other.wubDateCreated))
			return false;
		if (wubDateUpdated == null) {
			if (other.wubDateUpdated != null)
				return false;
		} else if (!wubDateUpdated.equals(other.wubDateUpdated))
			return false;
		if (wubIsActive != other.wubIsActive)
			return false;
		if (wubUpdatedBy == null) {
			if (other.wubUpdatedBy != null)
				return false;
		} else if (!wubUpdatedBy.equals(other.wubUpdatedBy))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtWrapUpBeginRules [wubBeginRuleKey=%s, wubIsActive=%s, wubDateCreated=%s, wubDateUpdated=%s, wubUpdatedBy=%s]",
				wubBeginRuleKey, wubIsActive, wubDateCreated, wubDateUpdated, wubUpdatedBy);
	}



	public CgtWrapUpBeginRulesDTO toDTO() {
		return new CgtWrapUpBeginRulesDTO(this);
	}

}
