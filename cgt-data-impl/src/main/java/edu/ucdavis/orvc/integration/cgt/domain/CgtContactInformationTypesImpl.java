package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtContactInformationTypes;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtContactInformationTypesDTO;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_ContactInformationTypes")
public class CgtContactInformationTypesImpl implements CgtContactInformationTypes {

	private static final long serialVersionUID = -7037977855168041090L;

	private int cntContactInfoTypeKey;
	private String cntName;

	public CgtContactInformationTypesImpl() {
	}

	public CgtContactInformationTypesImpl(final int cntContactInfoTypeKey, final String cntName) {
		this.cntContactInfoTypeKey = cntContactInfoTypeKey;
		this.cntName = cntName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformationTypes#getCntContactInfoTypeKey()
	 */
	@Override
	@Id
	@Column(name = "cnt_contactInfoTypeKey", unique = true, nullable = false)
	public int getCntContactInfoTypeKey() {
		return this.cntContactInfoTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformationTypes#setCntContactInfoTypeKey(int)
	 */
	@Override
	public void setCntContactInfoTypeKey(final int cntContactInfoTypeKey) {
		this.cntContactInfoTypeKey = cntContactInfoTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformationTypes#getCntName()
	 */
	@Override
	@Column(name = "cnt_name", nullable = false, length = 50)
	public String getCntName() {
		return this.cntName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtContactInformationTypes#setCntName(java.lang.String)
	 */
	@Override
	public void setCntName(final String cntName) {
		this.cntName = cntName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cntContactInfoTypeKey;
		result = prime * result + ((cntName == null) ? 0 : cntName.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof CgtContactInformationTypesImpl))
			return false;
		final CgtContactInformationTypesImpl other = (CgtContactInformationTypesImpl) obj;
		if (cntContactInfoTypeKey != other.cntContactInfoTypeKey)
			return false;
		if (cntName == null) {
			if (other.cntName != null)
				return false;
		} else if (!cntName.equals(other.cntName))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("CgtContactInformationTypes [cntContactInfoTypeKey=%s, cntName=%s]", cntContactInfoTypeKey,
				cntName);
	}

	public CgtContactInformationTypesDTO toDTO() {
		return new CgtContactInformationTypesDTO(this);
	}
}
