package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.MasStates;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.MasStatesDTO;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "MAS_States")
public class MasStatesImpl implements MasStates {

	private static final long serialVersionUID = 3662664716628551728L;
	private int staStateKey;
	private String staName;
	private String staShortName;
	private boolean staIsActive;
	private LocalDateTime staDateCreated;
	private LocalDateTime staDateUpdated;
	private String staUpdatedBy;

	public MasStatesImpl() {
	}

	public MasStatesImpl(final int staStateKey, final String staName, final String staShortName, final boolean staIsActive, final LocalDateTime staDateCreated,
			final LocalDateTime staDateUpdated, final String staUpdatedBy) {
		this.staStateKey = staStateKey;
		this.staName = staName;
		this.staShortName = staShortName;
		this.staIsActive = staIsActive;
		this.staDateCreated = staDateCreated;
		this.staDateUpdated = staDateUpdated;
		this.staUpdatedBy = staUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasStates#getStaStateKey()
	 */
	@Override
	@Id
	@Column(name = "sta_stateKey", unique = true, nullable = false)
	public int getStaStateKey() {
		return this.staStateKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasStates#setStaStateKey(int)
	 */
	@Override
	public void setStaStateKey(final int staStateKey) {
		this.staStateKey = staStateKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasStates#getStaName()
	 */
	@Override
	@Column(name = "sta_name", nullable = false)
	public String getStaName() {
		return this.staName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasStates#setStaName(java.lang.String)
	 */
	@Override
	public void setStaName(final String staName) {
		this.staName = staName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasStates#getStaShortName()
	 */
	@Override
	@Column(name = "sta_shortName", nullable = false, length = 2,columnDefinition="char")
	public String getStaShortName() {
		return this.staShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasStates#setStaShortName(java.lang.String)
	 */
	@Override
	public void setStaShortName(final String staShortName) {
		this.staShortName = staShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasStates#isStaIsActive()
	 */
	@Override
	@Column(name = "sta_isActive", nullable = false)
	public boolean isStaIsActive() {
		return this.staIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasStates#setStaIsActive(boolean)
	 */
	@Override
	public void setStaIsActive(final boolean staIsActive) {
		this.staIsActive = staIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasStates#getStaDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "sta_dateCreated", nullable = false, length = 23)
	public LocalDateTime getStaDateCreated() {
		return this.staDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasStates#setStaDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setStaDateCreated(final LocalDateTime staDateCreated) {
		this.staDateCreated = staDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasStates#getStaDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "sta_dateUpdated", nullable = false, length = 23)
	public LocalDateTime getStaDateUpdated() {
		return this.staDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasStates#setStaDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setStaDateUpdated(final LocalDateTime staDateUpdated) {
		this.staDateUpdated = staDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasStates#getStaUpdatedBy()
	 */
	@Override
	@Column(name = "sta_updatedBy", nullable = false, length = 35, columnDefinition="char")
	public String getStaUpdatedBy() {
		return this.staUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasStates#setStaUpdatedBy(java.lang.String)
	 */
	@Override
	public void setStaUpdatedBy(final String staUpdatedBy) {
		this.staUpdatedBy = staUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((staDateCreated == null) ? 0 : staDateCreated.hashCode());
		result = prime * result + ((staDateUpdated == null) ? 0 : staDateUpdated.hashCode());
		result = prime * result + (staIsActive ? 1231 : 1237);
		result = prime * result + ((staName == null) ? 0 : staName.hashCode());
		result = prime * result + ((staShortName == null) ? 0 : staShortName.hashCode());
		result = prime * result + staStateKey;
		result = prime * result + ((staUpdatedBy == null) ? 0 : staUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof MasStatesImpl))
			return false;
		final MasStatesImpl other = (MasStatesImpl) obj;
		if (staDateCreated == null) {
			if (other.staDateCreated != null)
				return false;
		} else if (!staDateCreated.equals(other.staDateCreated))
			return false;
		if (staDateUpdated == null) {
			if (other.staDateUpdated != null)
				return false;
		} else if (!staDateUpdated.equals(other.staDateUpdated))
			return false;
		if (staIsActive != other.staIsActive)
			return false;
		if (staName == null) {
			if (other.staName != null)
				return false;
		} else if (!staName.equals(other.staName))
			return false;
		if (staShortName == null) {
			if (other.staShortName != null)
				return false;
		} else if (!staShortName.equals(other.staShortName))
			return false;
		if (staStateKey != other.staStateKey)
			return false;
		if (staUpdatedBy == null) {
			if (other.staUpdatedBy != null)
				return false;
		} else if (!staUpdatedBy.equals(other.staUpdatedBy))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"MasStates [staStateKey=%s, staName=%s, staShortName=%s, staIsActive=%s, staDateCreated=%s, staDateUpdated=%s, staUpdatedBy=%s]",
				staStateKey, staName, staShortName, staIsActive, staDateCreated, staDateUpdated, staUpdatedBy);
	}



	public MasStatesDTO toDTO() {
		return new MasStatesDTO(this);
	}

}
