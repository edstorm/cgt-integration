package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Embeddable;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEDocsPickupHistoryId;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtEDocsPickupHistoryIdDTO;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Embeddable
public class CgtEDocsPickupHistoryIdImpl implements CgtEDocsPickupHistoryId {

	private static final long serialVersionUID = -6556884390114963859L;
	private String picEDocKey;
	private int picFileKey;
	private String picIpAddress;

	public CgtEDocsPickupHistoryIdImpl() {
	}

	public CgtEDocsPickupHistoryIdImpl(final String picEDocKey, final int picFileKey, final String picIpAddress) {
		this.picEDocKey = picEDocKey;
		this.picFileKey = picFileKey;
		this.picIpAddress = picIpAddress;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsPickupHistoryId#getPicEDocKey()
	 */
	@Override
	@Column(name = "pic_eDocKey", nullable = false, length = 35, columnDefinition="char")
	public String getPicEDocKey() {
		return this.picEDocKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsPickupHistoryId#setPicEDocKey(java.lang.String)
	 */
	@Override
	public void setPicEDocKey(final String picEDocKey) {
		this.picEDocKey = picEDocKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsPickupHistoryId#getPicFileKey()
	 */
	@Override
	@Column(name = "pic_fileKey", nullable = false)
	public int getPicFileKey() {
		return this.picFileKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsPickupHistoryId#setPicFileKey(int)
	 */
	@Override
	public void setPicFileKey(final int picFileKey) {
		this.picFileKey = picFileKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsPickupHistoryId#getPicIpAddress()
	 */
	@Override
	@Column(name = "pic_ipAddress", nullable = false, length = 15)
	public String getPicIpAddress() {
		return this.picIpAddress;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsPickupHistoryId#setPicIpAddress(java.lang.String)
	 */
	@Override
	public void setPicIpAddress(final String picIpAddress) {
		this.picIpAddress = picIpAddress;
	}

	@Override
	public boolean equals(final Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof CgtEDocsPickupHistoryIdImpl))
			return false;
		final CgtEDocsPickupHistoryId castOther = (CgtEDocsPickupHistoryId) other;

		return ((this.getPicEDocKey() == castOther.getPicEDocKey()) || (this.getPicEDocKey() != null
				&& castOther.getPicEDocKey() != null && this.getPicEDocKey().equals(castOther.getPicEDocKey())))
				&& (this.getPicFileKey() == castOther.getPicFileKey())
				&& ((this.getPicIpAddress() == castOther.getPicIpAddress())
						|| (this.getPicIpAddress() != null && castOther.getPicIpAddress() != null
						&& this.getPicIpAddress().equals(castOther.getPicIpAddress())));
	}

	@Override
	public int hashCode() {
		int result = 17;

		result = 37 * result + (getPicEDocKey() == null ? 0 : this.getPicEDocKey().hashCode());
		result = 37 * result + this.getPicFileKey();
		result = 37 * result + (getPicIpAddress() == null ? 0 : this.getPicIpAddress().hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("CgtEDocsPickupHistoryId [picEDocKey=%s, picFileKey=%s, picIpAddress=%s]", picEDocKey,
				picFileKey, picIpAddress);
	}

	public CgtEDocsPickupHistoryIdDTO toDTO() {
		return new CgtEDocsPickupHistoryIdDTO(this);
	}
}
