package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.MasUserPrefixes;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.MasUserPrefixesDTO;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "MAS_UserPrefixes")
public class MasUserPrefixesImpl implements MasUserPrefixes {

	private static final long serialVersionUID = -322461577288691040L;
	private int uspUserPrefixKey;
	private String uspName;
	private LocalDateTime uspDateCreated;
	private LocalDateTime uspDateUpdated;
	private String uspUpdatedBy;
	private boolean uspIsActive;

	public MasUserPrefixesImpl() {
	}

	public MasUserPrefixesImpl(final int uspUserPrefixKey, final String uspName, final LocalDateTime uspDateCreated, final LocalDateTime uspDateUpdated,
			final String uspUpdatedBy, final boolean uspIsActive) {
		this.uspUserPrefixKey = uspUserPrefixKey;
		this.uspName = uspName;
		this.uspDateCreated = uspDateCreated;
		this.uspDateUpdated = uspDateUpdated;
		this.uspUpdatedBy = uspUpdatedBy;
		this.uspIsActive = uspIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserPrefixes#getUspUserPrefixKey()
	 */
	@Override
	@Id
	@Column(name = "usp_userPrefixKey", unique = true, nullable = false)
	public int getUspUserPrefixKey() {
		return this.uspUserPrefixKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserPrefixes#setUspUserPrefixKey(int)
	 */
	@Override
	public void setUspUserPrefixKey(final int uspUserPrefixKey) {
		this.uspUserPrefixKey = uspUserPrefixKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserPrefixes#getUspName()
	 */
	@Override
	@Column(name = "usp_name", nullable = false, length = 50)
	public String getUspName() {
		return this.uspName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserPrefixes#setUspName(java.lang.String)
	 */
	@Override
	public void setUspName(final String uspName) {
		this.uspName = uspName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserPrefixes#getUspDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "usp_dateCreated", nullable = false, length = 23)
	public LocalDateTime getUspDateCreated() {
		return this.uspDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserPrefixes#setUspDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setUspDateCreated(final LocalDateTime uspDateCreated) {
		this.uspDateCreated = uspDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserPrefixes#getUspDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "usp_dateUpdated", nullable = false, length = 23)
	public LocalDateTime getUspDateUpdated() {
		return this.uspDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserPrefixes#setUspDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setUspDateUpdated(final LocalDateTime uspDateUpdated) {
		this.uspDateUpdated = uspDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserPrefixes#getUspUpdatedBy()
	 */
	@Override
	@Column(name = "usp_updatedBy", nullable = false, length = 35, columnDefinition="char")
	public String getUspUpdatedBy() {
		return this.uspUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserPrefixes#setUspUpdatedBy(java.lang.String)
	 */
	@Override
	public void setUspUpdatedBy(final String uspUpdatedBy) {
		this.uspUpdatedBy = uspUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserPrefixes#isUspIsActive()
	 */
	@Override
	@Column(name = "usp_isActive", nullable = false)
	public boolean isUspIsActive() {
		return this.uspIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUserPrefixes#setUspIsActive(boolean)
	 */
	@Override
	public void setUspIsActive(final boolean uspIsActive) {
		this.uspIsActive = uspIsActive;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((uspDateCreated == null) ? 0 : uspDateCreated.hashCode());
		result = prime * result + ((uspDateUpdated == null) ? 0 : uspDateUpdated.hashCode());
		result = prime * result + (uspIsActive ? 1231 : 1237);
		result = prime * result + ((uspName == null) ? 0 : uspName.hashCode());
		result = prime * result + ((uspUpdatedBy == null) ? 0 : uspUpdatedBy.hashCode());
		result = prime * result + uspUserPrefixKey;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof MasUserPrefixesImpl))
			return false;
		final MasUserPrefixesImpl other = (MasUserPrefixesImpl) obj;
		if (uspDateCreated == null) {
			if (other.uspDateCreated != null)
				return false;
		} else if (!uspDateCreated.equals(other.uspDateCreated))
			return false;
		if (uspDateUpdated == null) {
			if (other.uspDateUpdated != null)
				return false;
		} else if (!uspDateUpdated.equals(other.uspDateUpdated))
			return false;
		if (uspIsActive != other.uspIsActive)
			return false;
		if (uspName == null) {
			if (other.uspName != null)
				return false;
		} else if (!uspName.equals(other.uspName))
			return false;
		if (uspUpdatedBy == null) {
			if (other.uspUpdatedBy != null)
				return false;
		} else if (!uspUpdatedBy.equals(other.uspUpdatedBy))
			return false;
		if (uspUserPrefixKey != other.uspUserPrefixKey)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"MasUserPrefixes [uspUserPrefixKey=%s, uspName=%s, uspDateCreated=%s, uspDateUpdated=%s, uspUpdatedBy=%s, uspIsActive=%s]",
				uspUserPrefixKey, uspName, uspDateCreated, uspDateUpdated, uspUpdatedBy, uspIsActive);
	}



	public MasUserPrefixesDTO toDTO() {
		return new MasUserPrefixesDTO(this);
	}

}
