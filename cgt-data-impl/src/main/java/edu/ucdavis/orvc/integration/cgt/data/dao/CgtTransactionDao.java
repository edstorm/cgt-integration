package edu.ucdavis.orvc.integration.cgt.data.dao;

import java.util.List;

import org.joda.time.LocalDate;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEfaTransactionProcessInfo;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtSubmissionTypesEnum;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTransaction;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTransactionTypesEnum;
import edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionImpl;

public interface CgtTransactionDao {
	String FIELD_TRANSACTION_KEY = "ptrTransactionKey";
	String FIELD_PROJECT_NUMBER = "ptrProjectKey";
	String FIELD_TRANSACTION_TYPE_KEY = "ptrTransactionTypeKey";
	String FIELD_SUBMISSION_TYPE_KEY = "ptrSubmissionTypeKey";
	String FIELD_IS_ACTIVE = "ptrIsActive";
	String FIELD_DATE_CREATED = "ptrDateCreated";
	int CGA_TRANSACTION_TYPE_KEY = 3;
	int CGA_SUBMISSION_TYPE_KEY = 3;
	CgtTransactionTypesEnum CGA_TRANSACTION_TYPE = CgtTransactionTypesEnum.AWARD;
	CgtSubmissionTypesEnum CGA_SUBMISSION_TYPE = CgtSubmissionTypesEnum.AWARD_TYPES;
	LocalDate CGA_ELIGIBLE_START_DATE = new LocalDate(2011, 07, 20);

	List<CgtTransactionImpl> getProjectTransactionsByTypeAndSubmissionType(String projectNumber, int transactionTypeKey,
			int submissionTypeKey);

	List<CgtTransactionImpl> getProjectTransactionsByTypeAndSubmissionType(int transactionKey, String projectNumber, int transactionTypeKey,
			int submissionTypeKey);

	List<CgtTransactionImpl> getProjectTransactions(String projectNumber, int transactionTypeKey, int submissionTypeKey,
			boolean active, LocalDate createdAfterDt);

	List<CgtTransactionImpl> findFeedEligibleTransactions(String projectNumber);

	List<CgtTransactionImpl> findCgaProcessedTransactions(String projectNumber);

	List<CgtTransactionImpl> findCgaPendingTransactions(String projectNumber);
	
	List<CgtEfaTransactionProcessInfo> findAllEfaProcessInfo(String projectNumber);

	List<CgtTransactionImpl> findUnprocessedFeedEligibleTransactions(final int maxDaysOld);

	void storeCgtTransactionImpl(CgtTransaction transaction);

}