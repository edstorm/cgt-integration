package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTransactionSubmissionType;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTransactionSubmissionTypeId;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtTransactionSubmissionTypeDTO;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_TransactionSubmissionTypes")
public class CgtTransactionSubmissionTypeImpl implements CgtTransactionSubmissionType {

	private static final long serialVersionUID = -8303019222535285384L;
	
	private CgtTransactionSubmissionTypeIdImpl id;
	private String tstName;
	private String tstShortName;
	private Integer tstSortOrder;
	private Boolean tstIsVisible;
	private Boolean tstIsClosingType;
	private Boolean tstIsAutoCloseType;
	private Boolean tstIsEmailTriggerType;
	private Boolean tstIsSurveyTriggerType;
	private boolean tstValidateTransaction;
	private boolean tstIsActive;
	private String tstUpdatedBy;
	private LocalDateTime tstDateUpdated;
	private LocalDateTime tstDateCreated;

	public CgtTransactionSubmissionTypeImpl() {
	}

	public CgtTransactionSubmissionTypeImpl(final CgtTransactionSubmissionTypeIdImpl id, final String tstName,
			final boolean tstValidateTransaction, final boolean tstIsActive, final String tstUpdatedBy, final LocalDateTime tstDateUpdated,
			final LocalDateTime tstDateCreated) {
		this.id = id;
		this.tstName = tstName;
		this.tstValidateTransaction = tstValidateTransaction;
		this.tstIsActive = tstIsActive;
		this.tstUpdatedBy = tstUpdatedBy;
		this.tstDateUpdated = tstDateUpdated;
		this.tstDateCreated = tstDateCreated;
	}

	public CgtTransactionSubmissionTypeImpl(final CgtTransactionSubmissionTypeIdImpl id, final String tstName, final String tstShortName,
			final Integer tstSortOrder, final Boolean tstIsVisible, final Boolean tstIsClosingType, final Boolean tstIsAutoCloseType,
			final Boolean tstIsEmailTriggerType, final Boolean tstIsSurveyTriggerType, final boolean tstValidateTransaction,
			final boolean tstIsActive, final String tstUpdatedBy, final LocalDateTime tstDateUpdated, final LocalDateTime tstDateCreated) {
		this.id = id;
		this.tstName = tstName;
		this.tstShortName = tstShortName;
		this.tstSortOrder = tstSortOrder;
		this.tstIsVisible = tstIsVisible;
		this.tstIsClosingType = tstIsClosingType;
		this.tstIsAutoCloseType = tstIsAutoCloseType;
		this.tstIsEmailTriggerType = tstIsEmailTriggerType;
		this.tstIsSurveyTriggerType = tstIsSurveyTriggerType;
		this.tstValidateTransaction = tstValidateTransaction;
		this.tstIsActive = tstIsActive;
		this.tstUpdatedBy = tstUpdatedBy;
		this.tstDateUpdated = tstDateUpdated;
		this.tstDateCreated = tstDateCreated;
	}
	
	public CgtTransactionSubmissionTypeImpl(CgtTransactionSubmissionType fromObj) {
		this.id = new CgtTransactionSubmissionTypeIdImpl(fromObj.getId());
		this.tstName = fromObj.getTstName();
		this.tstShortName = fromObj.getTstShortName();
		this.tstSortOrder = fromObj.getTstSortOrder();
		this.tstIsVisible = fromObj.getTstIsVisible();
		this.tstIsClosingType = fromObj.getTstIsClosingType();
		this.tstIsAutoCloseType = fromObj.getTstIsAutoCloseType();
		this.tstIsEmailTriggerType = fromObj.getTstIsEmailTriggerType();
		this.tstIsSurveyTriggerType = fromObj.getTstIsSurveyTriggerType();
		this.tstValidateTransaction = fromObj.isTstValidateTransaction();
		this.tstIsActive = fromObj.isTstIsActive();
		this.tstUpdatedBy = fromObj.getTstUpdatedBy();
		this.tstDateUpdated = fromObj.getTstDateUpdated();
		this.tstDateCreated = fromObj.getTstDateCreated();
	}

	@EmbeddedId
	@AttributeOverrides({
		@AttributeOverride(name = "tstTransactionSubmissionTypeKey", column = @Column(name = "tst_transactionSubmissionTypeKey", nullable = false) ),
		@AttributeOverride(name = "tstTransactionTypeKey", column = @Column(name = "tst_transactionTypeKey", nullable = false) ) })
	public CgtTransactionSubmissionTypeIdImpl getIdImpl() {
		return this.id;
	}

	public void setIdImpl(final CgtTransactionSubmissionTypeIdImpl id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionSubmissionType#getTstName()
	 */
	@Override
	@Column(name = "tst_name", nullable = false)
	public String getTstName() {
		return this.tstName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionSubmissionType#setTstName(java.lang.String)
	 */
	@Override
	public void setTstName(final String tstName) {
		this.tstName = tstName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionSubmissionType#getTstShortName()
	 */
	@Override
	@Column(name = "tst_shortName")
	public String getTstShortName() {
		return this.tstShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionSubmissionType#setTstShortName(java.lang.String)
	 */
	@Override
	public void setTstShortName(final String tstShortName) {
		this.tstShortName = tstShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionSubmissionType#getTstSortOrder()
	 */
	@Override
	@Column(name = "tst_sortOrder")
	public Integer getTstSortOrder() {
		return this.tstSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionSubmissionType#setTstSortOrder(java.lang.Integer)
	 */
	@Override
	public void setTstSortOrder(final Integer tstSortOrder) {
		this.tstSortOrder = tstSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionSubmissionType#getTstIsVisible()
	 */
	@Override
	@Column(name = "tst_isVisible")
	public Boolean getTstIsVisible() {
		return this.tstIsVisible;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionSubmissionType#setTstIsVisible(java.lang.Boolean)
	 */
	@Override
	public void setTstIsVisible(final Boolean tstIsVisible) {
		this.tstIsVisible = tstIsVisible;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionSubmissionType#getTstIsClosingType()
	 */
	@Override
	@Column(name = "tst_isClosingType")
	public Boolean getTstIsClosingType() {
		return this.tstIsClosingType;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionSubmissionType#setTstIsClosingType(java.lang.Boolean)
	 */
	@Override
	public void setTstIsClosingType(final Boolean tstIsClosingType) {
		this.tstIsClosingType = tstIsClosingType;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionSubmissionType#getTstIsAutoCloseType()
	 */
	@Override
	@Column(name = "tst_isAutoCloseType")
	public Boolean getTstIsAutoCloseType() {
		return this.tstIsAutoCloseType;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionSubmissionType#setTstIsAutoCloseType(java.lang.Boolean)
	 */
	@Override
	public void setTstIsAutoCloseType(final Boolean tstIsAutoCloseType) {
		this.tstIsAutoCloseType = tstIsAutoCloseType;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionSubmissionType#getTstIsEmailTriggerType()
	 */
	@Override
	@Column(name = "tst_isEmailTriggerType")
	public Boolean getTstIsEmailTriggerType() {
		return this.tstIsEmailTriggerType;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionSubmissionType#setTstIsEmailTriggerType(java.lang.Boolean)
	 */
	@Override
	public void setTstIsEmailTriggerType(final Boolean tstIsEmailTriggerType) {
		this.tstIsEmailTriggerType = tstIsEmailTriggerType;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionSubmissionType#getTstIsSurveyTriggerType()
	 */
	@Override
	@Column(name = "tst_isSurveyTriggerType")
	public Boolean getTstIsSurveyTriggerType() {
		return this.tstIsSurveyTriggerType;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionSubmissionType#setTstIsSurveyTriggerType(java.lang.Boolean)
	 */
	@Override
	public void setTstIsSurveyTriggerType(final Boolean tstIsSurveyTriggerType) {
		this.tstIsSurveyTriggerType = tstIsSurveyTriggerType;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionSubmissionType#isTstValidateTransaction()
	 */
	@Override
	@Column(name = "tst_validateTransaction", nullable = false)
	public boolean isTstValidateTransaction() {
		return this.tstValidateTransaction;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionSubmissionType#setTstValidateTransaction(boolean)
	 */
	@Override
	public void setTstValidateTransaction(final boolean tstValidateTransaction) {
		this.tstValidateTransaction = tstValidateTransaction;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionSubmissionType#isTstIsActive()
	 */
	@Override
	@Column(name = "tst_isActive", nullable = false)
	public boolean isTstIsActive() {
		return this.tstIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionSubmissionType#setTstIsActive(boolean)
	 */
	@Override
	public void setTstIsActive(final boolean tstIsActive) {
		this.tstIsActive = tstIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionSubmissionType#getTstUpdatedBy()
	 */
	@Override
	@Column(name = "tst_updatedBy", nullable = false, length = 35, columnDefinition="char")
	public String getTstUpdatedBy() {
		return this.tstUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionSubmissionType#setTstUpdatedBy(java.lang.String)
	 */
	@Override
	public void setTstUpdatedBy(final String tstUpdatedBy) {
		this.tstUpdatedBy = tstUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionSubmissionType#getTstDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "tst_dateUpdated", nullable = false, length = 23)
	public LocalDateTime getTstDateUpdated() {
		return this.tstDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionSubmissionType#setTstDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setTstDateUpdated(final LocalDateTime tstDateUpdated) {
		this.tstDateUpdated = tstDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionSubmissionType#getTstDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "tst_dateCreated", nullable = false, length = 23)
	public LocalDateTime getTstDateCreated() {
		return this.tstDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionSubmissionType#setTstDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setTstDateCreated(final LocalDateTime tstDateCreated) {
		this.tstDateCreated = tstDateCreated;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((tstDateCreated == null) ? 0 : tstDateCreated.hashCode());
		result = prime * result + ((tstDateUpdated == null) ? 0 : tstDateUpdated.hashCode());
		result = prime * result + (tstIsActive ? 1231 : 1237);
		result = prime * result + ((tstIsAutoCloseType == null) ? 0 : tstIsAutoCloseType.hashCode());
		result = prime * result + ((tstIsClosingType == null) ? 0 : tstIsClosingType.hashCode());
		result = prime * result + ((tstIsEmailTriggerType == null) ? 0 : tstIsEmailTriggerType.hashCode());
		result = prime * result + ((tstIsSurveyTriggerType == null) ? 0 : tstIsSurveyTriggerType.hashCode());
		result = prime * result + ((tstIsVisible == null) ? 0 : tstIsVisible.hashCode());
		result = prime * result + ((tstName == null) ? 0 : tstName.hashCode());
		result = prime * result + ((tstShortName == null) ? 0 : tstShortName.hashCode());
		result = prime * result + ((tstSortOrder == null) ? 0 : tstSortOrder.hashCode());
		result = prime * result + ((tstUpdatedBy == null) ? 0 : tstUpdatedBy.hashCode());
		result = prime * result + (tstValidateTransaction ? 1231 : 1237);
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CgtTransactionSubmissionTypeImpl))
			return false;
		final CgtTransactionSubmissionTypeImpl other = (CgtTransactionSubmissionTypeImpl) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (tstDateCreated == null) {
			if (other.tstDateCreated != null)
				return false;
		} else if (!tstDateCreated.equals(other.tstDateCreated))
			return false;
		if (tstDateUpdated == null) {
			if (other.tstDateUpdated != null)
				return false;
		} else if (!tstDateUpdated.equals(other.tstDateUpdated))
			return false;
		if (tstIsActive != other.tstIsActive)
			return false;
		if (tstIsAutoCloseType == null) {
			if (other.tstIsAutoCloseType != null)
				return false;
		} else if (!tstIsAutoCloseType.equals(other.tstIsAutoCloseType))
			return false;
		if (tstIsClosingType == null) {
			if (other.tstIsClosingType != null)
				return false;
		} else if (!tstIsClosingType.equals(other.tstIsClosingType))
			return false;
		if (tstIsEmailTriggerType == null) {
			if (other.tstIsEmailTriggerType != null)
				return false;
		} else if (!tstIsEmailTriggerType.equals(other.tstIsEmailTriggerType))
			return false;
		if (tstIsSurveyTriggerType == null) {
			if (other.tstIsSurveyTriggerType != null)
				return false;
		} else if (!tstIsSurveyTriggerType.equals(other.tstIsSurveyTriggerType))
			return false;
		if (tstIsVisible == null) {
			if (other.tstIsVisible != null)
				return false;
		} else if (!tstIsVisible.equals(other.tstIsVisible))
			return false;
		if (tstName == null) {
			if (other.tstName != null)
				return false;
		} else if (!tstName.equals(other.tstName))
			return false;
		if (tstShortName == null) {
			if (other.tstShortName != null)
				return false;
		} else if (!tstShortName.equals(other.tstShortName))
			return false;
		if (tstSortOrder == null) {
			if (other.tstSortOrder != null)
				return false;
		} else if (!tstSortOrder.equals(other.tstSortOrder))
			return false;
		if (tstUpdatedBy == null) {
			if (other.tstUpdatedBy != null)
				return false;
		} else if (!tstUpdatedBy.equals(other.tstUpdatedBy))
			return false;
		if (tstValidateTransaction != other.tstValidateTransaction)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtTransactionSubmissionTypes [id=%s, tstName=%s, tstShortName=%s, tstSortOrder=%s, tstIsVisible=%s, tstIsClosingType=%s, tstIsAutoCloseType=%s, tstIsEmailTriggerType=%s, tstIsSurveyTriggerType=%s, tstValidateTransaction=%s, tstIsActive=%s, tstUpdatedBy=%s, tstDateUpdated=%s, tstDateCreated=%s]",
				id, tstName, tstShortName, tstSortOrder, tstIsVisible, tstIsClosingType, tstIsAutoCloseType,
				tstIsEmailTriggerType, tstIsSurveyTriggerType, tstValidateTransaction, tstIsActive, tstUpdatedBy,
				tstDateUpdated, tstDateCreated);
	}

	@Override
	@Transient
	public CgtTransactionSubmissionTypeId getId() {
		return id;
	}

	@Override
	public void setId(CgtTransactionSubmissionTypeId id) {
		this.id = (CgtTransactionSubmissionTypeIdImpl) id;
	}

	@Override
	public CgtTransactionSubmissionTypeDTO toDTO() {
		// TODO Auto-generated method stub
		return null;
	}

}
