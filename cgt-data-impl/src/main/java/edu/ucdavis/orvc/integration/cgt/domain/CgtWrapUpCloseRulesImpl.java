package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtWrapUpCloseRules;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtWrapUpCloseRulesDTO;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_WrapUpCloseRules")
public class CgtWrapUpCloseRulesImpl implements CgtWrapUpCloseRules {

	private static final long serialVersionUID = -3575915034964773653L;
	private int wucCloseRuleKey;
	private int wucBeginRuleKey;
	private Integer wucResultSubmissionTypeKey;
	private boolean wucIsActive;
	private LocalDateTime wucDateCreated;
	private LocalDateTime wucDateUpdated;
	private String wucUpdatedBy;

	public CgtWrapUpCloseRulesImpl() {
	}

	public CgtWrapUpCloseRulesImpl(final int wucCloseRuleKey, final int wucBeginRuleKey, final boolean wucIsActive, final LocalDateTime wucDateCreated,
			final LocalDateTime wucDateUpdated, final String wucUpdatedBy) {
		this.wucCloseRuleKey = wucCloseRuleKey;
		this.wucBeginRuleKey = wucBeginRuleKey;
		this.wucIsActive = wucIsActive;
		this.wucDateCreated = wucDateCreated;
		this.wucDateUpdated = wucDateUpdated;
		this.wucUpdatedBy = wucUpdatedBy;
	}

	public CgtWrapUpCloseRulesImpl(final int wucCloseRuleKey, final int wucBeginRuleKey, final Integer wucResultSubmissionTypeKey,
			final boolean wucIsActive, final LocalDateTime wucDateCreated, final LocalDateTime wucDateUpdated, final String wucUpdatedBy) {
		this.wucCloseRuleKey = wucCloseRuleKey;
		this.wucBeginRuleKey = wucBeginRuleKey;
		this.wucResultSubmissionTypeKey = wucResultSubmissionTypeKey;
		this.wucIsActive = wucIsActive;
		this.wucDateCreated = wucDateCreated;
		this.wucDateUpdated = wucDateUpdated;
		this.wucUpdatedBy = wucUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRules#getWucCloseRuleKey()
	 */
	@Override
	@Id
	@Column(name = "wuc_closeRuleKey", unique = true, nullable = false)
	public int getWucCloseRuleKey() {
		return this.wucCloseRuleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRules#setWucCloseRuleKey(int)
	 */
	@Override
	public void setWucCloseRuleKey(final int wucCloseRuleKey) {
		this.wucCloseRuleKey = wucCloseRuleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRules#getWucBeginRuleKey()
	 */
	@Override
	@Column(name = "wuc_beginRuleKey", nullable = false)
	public int getWucBeginRuleKey() {
		return this.wucBeginRuleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRules#setWucBeginRuleKey(int)
	 */
	@Override
	public void setWucBeginRuleKey(final int wucBeginRuleKey) {
		this.wucBeginRuleKey = wucBeginRuleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRules#getWucResultSubmissionTypeKey()
	 */
	@Override
	@Column(name = "wuc_resultSubmissionTypeKey")
	public Integer getWucResultSubmissionTypeKey() {
		return this.wucResultSubmissionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRules#setWucResultSubmissionTypeKey(java.lang.Integer)
	 */
	@Override
	public void setWucResultSubmissionTypeKey(final Integer wucResultSubmissionTypeKey) {
		this.wucResultSubmissionTypeKey = wucResultSubmissionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRules#isWucIsActive()
	 */
	@Override
	@Column(name = "wuc_isActive", nullable = false)
	public boolean isWucIsActive() {
		return this.wucIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRules#setWucIsActive(boolean)
	 */
	@Override
	public void setWucIsActive(final boolean wucIsActive) {
		this.wucIsActive = wucIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRules#getWucDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "wuc_dateCreated", nullable = false, length = 23)
	public LocalDateTime getWucDateCreated() {
		return this.wucDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRules#setWucDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setWucDateCreated(final LocalDateTime wucDateCreated) {
		this.wucDateCreated = wucDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRules#getWucDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "wuc_dateUpdated", nullable = false, length = 23)
	public LocalDateTime getWucDateUpdated() {
		return this.wucDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRules#setWucDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setWucDateUpdated(final LocalDateTime wucDateUpdated) {
		this.wucDateUpdated = wucDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRules#getWucUpdatedBy()
	 */
	@Override
	@Column(name = "wuc_updatedBy", nullable = false, length = 35, columnDefinition="char")
	public String getWucUpdatedBy() {
		return this.wucUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRules#setWucUpdatedBy(java.lang.String)
	 */
	@Override
	public void setWucUpdatedBy(final String wucUpdatedBy) {
		this.wucUpdatedBy = wucUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + wucBeginRuleKey;
		result = prime * result + wucCloseRuleKey;
		result = prime * result + ((wucDateCreated == null) ? 0 : wucDateCreated.hashCode());
		result = prime * result + ((wucDateUpdated == null) ? 0 : wucDateUpdated.hashCode());
		result = prime * result + (wucIsActive ? 1231 : 1237);
		result = prime * result + ((wucResultSubmissionTypeKey == null) ? 0 : wucResultSubmissionTypeKey.hashCode());
		result = prime * result + ((wucUpdatedBy == null) ? 0 : wucUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CgtWrapUpCloseRulesImpl))
			return false;
		final CgtWrapUpCloseRulesImpl other = (CgtWrapUpCloseRulesImpl) obj;
		if (wucBeginRuleKey != other.wucBeginRuleKey)
			return false;
		if (wucCloseRuleKey != other.wucCloseRuleKey)
			return false;
		if (wucDateCreated == null) {
			if (other.wucDateCreated != null)
				return false;
		} else if (!wucDateCreated.equals(other.wucDateCreated))
			return false;
		if (wucDateUpdated == null) {
			if (other.wucDateUpdated != null)
				return false;
		} else if (!wucDateUpdated.equals(other.wucDateUpdated))
			return false;
		if (wucIsActive != other.wucIsActive)
			return false;
		if (wucResultSubmissionTypeKey == null) {
			if (other.wucResultSubmissionTypeKey != null)
				return false;
		} else if (!wucResultSubmissionTypeKey.equals(other.wucResultSubmissionTypeKey))
			return false;
		if (wucUpdatedBy == null) {
			if (other.wucUpdatedBy != null)
				return false;
		} else if (!wucUpdatedBy.equals(other.wucUpdatedBy))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtWrapUpCloseRules [wucCloseRuleKey=%s, wucBeginRuleKey=%s, wucResultSubmissionTypeKey=%s, wucIsActive=%s, wucDateCreated=%s, wucDateUpdated=%s, wucUpdatedBy=%s]",
				wucCloseRuleKey, wucBeginRuleKey, wucResultSubmissionTypeKey, wucIsActive, wucDateCreated,
				wucDateUpdated, wucUpdatedBy);
	}



	public CgtWrapUpCloseRulesDTO toDTO() {
		return new CgtWrapUpCloseRulesDTO(this);
	}

}
