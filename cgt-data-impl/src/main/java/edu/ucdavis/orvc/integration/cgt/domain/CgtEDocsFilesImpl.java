package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEDocsFiles;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtEDocsFilesDTO;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_eDocsFiles")
public class CgtEDocsFilesImpl implements CgtEDocsFiles {

	private static final long serialVersionUID = 8664989096509433082L;
	private Integer filFileKey;
	private String filOriginalFileName;
	private String filRelativePath;
	private String filFileName;
	private String filFileExtension;
	private String filExtension;
	private String filMimeType;
	private int filFileSize;
	private String filFilePath;
	private String filDescription;
	private String filContentAsText;
	private boolean filIsActive;
	private LocalDateTime filDateCreated;
	private LocalDateTime filDateUpdated;
	private String filUpdatedBy;

	public CgtEDocsFilesImpl() {
		super();
	}
	
	public CgtEDocsFilesImpl(CgtEDocsFiles model) {
		this( model.getFilFileKey()
				, model.getFilOriginalFileName()
				, model.getFilRelativePath()
				, model.getFilFileName()
				, model.getFilFileExtension()
				, model.getFilExtension()
				, model.getFilMimeType()
				, model.getFilFileSize()
				, model.getFilFilePath()
				, model.getFilDescription()
				, model.getFilContentAsText()
				, model.isFilIsActive()
				, model.getFilDateCreated()
				, model.getFilDateUpdated()
				, model.getFilUpdatedBy());
	}

	public CgtEDocsFilesImpl(final int filFileKey, final String filOriginalFileName, final String filRelativePath, final String filFileName,
			final int filFileSize, final boolean filIsActive, final LocalDateTime filDateCreated, final LocalDateTime filDateUpdated, final String filUpdatedBy) {
		this();
		this.filFileKey = filFileKey;
		this.filOriginalFileName = filOriginalFileName;
		this.filRelativePath = filRelativePath;
		this.filFileName = filFileName;
		this.filFileSize = filFileSize;
		this.filIsActive = filIsActive;
		this.filDateCreated = filDateCreated;
		this.filDateUpdated = filDateUpdated;
		this.filUpdatedBy = filUpdatedBy;
	}

	public CgtEDocsFilesImpl(final int filFileKey, final String filOriginalFileName, final String filRelativePath, final String filFileName,
			final String filFileExtension, final String filExtension, final String filMimeType, final int filFileSize, final String filFilePath,
			final String filDescription, final String filContentAsText, final boolean filIsActive, final LocalDateTime filDateCreated,
			final LocalDateTime filDateUpdated, final String filUpdatedBy) {
		this();
		this.filFileKey = filFileKey;
		this.filOriginalFileName = filOriginalFileName;
		this.filRelativePath = filRelativePath;
		this.filFileName = filFileName;
		this.filFileExtension = filFileExtension;
		this.filExtension = filExtension;
		this.filMimeType = filMimeType;
		this.filFileSize = filFileSize;
		this.filFilePath = filFilePath;
		this.filDescription = filDescription;
		this.filContentAsText = filContentAsText;
		this.filIsActive = filIsActive;
		this.filDateCreated = filDateCreated;
		this.filDateUpdated = filDateUpdated;
		this.filUpdatedBy = filUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFiles#getFilFileKey()
	 */
	@Override
	@Id
	@Column(name = "fil_fileKey", unique = true, nullable = false)
	public Integer getFilFileKey() {
		return this.filFileKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFiles#setFilFileKey(int)
	 */
	@Override
	public void setFilFileKey(final Integer filFileKey) {
		this.filFileKey = filFileKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFiles#getFilOriginalFileName()
	 */
	@Override
	@Column(name = "fil_originalFileName", nullable = false, length = 500)
	public String getFilOriginalFileName() {
		return this.filOriginalFileName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFiles#setFilOriginalFileName(java.lang.String)
	 */
	@Override
	public void setFilOriginalFileName(final String filOriginalFileName) {
		this.filOriginalFileName = filOriginalFileName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFiles#getFilRelativePath()
	 */
	@Override
	@Column(name = "fil_relativePath", nullable = false, length = 500)
	public String getFilRelativePath() {
		return this.filRelativePath;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFiles#setFilRelativePath(java.lang.String)
	 */
	@Override
	public void setFilRelativePath(final String filRelativePath) {
		this.filRelativePath = filRelativePath;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFiles#getFilFileName()
	 */
	@Override
	@Column(name = "fil_fileName", nullable = false, length = 500)
	public String getFilFileName() {
		return this.filFileName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFiles#setFilFileName(java.lang.String)
	 */
	@Override
	public void setFilFileName(final String filFileName) {
		this.filFileName = filFileName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFiles#getFilFileExtension()
	 */
	@Override
	@Column(name = "fil_fileExtension", length = 500 )
	public String getFilFileExtension() {
		return this.filFileExtension;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFiles#setFilFileExtension(java.lang.String)
	 */
	@Override
	public void setFilFileExtension(final String filFileExtension) {
		this.filFileExtension = filFileExtension;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFiles#getFilExtension()
	 */
	@Override
	@Column(name = "fil_extension", length = 10)
	public String getFilExtension() {
		return this.filExtension;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFiles#setFilExtension(java.lang.String)
	 */
	@Override
	public void setFilExtension(final String filExtension) {
		this.filExtension = filExtension;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFiles#getFilMimeType()
	 */
	@Override
	@Column(name = "fil_mimeType", length = 200)
	public String getFilMimeType() {
		return this.filMimeType;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFiles#setFilMimeType(java.lang.String)
	 */
	@Override
	public void setFilMimeType(final String filMimeType) {
		this.filMimeType = filMimeType;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFiles#getFilFileSize()
	 */
	@Override
	@Column(name = "fil_fileSize", nullable = false)
	public int getFilFileSize() {
		return this.filFileSize;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFiles#setFilFileSize(int)
	 */
	@Override
	public void setFilFileSize(final int filFileSize) {
		this.filFileSize = filFileSize;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFiles#getFilFilePath()
	 */
	@Override
	@Column(name = "fil_filePath", length = 1012)
	public String getFilFilePath() {
		return this.filFilePath;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFiles#setFilFilePath(java.lang.String)
	 */
	@Override
	public void setFilFilePath(final String filFilePath) {
		this.filFilePath = filFilePath;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFiles#getFilDescription()
	 */
	@Override
	@Column(name = "fil_description", length = 8000)
	public String getFilDescription() {
		return this.filDescription;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFiles#setFilDescription(java.lang.String)
	 */
	@Override
	public void setFilDescription(final String filDescription) {
		this.filDescription = filDescription;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFiles#getFilContentAsText()
	 */
	@Override
	@Column(name = "fil_contentAsText")
	public String getFilContentAsText() {
		return this.filContentAsText;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFiles#setFilContentAsText(java.lang.String)
	 */
	@Override
	public void setFilContentAsText(final String filContentAsText) {
		this.filContentAsText = filContentAsText;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFiles#isFilIsActive()
	 */
	@Override
	@Column(name = "fil_isActive", nullable = false)
	public boolean isFilIsActive() {
		return this.filIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFiles#setFilIsActive(boolean)
	 */
	@Override
	public void setFilIsActive(final boolean filIsActive) {
		this.filIsActive = filIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFiles#getFilDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "fil_dateCreated", nullable = false, length = 23)
	public LocalDateTime getFilDateCreated() {
		return this.filDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFiles#setFilDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setFilDateCreated(final LocalDateTime filDateCreated) {
		this.filDateCreated = filDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFiles#getFilDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "fil_dateUpdated", nullable = false, length = 23)
	public LocalDateTime getFilDateUpdated() {
		return this.filDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFiles#setFilDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setFilDateUpdated(final LocalDateTime filDateUpdated) {
		this.filDateUpdated = filDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFiles#getFilUpdatedBy()
	 */
	@Override
	@Column(name = "fil_updatedBy", nullable = false, length = 35, columnDefinition="char")
	public String getFilUpdatedBy() {
		return this.filUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFiles#setFilUpdatedBy(java.lang.String)
	 */
	@Override
	public void setFilUpdatedBy(final String filUpdatedBy) {
		this.filUpdatedBy = filUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((filContentAsText == null) ? 0 : filContentAsText.hashCode());
		result = prime * result + ((filDateCreated == null) ? 0 : filDateCreated.hashCode());
		result = prime * result + ((filDateUpdated == null) ? 0 : filDateUpdated.hashCode());
		result = prime * result + ((filDescription == null) ? 0 : filDescription.hashCode());
		result = prime * result + ((filExtension == null) ? 0 : filExtension.hashCode());
		result = prime * result + ((filFileExtension == null) ? 0 : filFileExtension.hashCode());
		result = prime * result + ((filFileKey == null) ? 0 : filFileKey.hashCode());
		result = prime * result + ((filFileName == null) ? 0 : filFileName.hashCode());
		result = prime * result + ((filFilePath == null) ? 0 : filFilePath.hashCode());
		result = prime * result + filFileSize;
		result = prime * result + (filIsActive ? 1231 : 1237);
		result = prime * result + ((filMimeType == null) ? 0 : filMimeType.hashCode());
		result = prime * result + ((filOriginalFileName == null) ? 0 : filOriginalFileName.hashCode());
		result = prime * result + ((filRelativePath == null) ? 0 : filRelativePath.hashCode());
		result = prime * result + ((filUpdatedBy == null) ? 0 : filUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CgtEDocsFilesImpl other = (CgtEDocsFilesImpl) obj;
		if (filContentAsText == null) {
			if (other.filContentAsText != null) {
				return false;
			}
		} else if (!filContentAsText.equals(other.filContentAsText)) {
			return false;
		}
		if (filDateCreated == null) {
			if (other.filDateCreated != null) {
				return false;
			}
		} else if (!filDateCreated.equals(other.filDateCreated)) {
			return false;
		}
		if (filDateUpdated == null) {
			if (other.filDateUpdated != null) {
				return false;
			}
		} else if (!filDateUpdated.equals(other.filDateUpdated)) {
			return false;
		}
		if (filDescription == null) {
			if (other.filDescription != null) {
				return false;
			}
		} else if (!filDescription.equals(other.filDescription)) {
			return false;
		}
		if (filExtension == null) {
			if (other.filExtension != null) {
				return false;
			}
		} else if (!filExtension.equals(other.filExtension)) {
			return false;
		}
		if (filFileExtension == null) {
			if (other.filFileExtension != null) {
				return false;
			}
		} else if (!filFileExtension.equals(other.filFileExtension)) {
			return false;
		}
		if (filFileKey == null) {
			if (other.filFileKey != null) {
				return false;
			}
		} else if (!filFileKey.equals(other.filFileKey)) {
			return false;
		}
		if (filFileName == null) {
			if (other.filFileName != null) {
				return false;
			}
		} else if (!filFileName.equals(other.filFileName)) {
			return false;
		}
		if (filFilePath == null) {
			if (other.filFilePath != null) {
				return false;
			}
		} else if (!filFilePath.equals(other.filFilePath)) {
			return false;
		}
		if (filFileSize != other.filFileSize) {
			return false;
		}
		if (filIsActive != other.filIsActive) {
			return false;
		}
		if (filMimeType == null) {
			if (other.filMimeType != null) {
				return false;
			}
		} else if (!filMimeType.equals(other.filMimeType)) {
			return false;
		}
		if (filOriginalFileName == null) {
			if (other.filOriginalFileName != null) {
				return false;
			}
		} else if (!filOriginalFileName.equals(other.filOriginalFileName)) {
			return false;
		}
		if (filRelativePath == null) {
			if (other.filRelativePath != null) {
				return false;
			}
		} else if (!filRelativePath.equals(other.filRelativePath)) {
			return false;
		}
		if (filUpdatedBy == null) {
			if (other.filUpdatedBy != null) {
				return false;
			}
		} else if (!filUpdatedBy.equals(other.filUpdatedBy)) {
			return false;
		}
		return true;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtEDocsFilesImpl [filFileKey=%s, filOriginalFileName=%s, filRelativePath=%s, filFileName=%s, filFileExtension=%s, filExtension=%s, filMimeType=%s, filFileSize=%s, filFilePath=%s, filDescription=%s, filContentAsText=%s, filIsActive=%s, filDateCreated=%s, filDateUpdated=%s, filUpdatedBy=%s]",
				filFileKey, filOriginalFileName, filRelativePath, filFileName, filFileExtension, filExtension,
				filMimeType, filFileSize, filFilePath, filDescription, filContentAsText, filIsActive, filDateCreated,
				filDateUpdated, filUpdatedBy);
	}

	public CgtEDocsFilesDTO toDTO() {
		return new CgtEDocsFilesDTO(this);
	}
	
}
