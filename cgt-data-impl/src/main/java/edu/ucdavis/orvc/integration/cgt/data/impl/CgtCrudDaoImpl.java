package edu.ucdavis.orvc.integration.cgt.data.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.google.common.collect.ImmutableList;

import edu.ucdavis.orvc.core.data.CrudDao;
import edu.ucdavis.orvc.core.data.CrudExpressionQueryContext;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtBaseInterface;



/**
 * @author edstorm
 * 
 * Standard CRUD DAO Impl class to extend from. 
 * 
 */
public abstract class CgtCrudDaoImpl<T extends CgtBaseInterface<Q>, Id extends Serializable, Q extends I, I extends CgtBaseInterface<Q>> implements CrudDao<T, Id> {
	
	@Autowired
	@Qualifier("cgtDataSessionFactory")
    protected SessionFactory sessionFactory;
    protected Class<T> persistentClass;
    protected SessionFactory getSessionFactory() { return sessionFactory; }
    
    private static final String CONSTRAINT_STR_FORMAT = "[%s = %s]";
    
    private Logger logger = LoggerFactory.getLogger(
    		String.format("%s.<%s>", CgtCrudDaoImpl.class.getCanonicalName(),getPersistentClass())
    		);

    
    @Override
	public CrudExpressionQueryContext<T, Id> createExpressionQueryContext() {
    	final CrudExpressionQueryContext<T, Id> result = new CrudExpressionQueryContext<T,Id>(getCurrentSession(),getPersistentClass());
    	return result;
    }
    
    
    @SuppressWarnings("unchecked")
    public Class<T> getPersistentClass() {
        if (persistentClass == null) {
            persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        }
        return persistentClass;
    }
    
    public Session getCurrentSession() { return getSessionFactory().getCurrentSession(); }

    public void setSessionFactory(SessionFactory sessionFactory) { this.sessionFactory = sessionFactory; }
    public void setPersistentClass(final Class<T> persistentClass) { this.persistentClass = persistentClass; }

    
    public T find(Id id) {
        return (T) getCurrentSession().get(this.getPersistentClass(), id);
    }

    @SuppressWarnings({ "unchecked", "deprecation" })
    public List<T> findAll() {
        return getCurrentSession().createCriteria(this.getPersistentClass()).list();
    }

    public T save(T entity) {
        getCurrentSession().persist(entity);
        return entity;
    }

    public void delete(T entity) {
        getCurrentSession().delete(entity);
    }

    protected CriteriaBuilder createStandardCriterieBuilder() {
		return getCurrentSession().getCriteriaBuilder();
	}
	
	protected <S> CriteriaQuery<S> createStandardCriteriaQuery(CriteriaBuilder builder, Class<S> clazz) {
		return builder.createQuery(clazz);
	}
	
	protected CriteriaQuery<T> createPersistClassCriteriaQuery(CriteriaBuilder builder) {
		return builder.createQuery(getPersistentClass());
	}
	
	protected <S> TypedQuery<S> toTypedQuery(CriteriaQuery<S> qry) {
		return getCurrentSession().createQuery(qry);
	}
	
	protected TypedQuery<T> createTypedQuery(CriteriaQuery<T> qry) {
		return getCurrentSession().createQuery(qry);
	}
		
	/**
	 * Will create and execute a query that constrains the results by checking that the field identified
	 * by fieldAttribute is equal to the Object provided in the value input to this method.
	 * 
	 * An exception is currently thrown if more than one record exists that fulfills the criteria. 
	 * 
	 * 
	 * @param fieldAttribute the SingularAttribute for the field to be checked for equality
	 * @param value the value that fieldAttribute is check to be equal to in the query criteria.
	 *
	 * @return null if no records match the criteria, the object that fulfills the criteria if it exists.
	 * 
	 */
	protected <X> T findUniqueResultForSingleField(SingularAttribute<T,X> fieldAttribute, X value) {
		final T result;
		
		List<T> results = findAllMatchingEquality(fieldAttribute, value);
		if (results.size() > 1) {
			//@TODO Need to add app specific exception here 
			//so we can trap and log this exception while skipping the user 
			//that is causing it.
			logger.error("Found {} records with {}={}, expected at most 1."
					,results.size()
					,fieldAttribute.getName()					
					,value.toString());
			throw new RuntimeException(String.format("Too many records found for %s=%s"
					,fieldAttribute.getName()
					,value));
		} else if (results.size() == 0) {
			result = null;
		} else {
			result = results.get(0);
		}
		
		return result;
	}
	

	/**
	 * Will create and execute a query that constrains the results by checking that the field identified
	 * by fieldAttribute is equal to the Object provided in the value input to this method.
	 * 
	 * @param fieldAttribute the SingularAttribute for the field to be checked for equality
	 * @param value the value that fieldAttribute is check to be equal to in the query criteria.
	 *
	 * @return List of objects that fulfill the criteria, an empty list is returned if no such records exist.
	 * 
	 */
	protected <X> List<T> findAllMatchingEquality(SingularAttribute<T,X> fieldAttribute, X value) {
		final List<T> results;
		CriteriaBuilder builder  = createStandardCriterieBuilder();
		CriteriaQuery<T> qry = createPersistClassCriteriaQuery(builder);
		Root<T> profile = qry.from(getPersistentClass());
		qry.where( 
				builder.equal(profile.get(fieldAttribute),value)
			);
		
		results = createTypedQuery(qry).getResultList();
		return results;
	}
	
	/**
	 * Will create and execute a query that constrains the results by ANDing equality checks 
	 * for the list of attributes contained in fieldAttributes.  The value for each attribute is
	 * taken from the same position in fieldValues list as that attribute has in the fieldAttributes list.
	 * So, for example, if we have:
	 * 
	 * fieldAttributes = List { Column_C, Column_D, Column_A}
	 * values = { X, A, B }
	 * 
	 * then the query that is generated will have the where clause:
	 * 
	 * where 
	 * 			(Column_C = X) and (Column_D = A) and (Column_A = B)
	 * 
	 * 
	 * Similar to 
	 * 
	 * edu.ucdavis.orvc.integration.impl.hrconnect.HrConnectProfileDao#findUniqueResultForSingleField(javax.persistence.metamodel.SingularAttribute, X)
	 * 
	 * an exception is currently thrown if more than one record exists that fulfills the criteria. 
	 * 
	 * 
	 * @param fieldAttributes A list of SingularAttribute&lt;T,?&gt; which identify the property that will be checked for equality
	 * @param values A list of value objects representing the value each attribute will equal to. 
	 * 
	 * 
	 * @return null if no records match the criteria, the object that fulfills the criteria if it exists.
	 * 
	 */
	protected T findUniqueResultForMultipleFields(final List<SingularAttribute<T,?>> fieldAttributes
												, final List<?> values) {
		
		final T result;
		final List<T> results = findAllMatchingEquality(fieldAttributes,values);

		if (results.size() > 1) {
			//@TODO Need to add app specific exception here 
			//so we can trap and log this expception while skipping the user 
			//that is causing it.
			String constraintString = createConstraintString(fieldAttributes, values);
			logger.error("Found {} records with given constraints {}, expected at most 1."
					,results.size()
					,constraintString);
			throw new RuntimeException(String.format("Too many records found for constraints %s"
														, constraintString));
		} else if (results.size() == 0) {
			result = null;
		} else {
			result = results.get(0);
		}
		
		return result;
	}

	/**
	 * Will create and execute a query that constrains the results by ANDing equality checks 
	 * for the list of attributes contained in fieldAttributes.  The value for each attribute is
	 * taken from the same position in fieldValues list as that attribute has in the fieldAttributes list.
	 * So, for example, if we have:
	 * 
	 * fieldAttributes = List { Column_C, Column_D, Column_A}
	 * values = { X, A, B }
	 * 
	 * then the query that is generated will have the where clause:
	 * 
	 * where 
	 * 			(Column_C = X) and (Column_D = A) and (Column_A = B)
	 * 
	 * @param fieldAttributes A list of SingularAttribute&lt;T,?&gt; which identify the property that will be checked for equality
	 * @param values A list of value objects representing the value each attribute will equal to. 
	 * 
	 * 
	 * @return List&lt;T&gt; containing all records that fulfill the generated criteria.
	 * 
	 */
	protected List<T> findAllMatchingEquality(final List<SingularAttribute<T,?>> fieldAttributes
												, final List<?> values) {
		
		final List<T> results;
		
		final int constraintCount = fieldAttributes.size();
		final int constraintValuesCount = values.size();
		
		if (constraintCount != constraintValuesCount) {
			throw new IllegalArgumentException("Lists passed to createConstraintString do not contain the same number of elements.");
		}
		
		
		CriteriaBuilder builder  = createStandardCriterieBuilder();
		CriteriaQuery<T> qry = createPersistClassCriteriaQuery(builder);
		Root<T> profile = qry.from(getPersistentClass());
		
		if (constraintCount == 1) {
			qry.where( 
					builder.equal(profile.get(fieldAttributes.get(0)),values.get(0))
				);
		} else {
			List<Predicate> predicates = new ArrayList<Predicate>();
			int i = 0;
			for(SingularAttribute<T,?> fieldAttribute : fieldAttributes) {
				predicates.add(builder.equal(profile.get(fieldAttribute), values.get(i++)));
			}
			qry.where(builder.and(predicates.toArray(new Predicate[0])));
		}
		
		results = createTypedQuery(qry).getResultList();
		return results;
	}

	
	
	protected String createConstraintString(final List<SingularAttribute<T,?>> fieldAttributes
			  , final List<?> values) {

		final int constraintCount = fieldAttributes.size();
		final int constraintValuesCount = values.size();

		if (constraintCount != constraintValuesCount) {
				throw new IllegalArgumentException("Lists passed to createConstraintString do not contain the same number of elements.");
		}

		final StringBuilder builder = new StringBuilder();

		builder.append( "{" );

		int i = 0;
		for(SingularAttribute<T,?> fieldAttribute : fieldAttributes) {
			builder.append( 
					String.format(CONSTRAINT_STR_FORMAT, fieldAttribute.getName(), values.get(i++))
					);
			if (i<constraintCount) {
				builder.append(",");
			}
		}

		builder.append("}");
		return builder.toString();
	}


	protected List<T> immutableList(List<T> results) {
		
		if (results == null) {
			return ImmutableList.of();
		} else {
			return ImmutableList.copyOf(results);
		}
	}
	
	protected List<I> immutableDTOList(List<T> results) {
		
		if (results == null) {
			return ImmutableList.of();
		} else {
			return ImmutableList.copyOf(toDTO(results));
		}
	}
	
	protected I toDTO(T in) {
		return in.toDTO();
	}

	protected List<Q> toDTO(List<T> in) {
		final List<Q> result;
		if (in != null) {
			result = new ArrayList<Q>();
			for (T input : in) {
				result.add(input.toDTO());
			}
		} else {
			result = null;
		}
		return result;
	}
	
	
}