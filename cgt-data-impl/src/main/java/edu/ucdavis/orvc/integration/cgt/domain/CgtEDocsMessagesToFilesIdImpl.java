package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Embeddable;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEDocsMessagesToFilesId;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtEDocsMessagesToFilesIdDTO;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Embeddable
public class CgtEDocsMessagesToFilesIdImpl implements CgtEDocsMessagesToFilesId {

	private static final long serialVersionUID = 4422321939458770766L;
	private String edfEDocKey;
	private int edfFileKey;

	public CgtEDocsMessagesToFilesIdImpl() {
	}

	public CgtEDocsMessagesToFilesIdImpl(final String edfEDocKey, final int edfFileKey) {
		this.edfEDocKey = edfEDocKey;
		this.edfFileKey = edfFileKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsMessagesToFilesId#getEdfEDocKey()
	 */
	@Override
	@Column(name = "edf_eDocKey", nullable = false, length = 35, columnDefinition="char")
	public String getEdfEDocKey() {
		return this.edfEDocKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsMessagesToFilesId#setEdfEDocKey(java.lang.String)
	 */
	@Override
	public void setEdfEDocKey(final String edfEDocKey) {
		this.edfEDocKey = edfEDocKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsMessagesToFilesId#getEdfFileKey()
	 */
	@Override
	@Column(name = "edf_fileKey", nullable = false)
	public int getEdfFileKey() {
		return this.edfFileKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsMessagesToFilesId#setEdfFileKey(int)
	 */
	@Override
	public void setEdfFileKey(final int edfFileKey) {
		this.edfFileKey = edfFileKey;
	}

	@Override
	public boolean equals(final Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof CgtEDocsMessagesToFilesIdImpl))
			return false;
		final CgtEDocsMessagesToFilesId castOther = (CgtEDocsMessagesToFilesId) other;

		return ((this.getEdfEDocKey() == castOther.getEdfEDocKey()) || (this.getEdfEDocKey() != null
				&& castOther.getEdfEDocKey() != null && this.getEdfEDocKey().equals(castOther.getEdfEDocKey())))
				&& (this.getEdfFileKey() == castOther.getEdfFileKey());
	}

	@Override
	public int hashCode() {
		int result = 17;

		result = 37 * result + (getEdfEDocKey() == null ? 0 : this.getEdfEDocKey().hashCode());
		result = 37 * result + this.getEdfFileKey();
		return result;
	}

	public CgtEDocsMessagesToFilesIdDTO toDTO() {
		return new CgtEDocsMessagesToFilesIdDTO(this);
	}
}
