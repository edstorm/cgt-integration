package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtStopWords;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtStopWordsDTO;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_StopWords")
public class CgtStopWordsImpl implements CgtStopWords {

	private static final long serialVersionUID = 4959276627523872881L;
	private String word;

	public CgtStopWordsImpl() {
	}

	public CgtStopWordsImpl(final String word) {
		this.word = word;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtStopWords#getWord()
	 */
	@Override
	@Id
	@Column(name = "word", unique = true, nullable = false, length = 200)
	public String getWord() {
		return this.word;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtStopWords#setWord(java.lang.String)
	 */
	@Override
	public void setWord(final String word) {
		this.word = word;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((word == null) ? 0 : word.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtStopWordsImpl)) {
			return false;
		}
		CgtStopWordsImpl other = (CgtStopWordsImpl) obj;
		if (word == null) {
			if (other.word != null) {
				return false;
			}
		} else if (!word.equals(other.word)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("CgtStopWordsImpl [word=%s]", word);
	}



	public CgtStopWordsDTO toDTO() {
		return new CgtStopWordsDTO(this);
	}

}
