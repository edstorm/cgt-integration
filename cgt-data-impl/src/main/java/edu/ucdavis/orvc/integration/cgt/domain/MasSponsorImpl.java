package edu.ucdavis.orvc.integration.cgt.domain;


import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtSponsor;
import edu.ucdavis.orvc.integration.cgt.api.domain.MasSponsor;
import edu.ucdavis.orvc.integration.cgt.api.domain.MasSponsorAgency;
import edu.ucdavis.orvc.integration.cgt.api.domain.MasSponsorCategory;
import edu.ucdavis.orvc.integration.cgt.api.domain.MasSponsorSubAgency;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.MasSponsorDTO;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "MAS_Sponsors")
public class MasSponsorImpl implements MasSponsor {

	private static final long serialVersionUID = 3399567174983471446L;
	private String spoSponsorKey;
	private String spoSponsorCategoryKey;
	private String spoAgencyKey;
	private String spoSubagencyKey;
	private String spoName;
	private char spoIsForeign;
	private String spoUcopDated;
	private String spoDataSourceKey;
	private Boolean spoIspermanent;
	private String spoSubmittedby;
	private String spoApprovedby;
	private LocalDateTime spoDateapproved;
	private String spoDeniedby;
	private LocalDateTime spoDatedenied;
	private String spoNotes;
	private Boolean spoIsActive;
	private LocalDateTime spoDateCreated;
	private LocalDateTime spoDateUpdated;
	private String spoUpdatedBy;
	
	private MasSponsorAgencyImpl sponsorAgency;
	private MasSponsorSubAgencyImpl sponsorSubAgency;
	private MasSponsorCategoryImpl sponsorCategory;
	private List<CgtSponsorImpl> sponsors;
	

	public MasSponsorImpl() {
	}

	public MasSponsorImpl(final String spoSponsorKey, final String spoName, final char spoIsForeign, final LocalDateTime spoDateCreated,
			final LocalDateTime spoDateUpdated, final String spoUpdatedBy) {
		this.spoSponsorKey = spoSponsorKey;
		this.spoName = spoName;
		this.spoIsForeign = spoIsForeign;
		this.spoDateCreated = spoDateCreated;
		this.spoDateUpdated = spoDateUpdated;
		this.spoUpdatedBy = spoUpdatedBy;
	}

	public MasSponsorImpl(final String spoSponsorKey, final String spoSponsorCategoryKey, final String spoAgencyKey, final String spoSubagencyKey,
			final String spoName, final char spoIsForeign, final String spoUcopDated, final String spoDataSourceKey, final Boolean spoIspermanent,
			final String spoSubmittedby, final String spoApprovedby, final LocalDateTime spoDateapproved, final String spoDeniedby, final LocalDateTime spoDatedenied,
			final String spoNotes, final Boolean spoIsActive, final LocalDateTime spoDateCreated, final LocalDateTime spoDateUpdated, final String spoUpdatedBy) {
		this.spoSponsorKey = spoSponsorKey;
		this.spoSponsorCategoryKey = spoSponsorCategoryKey;
		this.spoAgencyKey = spoAgencyKey;
		this.spoSubagencyKey = spoSubagencyKey;
		this.spoName = spoName;
		this.spoIsForeign = spoIsForeign;
		this.spoUcopDated = spoUcopDated;
		this.spoDataSourceKey = spoDataSourceKey;
		this.spoIspermanent = spoIspermanent;
		this.spoSubmittedby = spoSubmittedby;
		this.spoApprovedby = spoApprovedby;
		this.spoDateapproved = spoDateapproved;
		this.spoDeniedby = spoDeniedby;
		this.spoDatedenied = spoDatedenied;
		this.spoNotes = spoNotes;
		this.spoIsActive = spoIsActive;
		this.spoDateCreated = spoDateCreated;
		this.spoDateUpdated = spoDateUpdated;
		this.spoUpdatedBy = spoUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#getSpoSponsorKey()
	 */
	@Override
	@Id
	@Column(name = "spo_sponsorKey", unique = true, nullable = false, length = 8, columnDefinition="char")
	public String getSpoSponsorKey() {
		return this.spoSponsorKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#setSpoSponsorKey(java.lang.String)
	 */
	@Override
	public void setSpoSponsorKey(final String spoSponsorKey) {
		this.spoSponsorKey = spoSponsorKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#getSpoSponsorCategoryKey()
	 */
	@Override
	@Column(name = "spo_sponsorCategoryKey", length = 2, columnDefinition="char")
	public String getSpoSponsorCategoryKey() {
		return this.spoSponsorCategoryKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#setSpoSponsorCategoryKey(java.lang.String)
	 */
	@Override
	public void setSpoSponsorCategoryKey(final String spoSponsorCategoryKey) {
		this.spoSponsorCategoryKey = spoSponsorCategoryKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#getSpoAgencyKey()
	 */
	@Override
	@Column(name = "spo_agencyKey", length = 2,columnDefinition="char")
	public String getSpoAgencyKey() {
		return this.spoAgencyKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#setSpoAgencyKey(java.lang.String)
	 */
	@Override
	public void setSpoAgencyKey(final String spoAgencyKey) {
		this.spoAgencyKey = spoAgencyKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#getSpoSubagencyKey()
	 */
	@Override
	@Column(name = "spo_subagencyKey", length = 2, columnDefinition="char")
	public String getSpoSubagencyKey() {
		return this.spoSubagencyKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#setSpoSubagencyKey(java.lang.String)
	 */
	@Override
	public void setSpoSubagencyKey(final String spoSubagencyKey) {
		this.spoSubagencyKey = spoSubagencyKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#getSpoName()
	 */
	@Override
	@Column(name = "spo_name", nullable = false, length = 70)
	public String getSpoName() {
		return this.spoName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#setSpoName(java.lang.String)
	 */
	@Override
	public void setSpoName(final String spoName) {
		this.spoName = spoName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#getSpoIsForeign()
	 */
	@Override
	@Column(name = "spo_isForeign", nullable = false, length = 1)
	public char getSpoIsForeign() {
		return this.spoIsForeign;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#setSpoIsForeign(char)
	 */
	@Override
	public void setSpoIsForeign(final char spoIsForeign) {
		this.spoIsForeign = spoIsForeign;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#getSpoUcopDated()
	 */
	@Override
	@Column(name = "spo_ucop_dated", length = 50)
	public String getSpoUcopDated() {
		return this.spoUcopDated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#setSpoUcopDated(java.lang.String)
	 */
	@Override
	public void setSpoUcopDated(final String spoUcopDated) {
		this.spoUcopDated = spoUcopDated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#getSpoDataSourceKey()
	 */
	@Override
	@Column(name = "spo_dataSourceKey", length = 3, columnDefinition="char")
	public String getSpoDataSourceKey() {
		return this.spoDataSourceKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#setSpoDataSourceKey(java.lang.String)
	 */
	@Override
	public void setSpoDataSourceKey(final String spoDataSourceKey) {
		this.spoDataSourceKey = spoDataSourceKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#getSpoIspermanent()
	 */
	@Override
	@Column(name = "spo_ispermanent")
	public Boolean getSpoIspermanent() {
		return this.spoIspermanent;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#setSpoIspermanent(java.lang.Boolean)
	 */
	@Override
	public void setSpoIspermanent(final Boolean spoIspermanent) {
		this.spoIspermanent = spoIspermanent;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#getSpoSubmittedby()
	 */
	@Override
	@Column(name = "spo_submittedby", length = 35, columnDefinition="char")
	public String getSpoSubmittedby() {
		return this.spoSubmittedby;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#setSpoSubmittedby(java.lang.String)
	 */
	@Override
	public void setSpoSubmittedby(final String spoSubmittedby) {
		this.spoSubmittedby = spoSubmittedby;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#getSpoApprovedby()
	 */
	@Override
	@Column(name = "spo_approvedby", length = 35, columnDefinition="char")
	public String getSpoApprovedby() {
		return this.spoApprovedby;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#setSpoApprovedby(java.lang.String)
	 */
	@Override
	public void setSpoApprovedby(final String spoApprovedby) {
		this.spoApprovedby = spoApprovedby;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#getSpoDateapproved()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "spo_dateapproved", length = 23)
	public LocalDateTime getSpoDateapproved() {
		return this.spoDateapproved;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#setSpoDateapproved(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setSpoDateapproved(final LocalDateTime spoDateapproved) {
		this.spoDateapproved = spoDateapproved;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#getSpoDeniedby()
	 */
	@Override
	@Column(name = "spo_deniedby", length = 35, columnDefinition="char")
	public String getSpoDeniedby() {
		return this.spoDeniedby;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#setSpoDeniedby(java.lang.String)
	 */
	@Override
	public void setSpoDeniedby(final String spoDeniedby) {
		this.spoDeniedby = spoDeniedby;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#getSpoDatedenied()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "spo_datedenied", length = 23)
	public LocalDateTime getSpoDatedenied() {
		return this.spoDatedenied;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#setSpoDatedenied(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setSpoDatedenied(final LocalDateTime spoDatedenied) {
		this.spoDatedenied = spoDatedenied;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#getSpoNotes()
	 */
	@Override
	@Column(name = "spo_notes", length = 8000)
	public String getSpoNotes() {
		return this.spoNotes;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#setSpoNotes(java.lang.String)
	 */
	@Override
	public void setSpoNotes(final String spoNotes) {
		this.spoNotes = spoNotes;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#getSpoIsActive()
	 */
	@Override
	@Column(name = "spo_isActive")
	public Boolean getSpoIsActive() {
		return this.spoIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#setSpoIsActive(java.lang.Boolean)
	 */
	@Override
	public void setSpoIsActive(final Boolean spoIsActive) {
		this.spoIsActive = spoIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#getSpoDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "spo_dateCreated", nullable = false, length = 23)
	public LocalDateTime getSpoDateCreated() {
		return this.spoDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#setSpoDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setSpoDateCreated(final LocalDateTime spoDateCreated) {
		this.spoDateCreated = spoDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#getSpoDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "spo_dateUpdated", nullable = false, length = 23)
	public LocalDateTime getSpoDateUpdated() {
		return this.spoDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#setSpoDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setSpoDateUpdated(final LocalDateTime spoDateUpdated) {
		this.spoDateUpdated = spoDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#getSpoUpdatedBy()
	 */
	@Override
	@Column(name = "spo_updatedBy", nullable = false, length = 35, columnDefinition="char")
	public String getSpoUpdatedBy() {
		return this.spoUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorInterface#setSpoUpdatedBy(java.lang.String)
	 */
	@Override
	public void setSpoUpdatedBy(final String spoUpdatedBy) {
		this.spoUpdatedBy = spoUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((spoAgencyKey == null) ? 0 : spoAgencyKey.hashCode());
		result = prime * result + ((spoApprovedby == null) ? 0 : spoApprovedby.hashCode());
		result = prime * result + ((spoDataSourceKey == null) ? 0 : spoDataSourceKey.hashCode());
		result = prime * result + ((spoDateCreated == null) ? 0 : spoDateCreated.hashCode());
		result = prime * result + ((spoDateUpdated == null) ? 0 : spoDateUpdated.hashCode());
		result = prime * result + ((spoDateapproved == null) ? 0 : spoDateapproved.hashCode());
		result = prime * result + ((spoDatedenied == null) ? 0 : spoDatedenied.hashCode());
		result = prime * result + ((spoDeniedby == null) ? 0 : spoDeniedby.hashCode());
		result = prime * result + ((spoIsActive == null) ? 0 : spoIsActive.hashCode());
		result = prime * result + spoIsForeign;
		result = prime * result + ((spoIspermanent == null) ? 0 : spoIspermanent.hashCode());
		result = prime * result + ((spoName == null) ? 0 : spoName.hashCode());
		result = prime * result + ((spoNotes == null) ? 0 : spoNotes.hashCode());
		result = prime * result + ((spoSponsorCategoryKey == null) ? 0 : spoSponsorCategoryKey.hashCode());
		result = prime * result + ((spoSponsorKey == null) ? 0 : spoSponsorKey.hashCode());
		result = prime * result + ((spoSubagencyKey == null) ? 0 : spoSubagencyKey.hashCode());
		result = prime * result + ((spoSubmittedby == null) ? 0 : spoSubmittedby.hashCode());
		result = prime * result + ((spoUcopDated == null) ? 0 : spoUcopDated.hashCode());
		result = prime * result + ((spoUpdatedBy == null) ? 0 : spoUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof MasSponsorImpl))
			return false;
		final MasSponsorImpl other = (MasSponsorImpl) obj;
		if (spoAgencyKey == null) {
			if (other.spoAgencyKey != null)
				return false;
		} else if (!spoAgencyKey.equals(other.spoAgencyKey))
			return false;
		if (spoApprovedby == null) {
			if (other.spoApprovedby != null)
				return false;
		} else if (!spoApprovedby.equals(other.spoApprovedby))
			return false;
		if (spoDataSourceKey == null) {
			if (other.spoDataSourceKey != null)
				return false;
		} else if (!spoDataSourceKey.equals(other.spoDataSourceKey))
			return false;
		if (spoDateCreated == null) {
			if (other.spoDateCreated != null)
				return false;
		} else if (!spoDateCreated.equals(other.spoDateCreated))
			return false;
		if (spoDateUpdated == null) {
			if (other.spoDateUpdated != null)
				return false;
		} else if (!spoDateUpdated.equals(other.spoDateUpdated))
			return false;
		if (spoDateapproved == null) {
			if (other.spoDateapproved != null)
				return false;
		} else if (!spoDateapproved.equals(other.spoDateapproved))
			return false;
		if (spoDatedenied == null) {
			if (other.spoDatedenied != null)
				return false;
		} else if (!spoDatedenied.equals(other.spoDatedenied))
			return false;
		if (spoDeniedby == null) {
			if (other.spoDeniedby != null)
				return false;
		} else if (!spoDeniedby.equals(other.spoDeniedby))
			return false;
		if (spoIsActive == null) {
			if (other.spoIsActive != null)
				return false;
		} else if (!spoIsActive.equals(other.spoIsActive))
			return false;
		if (spoIsForeign != other.spoIsForeign)
			return false;
		if (spoIspermanent == null) {
			if (other.spoIspermanent != null)
				return false;
		} else if (!spoIspermanent.equals(other.spoIspermanent))
			return false;
		if (spoName == null) {
			if (other.spoName != null)
				return false;
		} else if (!spoName.equals(other.spoName))
			return false;
		if (spoNotes == null) {
			if (other.spoNotes != null)
				return false;
		} else if (!spoNotes.equals(other.spoNotes))
			return false;
		if (spoSponsorCategoryKey == null) {
			if (other.spoSponsorCategoryKey != null)
				return false;
		} else if (!spoSponsorCategoryKey.equals(other.spoSponsorCategoryKey))
			return false;
		if (spoSponsorKey == null) {
			if (other.spoSponsorKey != null)
				return false;
		} else if (!spoSponsorKey.equals(other.spoSponsorKey))
			return false;
		if (spoSubagencyKey == null) {
			if (other.spoSubagencyKey != null)
				return false;
		} else if (!spoSubagencyKey.equals(other.spoSubagencyKey))
			return false;
		if (spoSubmittedby == null) {
			if (other.spoSubmittedby != null)
				return false;
		} else if (!spoSubmittedby.equals(other.spoSubmittedby))
			return false;
		if (spoUcopDated == null) {
			if (other.spoUcopDated != null)
				return false;
		} else if (!spoUcopDated.equals(other.spoUcopDated))
			return false;
		if (spoUpdatedBy == null) {
			if (other.spoUpdatedBy != null)
				return false;
		} else if (!spoUpdatedBy.equals(other.spoUpdatedBy))
			return false;
		return true;
	}

	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"MasSponsor [spoSponsorKey=%s, spoSponsorCategoryKey=%s, spoAgencyKey=%s, spoSubagencyKey=%s, spoName=%s, spoIsForeign=%s, spoUcopDated=%s, spoDataSourceKey=%s, spoIspermanent=%s, spoSubmittedby=%s, spoApprovedby=%s, spoDateapproved=%s, spoDeniedby=%s, spoDatedenied=%s, spoNotes=%s, spoIsActive=%s, spoDateCreated=%s, spoDateUpdated=%s, spoUpdatedBy=%s, sponsorAgency=%s, sponsorSubAgency=%s, sponsorCategory=%s]",
				spoSponsorKey, spoSponsorCategoryKey, spoAgencyKey, spoSubagencyKey, spoName, spoIsForeign,
				spoUcopDated, spoDataSourceKey, spoIspermanent, spoSubmittedby, spoApprovedby, spoDateapproved,
				spoDeniedby, spoDatedenied, spoNotes, spoIsActive, spoDateCreated, spoDateUpdated, spoUpdatedBy,
				sponsorAgency, sponsorSubAgency, sponsorCategory);
	}


	@ManyToOne
	@JoinColumn(name="spo_agencykey",insertable=false,updatable=false)
	public MasSponsorAgencyImpl getSponsorAgencyImpl() {
		return sponsorAgency;
	}

	public void setSponsorAgencyImpl(MasSponsorAgencyImpl sponsorAgency) {
		this.sponsorAgency = sponsorAgency;
	}

	
	@ManyToOne
	@JoinColumn(name="spo_subagencykey",insertable=false,updatable=false)
	public MasSponsorSubAgencyImpl getSponsorSubAgencyImpl() {
		return sponsorSubAgency;
	}

	public void setSponsorSubAgencyImpl(MasSponsorSubAgencyImpl sponsorSubAgency) {
		this.sponsorSubAgency = sponsorSubAgency;
	}

	@ManyToOne
	@JoinColumn(name="spo_sponsorcategorykey",insertable=false,updatable=false)
	public MasSponsorCategoryImpl getSponsorCategoryImpl() {
		return sponsorCategory;
	}

	public void setSponsorCategoryImpl(MasSponsorCategoryImpl sponsorCategory) {
		this.sponsorCategory = sponsorCategory;
	}

	@OneToMany(fetch=FetchType.LAZY,mappedBy="masSponsorImpl")
	public List<CgtSponsorImpl> getSponsorImpls() {
		return sponsors;
	}

	public void setSponsorImpls(List<CgtSponsorImpl> sponsor) {
		this.sponsors = sponsor;
	}

	
	//methods to fullfill the interface. They get data from the impl versions of the methods.
	
	
	@Override
	@Transient
	public MasSponsorAgency getSponsorAgency() {
		return this.sponsorAgency;
	}

	@Override
	public void setSponsorAgency(MasSponsorAgency sponsorAgency) {
		this.sponsorAgency = (MasSponsorAgencyImpl) sponsorAgency;
	}

	@Override
	@Transient
	public MasSponsorSubAgency getSponsorSubAgency() {
		return this.sponsorSubAgency;
	}

	@Override
	public void setSponsorSubAgency(MasSponsorSubAgency sponsorSubAgency) {
		this.sponsorSubAgency = (MasSponsorSubAgencyImpl) sponsorSubAgency;
	}

	@Override
	@Transient
	public MasSponsorCategory getSponsorCategory() {
		return this.sponsorCategory;
	}

	@Override
	public void setSponsorCategory(MasSponsorCategory sponsorCategory) {
		this.sponsorCategory = (MasSponsorCategoryImpl) sponsorCategory;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	@Transient
	public List<CgtSponsor> getSponsors() {
		return (List)this.sponsors;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void setSponsors(List<CgtSponsor> sponsor) {
		this.sponsors = (List)sponsor;
	}



	public MasSponsorDTO toDTO() {
		return new MasSponsorDTO(this);
	}

}
