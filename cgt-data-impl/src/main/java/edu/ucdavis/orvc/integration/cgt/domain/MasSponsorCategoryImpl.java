package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.MasSponsorCategory;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.MasSponsorCategoryDTO;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "MAS_SponsorCategories")
public class MasSponsorCategoryImpl implements MasSponsorCategory {

	private static final long serialVersionUID = 5870688968121580386L;
	private String spcSponsorCategoryKey;
	private String spcName;
	private boolean spcIsActive;
	private LocalDateTime spcDateCreated;
	private LocalDateTime spcDateUpdated;
	private String spcUpdatedBy;

	public MasSponsorCategoryImpl() {
	}

	public MasSponsorCategoryImpl(final String spcSponsorCategoryKey, final String spcName, final boolean spcIsActive, final LocalDateTime spcDateCreated,
			final LocalDateTime spcDateUpdated, final String spcUpdatedBy) {
		this.spcSponsorCategoryKey = spcSponsorCategoryKey;
		this.spcName = spcName;
		this.spcIsActive = spcIsActive;
		this.spcDateCreated = spcDateCreated;
		this.spcDateUpdated = spcDateUpdated;
		this.spcUpdatedBy = spcUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorCategory#getSpcSponsorCategoryKey()
	 */
	@Override
	@Id
	@Column(name = "spc_sponsorCategoryKey", unique = true, nullable = false, length = 2,columnDefinition="char")
	public String getSpcSponsorCategoryKey() {
		return this.spcSponsorCategoryKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorCategory#setSpcSponsorCategoryKey(java.lang.String)
	 */
	@Override
	public void setSpcSponsorCategoryKey(final String spcSponsorCategoryKey) {
		this.spcSponsorCategoryKey = spcSponsorCategoryKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorCategory#getSpcName()
	 */
	@Override
	@Column(name = "spc_name", nullable = false, length = 50)
	public String getSpcName() {
		return this.spcName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorCategory#setSpcName(java.lang.String)
	 */
	@Override
	public void setSpcName(final String spcName) {
		this.spcName = spcName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorCategory#isSpcIsActive()
	 */
	@Override
	@Column(name = "spc_isActive", nullable = false)
	public boolean isSpcIsActive() {
		return this.spcIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorCategory#setSpcIsActive(boolean)
	 */
	@Override
	public void setSpcIsActive(final boolean spcIsActive) {
		this.spcIsActive = spcIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorCategory#getSpcDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "spc_dateCreated", nullable = false, length = 23)
	public LocalDateTime getSpcDateCreated() {
		return this.spcDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorCategory#setSpcDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setSpcDateCreated(final LocalDateTime spcDateCreated) {
		this.spcDateCreated = spcDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorCategory#getSpcDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "spc_dateUpdated", nullable = false, length = 23)
	public LocalDateTime getSpcDateUpdated() {
		return this.spcDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorCategory#setSpcDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setSpcDateUpdated(final LocalDateTime spcDateUpdated) {
		this.spcDateUpdated = spcDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorCategory#getSpcUpdatedBy()
	 */
	@Override
	@Column(name = "spc_updatedBy", nullable = false, length = 35, columnDefinition="char")
	public String getSpcUpdatedBy() {
		return this.spcUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasSponsorCategory#setSpcUpdatedBy(java.lang.String)
	 */
	@Override
	public void setSpcUpdatedBy(final String spcUpdatedBy) {
		this.spcUpdatedBy = spcUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((spcDateCreated == null) ? 0 : spcDateCreated.hashCode());
		result = prime * result + ((spcDateUpdated == null) ? 0 : spcDateUpdated.hashCode());
		result = prime * result + (spcIsActive ? 1231 : 1237);
		result = prime * result + ((spcName == null) ? 0 : spcName.hashCode());
		result = prime * result + ((spcSponsorCategoryKey == null) ? 0 : spcSponsorCategoryKey.hashCode());
		result = prime * result + ((spcUpdatedBy == null) ? 0 : spcUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof MasSponsorCategoryImpl))
			return false;
		final MasSponsorCategoryImpl other = (MasSponsorCategoryImpl) obj;
		if (spcDateCreated == null) {
			if (other.spcDateCreated != null)
				return false;
		} else if (!spcDateCreated.equals(other.spcDateCreated))
			return false;
		if (spcDateUpdated == null) {
			if (other.spcDateUpdated != null)
				return false;
		} else if (!spcDateUpdated.equals(other.spcDateUpdated))
			return false;
		if (spcIsActive != other.spcIsActive)
			return false;
		if (spcName == null) {
			if (other.spcName != null)
				return false;
		} else if (!spcName.equals(other.spcName))
			return false;
		if (spcSponsorCategoryKey == null) {
			if (other.spcSponsorCategoryKey != null)
				return false;
		} else if (!spcSponsorCategoryKey.equals(other.spcSponsorCategoryKey))
			return false;
		if (spcUpdatedBy == null) {
			if (other.spcUpdatedBy != null)
				return false;
		} else if (!spcUpdatedBy.equals(other.spcUpdatedBy))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"MasSponsorCategories [spcSponsorCategoryKey=%s, spcName=%s, spcIsActive=%s, spcDateCreated=%s, spcDateUpdated=%s, spcUpdatedBy=%s]",
				spcSponsorCategoryKey, spcName, spcIsActive, spcDateCreated, spcDateUpdated, spcUpdatedBy);
	}



	public MasSponsorCategoryDTO toDTO() {
		return new MasSponsorCategoryDTO(this);
	}

}
