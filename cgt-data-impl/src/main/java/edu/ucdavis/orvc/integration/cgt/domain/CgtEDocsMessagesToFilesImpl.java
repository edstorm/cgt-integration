package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEDocsMessagesToFiles;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEDocsMessagesToFilesId;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtEDocsMessagesToFilesDTO;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_eDocsMessagesToFiles")
public class CgtEDocsMessagesToFilesImpl implements CgtEDocsMessagesToFiles {

	private static final long serialVersionUID = 2664705161278878749L;
	private CgtEDocsMessagesToFilesIdImpl id;

	public CgtEDocsMessagesToFilesImpl() {
	}

	public CgtEDocsMessagesToFilesImpl(final CgtEDocsMessagesToFilesIdImpl id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsMessagesToFiles#getId()
	 */

	@EmbeddedId
	@AttributeOverrides({
		@AttributeOverride(name = "edfEDocKey", column = @Column(name = "edf_eDocKey", nullable = false, length = 35, columnDefinition="char" )),
		@AttributeOverride(name = "edfFileKey", column = @Column(name = "edf_fileKey", nullable = false) ) })
	public CgtEDocsMessagesToFilesIdImpl getIdImpl() {
		return this.id;
	}

	public void setIdImpl(final CgtEDocsMessagesToFilesIdImpl id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtEDocsMessagesToFilesImpl)) {
			return false;
		}
		CgtEDocsMessagesToFilesImpl other = (CgtEDocsMessagesToFilesImpl) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("CgtEDocsMessagesToFilesImpl [id=%s]", id);
	}

	@Override
	@Transient
	public CgtEDocsMessagesToFilesId getId() {
		return id;
	}

	@Override
	public void setId(CgtEDocsMessagesToFilesId id) {
		this.id = (CgtEDocsMessagesToFilesIdImpl) id;
	}

	public CgtEDocsMessagesToFilesDTO toDTO() {
		return new CgtEDocsMessagesToFilesDTO(this);
	}
}
