package edu.ucdavis.orvc.integration.cgt.data.impl;

import static edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFilesImpl_.filDateCreated;
import static edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFilesImpl_.filExtension;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEDocsFiles;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtEDocsFilesDTO;
import edu.ucdavis.orvc.integration.cgt.api.domain.service.CgtFilesDao;
import edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsFilesImpl;




@Transactional
@Repository("cgtFilesDao")
public class CgtFilesDaoImpl extends CgtCrudDaoImpl<CgtEDocsFilesImpl, Integer, CgtEDocsFilesDTO, CgtEDocsFiles> implements CgtFilesDao {
	
	
	protected Logger log = LoggerFactory.getLogger(CgtFilesDaoImpl.class);
	
	protected int _DEFAULT_FILE_AGE_IN_DAYS = 365 + 180;
	
	
	public void init() {
		log.info("STD:SERVICE STARTED");
	}
	
	
	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.data.impl.CgtFilesDao#getByCriteria(java.lang.String, org.joda.time.LocalDateTime, org.joda.time.LocalDateTime)
	 */
	
	protected List<CgtEDocsFilesImpl> getByCriteria(final String fileExtension
												,final LocalDateTime startCreateDate
												,final LocalDateTime endCreateDate ) 
	{
	
		log.debug("Looking for edodfiles with extension={}, and {} <= created date <={}"
				, fileExtension, startCreateDate, endCreateDate);
		
		Assert.hasLength(fileExtension,"fileExtenion must not be null or empty.");
		if (startCreateDate != null && endCreateDate != null) {
			Assert.state(endCreateDate.isAfter(startCreateDate));
		}
		
		final List<CgtEDocsFilesImpl> results;
		final CriteriaBuilder builder  = createStandardCriterieBuilder();
		final CriteriaQuery<CgtEDocsFilesImpl> qry = createPersistClassCriteriaQuery(builder);
		final Root<CgtEDocsFilesImpl> profile = qry.from(CgtEDocsFilesImpl.class);
		final LocalDateTime pStartCreatedDate = 
				startCreateDate == null ? new LocalDateTime().minusDays(_DEFAULT_FILE_AGE_IN_DAYS) : startCreateDate;
		
		log.info("start={}", pStartCreatedDate);
		
		Predicate[] crit = new Predicate[1+(pStartCreatedDate==null?0:1)+(endCreateDate==null?0:1)];
		crit[0] = builder.equal(profile.get(filExtension), fileExtension);
		int i = 0;
		if (pStartCreatedDate != null) {
			crit[1] = builder.greaterThanOrEqualTo(profile.get(filDateCreated), pStartCreatedDate);
			i = 1;
		}
		
		if (endCreateDate != null) {
			crit[i+1] = builder.lessThanOrEqualTo(profile.get(filDateCreated), endCreateDate);
		}
		
		Predicate p = builder.and(crit);
		
		
		qry.where(p);
		
		results = createTypedQuery(qry).getResultList();
		return results;
	}
	
	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.data.impl.CgtFilesDao#getDTOByCriteria(java.lang.String, org.joda.time.LocalDateTime, org.joda.time.LocalDateTime)
	 */
	@Override
	public List<CgtEDocsFiles> getDTOByCriteria(final String fileExtension
												,final LocalDateTime startCreateDate
												,final LocalDateTime endCreateDate) {
		return immutableDTOList(getByCriteria(fileExtension, startCreateDate, endCreateDate));
	}
	
	@Override
	public CgtEDocsFiles findByKey(Integer key) {
		final CgtEDocsFilesImpl resultA = find(key);
		final CgtEDocsFiles resultDTO = resultA==null?null:toDTO(resultA);
		return resultDTO;
	}


	@Override
	public CgtEDocsFilesImpl saveOrUpdate(CgtEDocsFilesImpl entity) {
		throw new UnsupportedOperationException("Method is not currently implemented");
	}
	
}
