package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtWrapUpCloseRulesToSponsorCategories;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtWrapUpCloseRulesToSponsorCategoriesId;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtWrapUpCloseRulesToSponsorCategoriesDTO;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_WrapUpCloseRulesToSponsorCategories")
public class CgtWrapUpCloseRulesToSponsorCategoriesImpl implements CgtWrapUpCloseRulesToSponsorCategories {

	private static final long serialVersionUID = -3898134266117809625L;
	private CgtWrapUpCloseRulesToSponsorCategoriesIdImpl id;
	private int w2cResultSubmissionTypeKey;

	public CgtWrapUpCloseRulesToSponsorCategoriesImpl() {
	}

	public CgtWrapUpCloseRulesToSponsorCategoriesImpl(final CgtWrapUpCloseRulesToSponsorCategoriesIdImpl id,
			final int w2cResultSubmissionTypeKey) {
		this.id = id;
		this.w2cResultSubmissionTypeKey = w2cResultSubmissionTypeKey;
	}

	@EmbeddedId
	@AttributeOverrides({
		@AttributeOverride(name = "w2cCloseRuleKey", column = @Column(name = "w2c_closeRuleKey", nullable = false) ),
		@AttributeOverride(name = "w2cSponsorCategoryKey", column = @Column(name = "w2c_sponsorCategoryKey", nullable = false, length = 2,columnDefinition="char") ) })
	public CgtWrapUpCloseRulesToSponsorCategoriesIdImpl getIdImpl() {
		return this.id;
	}

	public void setIdImpl(final CgtWrapUpCloseRulesToSponsorCategoriesIdImpl id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToSponsorCategories#getW2cResultSubmissionTypeKey()
	 */
	@Override
	@Column(name = "w2c_resultSubmissionTypeKey", nullable = false)
	public int getW2cResultSubmissionTypeKey() {
		return this.w2cResultSubmissionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToSponsorCategories#setW2cResultSubmissionTypeKey(int)
	 */
	@Override
	public void setW2cResultSubmissionTypeKey(final int w2cResultSubmissionTypeKey) {
		this.w2cResultSubmissionTypeKey = w2cResultSubmissionTypeKey;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + w2cResultSubmissionTypeKey;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CgtWrapUpCloseRulesToSponsorCategoriesImpl))
			return false;
		final CgtWrapUpCloseRulesToSponsorCategoriesImpl other = (CgtWrapUpCloseRulesToSponsorCategoriesImpl) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (w2cResultSubmissionTypeKey != other.w2cResultSubmissionTypeKey)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("CgtWrapUpCloseRulesToSponsorCategories [id=%s, w2cResultSubmissionTypeKey=%s]", id,
				w2cResultSubmissionTypeKey);
	}

	@Override
	@Transient
	public CgtWrapUpCloseRulesToSponsorCategoriesId getId() {
		return id;
	}

	@Override
	public void setId(CgtWrapUpCloseRulesToSponsorCategoriesId id) {
		this.id = (CgtWrapUpCloseRulesToSponsorCategoriesIdImpl) id;
	}



	public CgtWrapUpCloseRulesToSponsorCategoriesDTO toDTO() {
		return new CgtWrapUpCloseRulesToSponsorCategoriesDTO(this);
	}

}
