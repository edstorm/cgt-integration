package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.MasUser;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.MasUserDTO;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "MAS_Users", uniqueConstraints = {
		@UniqueConstraint(columnNames = "use_email"), @UniqueConstraint(columnNames = "use_employeeID"),
		@UniqueConstraint(columnNames = "use_mothraID"), @UniqueConstraint(columnNames = "use_UCDLogin") })
public class MasUserImpl implements MasUser {

	private static final long serialVersionUID = 7917350616809220383L;
	
	private String useUserKey;
	private String useSsn;
	private String useUcdlogin;
	private String useUserName;
	private String usePassword;
	private String useEmployeeId;
	private String useFirstName;
	private String useMiddleName;
	private String useLastName;
	private String useMothraFname;
	private String useMothraMname;
	private String useMothraLname;
	private String useMothraId;
	private String useDisplayTitle;
	private String useUserTitleKey;
	private Integer useUserPrefixKey;
	private Integer useUserSuffixKey;
	private String useDepartmentKey;
	private String useInstitutionKey;
	private String usePhone;
	private String useFax;
	private String useEmail;
	private LocalDateTime useMothraVerifyDate;
	private boolean useIsActive;
	private String useNotes;
	private Boolean useIsPasswordReset;
	private LocalDateTime useDateCreated;
	private LocalDateTime useDateUpdated;
	private String useDataSourceKey;
	private String useUpdatedBy;
	private boolean useExistsInLdap;

	public MasUserImpl() {
	}

	public MasUserImpl(final String useUserKey, final String useFirstName, final String useLastName, final boolean useIsActive,
			final LocalDateTime useDateCreated, final LocalDateTime useDateUpdated, final String useDataSourceKey, final String useUpdatedBy,
			final boolean useExistsInLdap) {
		this.useUserKey = useUserKey;
		this.useFirstName = useFirstName;
		this.useLastName = useLastName;
		this.useIsActive = useIsActive;
		this.useDateCreated = useDateCreated;
		this.useDateUpdated = useDateUpdated;
		this.useDataSourceKey = useDataSourceKey;
		this.useUpdatedBy = useUpdatedBy;
		this.useExistsInLdap = useExistsInLdap;
	}

	public MasUserImpl(final String useUserKey, final String useSsn, final String useUcdlogin, final String useUserName, final String usePassword,
			final String useEmployeeId, final String useFirstName, final String useMiddleName, final String useLastName, final String useMothraFname,
			final String useMothraMname, final String useMothraLname, final String useMothraId, final String useDisplayTitle,
			final String useUserTitleKey, final Integer useUserPrefixKey, final Integer useUserSuffixKey, final String useDepartmentKey,
			final String useInstitutionKey, final String usePhone, final String useFax, final String useEmail, final LocalDateTime useMothraVerifyDate,
			final boolean useIsActive, final String useNotes, final Boolean useIsPasswordReset, final LocalDateTime useDateCreated, final LocalDateTime useDateUpdated,
			final String useDataSourceKey, final String useUpdatedBy, final boolean useExistsInLdap) {
		this.useUserKey = useUserKey;
		this.useSsn = useSsn;
		this.useUcdlogin = useUcdlogin;
		this.useUserName = useUserName;
		this.usePassword = usePassword;
		this.useEmployeeId = useEmployeeId;
		this.useFirstName = useFirstName;
		this.useMiddleName = useMiddleName;
		this.useLastName = useLastName;
		this.useMothraFname = useMothraFname;
		this.useMothraMname = useMothraMname;
		this.useMothraLname = useMothraLname;
		this.useMothraId = useMothraId;
		this.useDisplayTitle = useDisplayTitle;
		this.useUserTitleKey = useUserTitleKey;
		this.useUserPrefixKey = useUserPrefixKey;
		this.useUserSuffixKey = useUserSuffixKey;
		this.useDepartmentKey = useDepartmentKey;
		this.useInstitutionKey = useInstitutionKey;
		this.usePhone = usePhone;
		this.useFax = useFax;
		this.useEmail = useEmail;
		this.useMothraVerifyDate = useMothraVerifyDate;
		this.useIsActive = useIsActive;
		this.useNotes = useNotes;
		this.useIsPasswordReset = useIsPasswordReset;
		this.useDateCreated = useDateCreated;
		this.useDateUpdated = useDateUpdated;
		this.useDataSourceKey = useDataSourceKey;
		this.useUpdatedBy = useUpdatedBy;
		this.useExistsInLdap = useExistsInLdap;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#getUseUserKey()
	 */
	@Override
	@Id
	@Column(name = "use_userKey", unique = true, nullable = false, length = 35,columnDefinition="char")
	public String getUseUserKey() {
		return this.useUserKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUseUserKey(java.lang.String)
	 */
	@Override
	public void setUseUserKey(final String useUserKey) {
		this.useUserKey = useUserKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#getUseSsn()
	 */
	@Override
	@Column(name = "use_SSN", length = 10, columnDefinition="char")
	public String getUseSsn() {
		return this.useSsn;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUseSsn(java.lang.String)
	 */
	@Override
	public void setUseSsn(final String useSsn) {
		this.useSsn = useSsn;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#getUseUcdlogin()
	 */
	@Override
	@Column(name = "use_UCDLogin", unique = true, length = 20, columnDefinition="char")
	public String getUseUcdlogin() {
		return this.useUcdlogin;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUseUcdlogin(java.lang.String)
	 */
	@Override
	public void setUseUcdlogin(final String useUcdlogin) {
		this.useUcdlogin = useUcdlogin;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#getUseUserName()
	 */
	@Override
	@Column(name = "use_userName", length = 50)
	public String getUseUserName() {
		return this.useUserName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUseUserName(java.lang.String)
	 */
	@Override
	public void setUseUserName(final String useUserName) {
		this.useUserName = useUserName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#getUsePassword()
	 */
	@Override
	@Column(name = "use_password", length = 25)
	public String getUsePassword() {
		return this.usePassword;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUsePassword(java.lang.String)
	 */
	@Override
	public void setUsePassword(final String usePassword) {
		this.usePassword = usePassword;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#getUseEmployeeId()
	 */
	@Override
	@Column(name = "use_employeeID", unique = true, length = 10, columnDefinition="char")
	public String getUseEmployeeId() {
		return this.useEmployeeId;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUseEmployeeId(java.lang.String)
	 */
	@Override
	public void setUseEmployeeId(final String useEmployeeId) {
		this.useEmployeeId = useEmployeeId;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#getUseFirstName()
	 */
	@Override
	@Column(name = "use_firstName", nullable = false, length = 50)
	public String getUseFirstName() {
		return this.useFirstName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUseFirstName(java.lang.String)
	 */
	@Override
	public void setUseFirstName(final String useFirstName) {
		this.useFirstName = useFirstName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#getUseMiddleName()
	 */
	@Override
	@Column(name = "use_middleName", length = 50)
	public String getUseMiddleName() {
		return this.useMiddleName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUseMiddleName(java.lang.String)
	 */
	@Override
	public void setUseMiddleName(final String useMiddleName) {
		this.useMiddleName = useMiddleName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#getUseLastName()
	 */
	@Override
	@Column(name = "use_lastName", nullable = false, length = 50)
	public String getUseLastName() {
		return this.useLastName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUseLastName(java.lang.String)
	 */
	@Override
	public void setUseLastName(final String useLastName) {
		this.useLastName = useLastName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#getUseMothraFname()
	 */
	@Override
	@Column(name = "use_mothraFName", length = 50)
	public String getUseMothraFname() {
		return this.useMothraFname;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUseMothraFname(java.lang.String)
	 */
	@Override
	public void setUseMothraFname(final String useMothraFname) {
		this.useMothraFname = useMothraFname;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#getUseMothraMname()
	 */
	@Override
	@Column(name = "use_mothraMName", length = 50)
	public String getUseMothraMname() {
		return this.useMothraMname;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUseMothraMname(java.lang.String)
	 */
	@Override
	public void setUseMothraMname(final String useMothraMname) {
		this.useMothraMname = useMothraMname;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#getUseMothraLname()
	 */
	@Override
	@Column(name = "use_mothraLName", length = 50)
	public String getUseMothraLname() {
		return this.useMothraLname;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUseMothraLname(java.lang.String)
	 */
	@Override
	public void setUseMothraLname(final String useMothraLname) {
		this.useMothraLname = useMothraLname;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#getUseMothraId()
	 */
	@Override
	@Column(name = "use_mothraID", unique = true, length = 8, columnDefinition="char")
	public String getUseMothraId() {
		return this.useMothraId;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUseMothraId(java.lang.String)
	 */
	@Override
	public void setUseMothraId(final String useMothraId) {
		this.useMothraId = useMothraId;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#getUseDisplayTitle()
	 */
	@Override
	@Column(name = "use_displayTitle", length = 100)
	public String getUseDisplayTitle() {
		return this.useDisplayTitle;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUseDisplayTitle(java.lang.String)
	 */
	@Override
	public void setUseDisplayTitle(final String useDisplayTitle) {
		this.useDisplayTitle = useDisplayTitle;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#getUseUserTitleKey()
	 */
	@Override
	@Column(name = "use_userTitleKey", length = 10, columnDefinition="char")
	public String getUseUserTitleKey() {
		return this.useUserTitleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUseUserTitleKey(java.lang.String)
	 */
	@Override
	public void setUseUserTitleKey(final String useUserTitleKey) {
		this.useUserTitleKey = useUserTitleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#getUseUserPrefixKey()
	 */
	@Override
	@Column(name = "use_userPrefixKey")
	public Integer getUseUserPrefixKey() {
		return this.useUserPrefixKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUseUserPrefixKey(java.lang.Integer)
	 */
	@Override
	public void setUseUserPrefixKey(final Integer useUserPrefixKey) {
		this.useUserPrefixKey = useUserPrefixKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#getUseUserSuffixKey()
	 */
	@Override
	@Column(name = "use_userSuffixKey")
	public Integer getUseUserSuffixKey() {
		return this.useUserSuffixKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUseUserSuffixKey(java.lang.Integer)
	 */
	@Override
	public void setUseUserSuffixKey(final Integer useUserSuffixKey) {
		this.useUserSuffixKey = useUserSuffixKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#getUseDepartmentKey()
	 */
	@Override
	@Column(name = "use_departmentKey", length = 10, columnDefinition="char")
	public String getUseDepartmentKey() {
		return this.useDepartmentKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUseDepartmentKey(java.lang.String)
	 */
	@Override
	public void setUseDepartmentKey(final String useDepartmentKey) {
		this.useDepartmentKey = useDepartmentKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#getUseInstitutionKey()
	 */
	@Override
	@Column(name = "use_institutionKey", length = 10, columnDefinition="char")
	public String getUseInstitutionKey() {
		return this.useInstitutionKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUseInstitutionKey(java.lang.String)
	 */
	@Override
	public void setUseInstitutionKey(final String useInstitutionKey) {
		this.useInstitutionKey = useInstitutionKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#getUsePhone()
	 */
	@Override
	@Column(name = "use_phone", length = 16)
	public String getUsePhone() {
		return this.usePhone;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUsePhone(java.lang.String)
	 */
	@Override
	public void setUsePhone(final String usePhone) {
		this.usePhone = usePhone;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#getUseFax()
	 */
	@Override
	@Column(name = "use_fax", length = 16)
	public String getUseFax() {
		return this.useFax;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUseFax(java.lang.String)
	 */
	@Override
	public void setUseFax(final String useFax) {
		this.useFax = useFax;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#getUseEmail()
	 */
	@Override
	@Column(name = "use_email", unique = true, length = 300)
	public String getUseEmail() {
		return this.useEmail;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUseEmail(java.lang.String)
	 */
	@Override
	public void setUseEmail(final String useEmail) {
		this.useEmail = useEmail;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#getUseMothraVerifyDate()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "use_mothraVerifyDate", length = 23)
	public LocalDateTime getUseMothraVerifyDate() {
		return this.useMothraVerifyDate;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUseMothraVerifyDate(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setUseMothraVerifyDate(final LocalDateTime useMothraVerifyDate) {
		this.useMothraVerifyDate = useMothraVerifyDate;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#isUseIsActive()
	 */
	@Override
	@Column(name = "use_isActive", nullable = false)
	public boolean isUseIsActive() {
		return this.useIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUseIsActive(boolean)
	 */
	@Override
	public void setUseIsActive(final boolean useIsActive) {
		this.useIsActive = useIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#getUseNotes()
	 */
	@Override
	@Column(name = "use_notes", columnDefinition="text")
	public String getUseNotes() {
		return this.useNotes;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUseNotes(java.lang.String)
	 */
	@Override
	public void setUseNotes(final String useNotes) {
		this.useNotes = useNotes;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#getUseIsPasswordReset()
	 */
	@Override
	@Column(name = "use_isPasswordReset")
	public Boolean getUseIsPasswordReset() {
		return this.useIsPasswordReset;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUseIsPasswordReset(java.lang.Boolean)
	 */
	@Override
	public void setUseIsPasswordReset(final Boolean useIsPasswordReset) {
		this.useIsPasswordReset = useIsPasswordReset;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#getUseDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "use_dateCreated", nullable = false, length = 23)
	public LocalDateTime getUseDateCreated() {
		return this.useDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUseDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setUseDateCreated(final LocalDateTime useDateCreated) {
		this.useDateCreated = useDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#getUseDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "use_dateUpdated", nullable = false, length = 23)
	public LocalDateTime getUseDateUpdated() {
		return this.useDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUseDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setUseDateUpdated(final LocalDateTime useDateUpdated) {
		this.useDateUpdated = useDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#getUseDataSourceKey()
	 */
	@Override
	@Column(name = "use_dataSourceKey", nullable = false, length = 3,columnDefinition="char")
	public String getUseDataSourceKey() {
		return this.useDataSourceKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUseDataSourceKey(java.lang.String)
	 */
	@Override
	public void setUseDataSourceKey(final String useDataSourceKey) {
		this.useDataSourceKey = useDataSourceKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#getUseUpdatedBy()
	 */
	@Override
	@Column(name = "use_updatedBy", nullable = false, length = 35, columnDefinition="char")
	public String getUseUpdatedBy() {
		return this.useUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUseUpdatedBy(java.lang.String)
	 */
	@Override
	public void setUseUpdatedBy(final String useUpdatedBy) {
		this.useUpdatedBy = useUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#isUseExistsInLdap()
	 */
	@Override
	@Column(name = "use_existsInLdap", nullable = false)
	public boolean isUseExistsInLdap() {
		return this.useExistsInLdap;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.MasUser#setUseExistsInLdap(boolean)
	 */
	@Override
	public void setUseExistsInLdap(final boolean useExistsInLdap) {
		this.useExistsInLdap = useExistsInLdap;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((useDataSourceKey == null) ? 0 : useDataSourceKey.hashCode());
		result = prime * result + ((useDateCreated == null) ? 0 : useDateCreated.hashCode());
		result = prime * result + ((useDateUpdated == null) ? 0 : useDateUpdated.hashCode());
		result = prime * result + ((useDepartmentKey == null) ? 0 : useDepartmentKey.hashCode());
		result = prime * result + ((useDisplayTitle == null) ? 0 : useDisplayTitle.hashCode());
		result = prime * result + ((useEmail == null) ? 0 : useEmail.hashCode());
		result = prime * result + ((useEmployeeId == null) ? 0 : useEmployeeId.hashCode());
		result = prime * result + (useExistsInLdap ? 1231 : 1237);
		result = prime * result + ((useFax == null) ? 0 : useFax.hashCode());
		result = prime * result + ((useFirstName == null) ? 0 : useFirstName.hashCode());
		result = prime * result + ((useInstitutionKey == null) ? 0 : useInstitutionKey.hashCode());
		result = prime * result + (useIsActive ? 1231 : 1237);
		result = prime * result + ((useIsPasswordReset == null) ? 0 : useIsPasswordReset.hashCode());
		result = prime * result + ((useLastName == null) ? 0 : useLastName.hashCode());
		result = prime * result + ((useMiddleName == null) ? 0 : useMiddleName.hashCode());
		result = prime * result + ((useMothraFname == null) ? 0 : useMothraFname.hashCode());
		result = prime * result + ((useMothraId == null) ? 0 : useMothraId.hashCode());
		result = prime * result + ((useMothraLname == null) ? 0 : useMothraLname.hashCode());
		result = prime * result + ((useMothraMname == null) ? 0 : useMothraMname.hashCode());
		result = prime * result + ((useMothraVerifyDate == null) ? 0 : useMothraVerifyDate.hashCode());
		result = prime * result + ((useNotes == null) ? 0 : useNotes.hashCode());
		result = prime * result + ((usePassword == null) ? 0 : usePassword.hashCode());
		result = prime * result + ((usePhone == null) ? 0 : usePhone.hashCode());
		result = prime * result + ((useSsn == null) ? 0 : useSsn.hashCode());
		result = prime * result + ((useUcdlogin == null) ? 0 : useUcdlogin.hashCode());
		result = prime * result + ((useUpdatedBy == null) ? 0 : useUpdatedBy.hashCode());
		result = prime * result + ((useUserKey == null) ? 0 : useUserKey.hashCode());
		result = prime * result + ((useUserName == null) ? 0 : useUserName.hashCode());
		result = prime * result + ((useUserPrefixKey == null) ? 0 : useUserPrefixKey.hashCode());
		result = prime * result + ((useUserSuffixKey == null) ? 0 : useUserSuffixKey.hashCode());
		result = prime * result + ((useUserTitleKey == null) ? 0 : useUserTitleKey.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof MasUserImpl))
			return false;
		final MasUserImpl other = (MasUserImpl) obj;
		if (useDataSourceKey == null) {
			if (other.useDataSourceKey != null)
				return false;
		} else if (!useDataSourceKey.equals(other.useDataSourceKey))
			return false;
		if (useDateCreated == null) {
			if (other.useDateCreated != null)
				return false;
		} else if (!useDateCreated.equals(other.useDateCreated))
			return false;
		if (useDateUpdated == null) {
			if (other.useDateUpdated != null)
				return false;
		} else if (!useDateUpdated.equals(other.useDateUpdated))
			return false;
		if (useDepartmentKey == null) {
			if (other.useDepartmentKey != null)
				return false;
		} else if (!useDepartmentKey.equals(other.useDepartmentKey))
			return false;
		if (useDisplayTitle == null) {
			if (other.useDisplayTitle != null)
				return false;
		} else if (!useDisplayTitle.equals(other.useDisplayTitle))
			return false;
		if (useEmail == null) {
			if (other.useEmail != null)
				return false;
		} else if (!useEmail.equals(other.useEmail))
			return false;
		if (useEmployeeId == null) {
			if (other.useEmployeeId != null)
				return false;
		} else if (!useEmployeeId.equals(other.useEmployeeId))
			return false;
		if (useExistsInLdap != other.useExistsInLdap)
			return false;
		if (useFax == null) {
			if (other.useFax != null)
				return false;
		} else if (!useFax.equals(other.useFax))
			return false;
		if (useFirstName == null) {
			if (other.useFirstName != null)
				return false;
		} else if (!useFirstName.equals(other.useFirstName))
			return false;
		if (useInstitutionKey == null) {
			if (other.useInstitutionKey != null)
				return false;
		} else if (!useInstitutionKey.equals(other.useInstitutionKey))
			return false;
		if (useIsActive != other.useIsActive)
			return false;
		if (useIsPasswordReset == null) {
			if (other.useIsPasswordReset != null)
				return false;
		} else if (!useIsPasswordReset.equals(other.useIsPasswordReset))
			return false;
		if (useLastName == null) {
			if (other.useLastName != null)
				return false;
		} else if (!useLastName.equals(other.useLastName))
			return false;
		if (useMiddleName == null) {
			if (other.useMiddleName != null)
				return false;
		} else if (!useMiddleName.equals(other.useMiddleName))
			return false;
		if (useMothraFname == null) {
			if (other.useMothraFname != null)
				return false;
		} else if (!useMothraFname.equals(other.useMothraFname))
			return false;
		if (useMothraId == null) {
			if (other.useMothraId != null)
				return false;
		} else if (!useMothraId.equals(other.useMothraId))
			return false;
		if (useMothraLname == null) {
			if (other.useMothraLname != null)
				return false;
		} else if (!useMothraLname.equals(other.useMothraLname))
			return false;
		if (useMothraMname == null) {
			if (other.useMothraMname != null)
				return false;
		} else if (!useMothraMname.equals(other.useMothraMname))
			return false;
		if (useMothraVerifyDate == null) {
			if (other.useMothraVerifyDate != null)
				return false;
		} else if (!useMothraVerifyDate.equals(other.useMothraVerifyDate))
			return false;
		if (useNotes == null) {
			if (other.useNotes != null)
				return false;
		} else if (!useNotes.equals(other.useNotes))
			return false;
		if (usePassword == null) {
			if (other.usePassword != null)
				return false;
		} else if (!usePassword.equals(other.usePassword))
			return false;
		if (usePhone == null) {
			if (other.usePhone != null)
				return false;
		} else if (!usePhone.equals(other.usePhone))
			return false;
		if (useSsn == null) {
			if (other.useSsn != null)
				return false;
		} else if (!useSsn.equals(other.useSsn))
			return false;
		if (useUcdlogin == null) {
			if (other.useUcdlogin != null)
				return false;
		} else if (!useUcdlogin.equals(other.useUcdlogin))
			return false;
		if (useUpdatedBy == null) {
			if (other.useUpdatedBy != null)
				return false;
		} else if (!useUpdatedBy.equals(other.useUpdatedBy))
			return false;
		if (useUserKey == null) {
			if (other.useUserKey != null)
				return false;
		} else if (!useUserKey.equals(other.useUserKey))
			return false;
		if (useUserName == null) {
			if (other.useUserName != null)
				return false;
		} else if (!useUserName.equals(other.useUserName))
			return false;
		if (useUserPrefixKey == null) {
			if (other.useUserPrefixKey != null)
				return false;
		} else if (!useUserPrefixKey.equals(other.useUserPrefixKey))
			return false;
		if (useUserSuffixKey == null) {
			if (other.useUserSuffixKey != null)
				return false;
		} else if (!useUserSuffixKey.equals(other.useUserSuffixKey))
			return false;
		if (useUserTitleKey == null) {
			if (other.useUserTitleKey != null)
				return false;
		} else if (!useUserTitleKey.equals(other.useUserTitleKey))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"MasUsers [useUserKey=%s, useSsn=%s, useUcdlogin=%s, useUserName=%s, usePassword=%s, useEmployeeId=%s, useFirstName=%s, useMiddleName=%s, useLastName=%s, useMothraFname=%s, useMothraMname=%s, useMothraLname=%s, useMothraId=%s, useDisplayTitle=%s, useUserTitleKey=%s, useUserPrefixKey=%s, useUserSuffixKey=%s, useDepartmentKey=%s, useInstitutionKey=%s, usePhone=%s, useFax=%s, useEmail=%s, useMothraVerifyDate=%s, useIsActive=%s, useNotes=%s, useIsPasswordReset=%s, useDateCreated=%s, useDateUpdated=%s, useDataSourceKey=%s, useUpdatedBy=%s, useExistsInLdap=%s]",
				useUserKey, useSsn, useUcdlogin, useUserName, usePassword, useEmployeeId, useFirstName, useMiddleName,
				useLastName, useMothraFname, useMothraMname, useMothraLname, useMothraId, useDisplayTitle,
				useUserTitleKey, useUserPrefixKey, useUserSuffixKey, useDepartmentKey, useInstitutionKey, usePhone,
				useFax, useEmail, useMothraVerifyDate, useIsActive, useNotes, useIsPasswordReset, useDateCreated,
				useDateUpdated, useDataSourceKey, useUpdatedBy, useExistsInLdap);
	}



	public MasUserDTO toDTO() {
		return new MasUserDTO(this);
	}

}
