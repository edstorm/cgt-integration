package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtSubContractors;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtSubContractorsDTO;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_SubContractors")
public class CgtSubContractorsImpl implements CgtSubContractors {

	private static final long serialVersionUID = -1208683208000537449L;
	private String scoSubContractorKey;
	private String scoName;
	private Boolean scoIsActive;
	private String scoUpdatedBy;
	private LocalDateTime scoDateUpdated;
	private LocalDateTime scoDateCreated;

	public CgtSubContractorsImpl() {
	}

	public CgtSubContractorsImpl(final String scoSubContractorKey, final String scoName) {
		this.scoSubContractorKey = scoSubContractorKey;
		this.scoName = scoName;
	}

	public CgtSubContractorsImpl(final String scoSubContractorKey, final String scoName, final Boolean scoIsActive, final String scoUpdatedBy,
			final LocalDateTime scoDateUpdated, final LocalDateTime scoDateCreated) {
		this.scoSubContractorKey = scoSubContractorKey;
		this.scoName = scoName;
		this.scoIsActive = scoIsActive;
		this.scoUpdatedBy = scoUpdatedBy;
		this.scoDateUpdated = scoDateUpdated;
		this.scoDateCreated = scoDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContractors#getScoSubContractorKey()
	 */
	@Override
	@Id
	@Column(name = "sco_subContractorKey", unique = true, nullable = false, length = 10)
	public String getScoSubContractorKey() {
		return this.scoSubContractorKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContractors#setScoSubContractorKey(java.lang.String)
	 */
	@Override
	public void setScoSubContractorKey(final String scoSubContractorKey) {
		this.scoSubContractorKey = scoSubContractorKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContractors#getScoName()
	 */
	@Override
	@Column(name = "sco_name", nullable = false)
	public String getScoName() {
		return this.scoName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContractors#setScoName(java.lang.String)
	 */
	@Override
	public void setScoName(final String scoName) {
		this.scoName = scoName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContractors#getScoIsActive()
	 */
	@Override
	@Column(name = "sco_isActive")
	public Boolean getScoIsActive() {
		return this.scoIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContractors#setScoIsActive(java.lang.Boolean)
	 */
	@Override
	public void setScoIsActive(final Boolean scoIsActive) {
		this.scoIsActive = scoIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContractors#getScoUpdatedBy()
	 */
	@Override
	@Column(name = "sco_updatedBy", length = 35, columnDefinition="char")
	public String getScoUpdatedBy() {
		return this.scoUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContractors#setScoUpdatedBy(java.lang.String)
	 */
	@Override
	public void setScoUpdatedBy(final String scoUpdatedBy) {
		this.scoUpdatedBy = scoUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContractors#getScoDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "sco_dateUpdated", length = 23)
	public LocalDateTime getScoDateUpdated() {
		return this.scoDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContractors#setScoDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setScoDateUpdated(final LocalDateTime scoDateUpdated) {
		this.scoDateUpdated = scoDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContractors#getScoDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "sco_dateCreated", length = 23)
	public LocalDateTime getScoDateCreated() {
		return this.scoDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSubContractors#setScoDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setScoDateCreated(final LocalDateTime scoDateCreated) {
		this.scoDateCreated = scoDateCreated;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((scoDateCreated == null) ? 0 : scoDateCreated.hashCode());
		result = prime * result + ((scoDateUpdated == null) ? 0 : scoDateUpdated.hashCode());
		result = prime * result + ((scoIsActive == null) ? 0 : scoIsActive.hashCode());
		result = prime * result + ((scoName == null) ? 0 : scoName.hashCode());
		result = prime * result + ((scoSubContractorKey == null) ? 0 : scoSubContractorKey.hashCode());
		result = prime * result + ((scoUpdatedBy == null) ? 0 : scoUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtSubContractorsImpl)) {
			return false;
		}
		CgtSubContractorsImpl other = (CgtSubContractorsImpl) obj;
		if (scoDateCreated == null) {
			if (other.scoDateCreated != null) {
				return false;
			}
		} else if (!scoDateCreated.equals(other.scoDateCreated)) {
			return false;
		}
		if (scoDateUpdated == null) {
			if (other.scoDateUpdated != null) {
				return false;
			}
		} else if (!scoDateUpdated.equals(other.scoDateUpdated)) {
			return false;
		}
		if (scoIsActive == null) {
			if (other.scoIsActive != null) {
				return false;
			}
		} else if (!scoIsActive.equals(other.scoIsActive)) {
			return false;
		}
		if (scoName == null) {
			if (other.scoName != null) {
				return false;
			}
		} else if (!scoName.equals(other.scoName)) {
			return false;
		}
		if (scoSubContractorKey == null) {
			if (other.scoSubContractorKey != null) {
				return false;
			}
		} else if (!scoSubContractorKey.equals(other.scoSubContractorKey)) {
			return false;
		}
		if (scoUpdatedBy == null) {
			if (other.scoUpdatedBy != null) {
				return false;
			}
		} else if (!scoUpdatedBy.equals(other.scoUpdatedBy)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtSubContractorsImpl [scoSubContractorKey=%s, scoName=%s, scoIsActive=%s, scoUpdatedBy=%s, scoDateUpdated=%s, scoDateCreated=%s]",
				scoSubContractorKey, scoName, scoIsActive, scoUpdatedBy, scoDateUpdated, scoDateCreated);
	}



	public CgtSubContractorsDTO toDTO() {
		return new CgtSubContractorsDTO(this);
	}

}
