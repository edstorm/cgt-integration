package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtBaseRateTypes;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtBaseRateTypesDTO;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_BaseRateTypes")
public class CgtBaseRateTypesImpl implements CgtBaseRateTypes {

	private static final long serialVersionUID = 3518263820179820015L;

	private int brtBaseRateTypeKey;
	private String brtName;
	private String brtShortName;
	private Integer brtSortOrder;
	private boolean brtIsActive;
	private String brtUpdatedBy;
	private LocalDateTime brtDateCreated;
	private LocalDateTime brtDateUpdated;

	public CgtBaseRateTypesImpl() {
	}

	public CgtBaseRateTypesImpl(final int brtBaseRateTypeKey, final String brtName, final boolean brtIsActive, final String brtUpdatedBy,
			final LocalDateTime brtDateCreated, final LocalDateTime brtDateUpdated) {
		this.brtBaseRateTypeKey = brtBaseRateTypeKey;
		this.brtName = brtName;
		this.brtIsActive = brtIsActive;
		this.brtUpdatedBy = brtUpdatedBy;
		this.brtDateCreated = brtDateCreated;
		this.brtDateUpdated = brtDateUpdated;
	}

	public CgtBaseRateTypesImpl(final int brtBaseRateTypeKey, final String brtName, final String brtShortName, final Integer brtSortOrder,
			final boolean brtIsActive, final String brtUpdatedBy, final LocalDateTime brtDateCreated, final LocalDateTime brtDateUpdated) {
		this.brtBaseRateTypeKey = brtBaseRateTypeKey;
		this.brtName = brtName;
		this.brtShortName = brtShortName;
		this.brtSortOrder = brtSortOrder;
		this.brtIsActive = brtIsActive;
		this.brtUpdatedBy = brtUpdatedBy;
		this.brtDateCreated = brtDateCreated;
		this.brtDateUpdated = brtDateUpdated;
	}
	
	public CgtBaseRateTypesImpl(CgtBaseRateTypes fromObj) {
		this.brtBaseRateTypeKey = fromObj.getBrtBaseRateTypeKey();
		this.brtName = fromObj.getBrtName();
		this.brtShortName = fromObj.getBrtShortName();
		this.brtSortOrder = fromObj.getBrtSortOrder();
		this.brtIsActive = fromObj.isBrtIsActive();
		this.brtUpdatedBy = fromObj.getBrtUpdatedBy();
		this.brtDateCreated = fromObj.getBrtDateCreated();
		this.brtDateUpdated = fromObj.getBrtDateUpdated();
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtBaseRateTypesInterface#getBrtBaseRateTypeKey()
	 */
	@Override
	
	@Id
	@Column(name = "brt_baseRateTypeKey", unique = true, nullable = false)
	public int getBrtBaseRateTypeKey() {
		return this.brtBaseRateTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtBaseRateTypesInterface#setBrtBaseRateTypeKey(int)
	 */
	@Override
	public void setBrtBaseRateTypeKey(final int brtBaseRateTypeKey) {
		this.brtBaseRateTypeKey = brtBaseRateTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtBaseRateTypesInterface#getBrtName()
	 */
	@Override
	@Column(name = "brt_name", nullable = false)
	public String getBrtName() {
		return this.brtName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtBaseRateTypesInterface#setBrtName(java.lang.String)
	 */
	@Override
	public void setBrtName(final String brtName) {
		this.brtName = brtName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtBaseRateTypesInterface#getBrtShortName()
	 */
	@Override
	@Column(name = "brt_shortName")
	public String getBrtShortName() {
		return this.brtShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtBaseRateTypesInterface#setBrtShortName(java.lang.String)
	 */
	@Override
	public void setBrtShortName(final String brtShortName) {
		this.brtShortName = brtShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtBaseRateTypesInterface#getBrtSortOrder()
	 */
	@Override
	@Column(name = "brt_sortOrder")
	public Integer getBrtSortOrder() {
		return this.brtSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtBaseRateTypesInterface#setBrtSortOrder(java.lang.Integer)
	 */
	@Override
	public void setBrtSortOrder(final Integer brtSortOrder) {
		this.brtSortOrder = brtSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtBaseRateTypesInterface#isBrtIsActive()
	 */
	@Override
	@Column(name = "brt_isActive", nullable = false)
	public boolean isBrtIsActive() {
		return this.brtIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtBaseRateTypesInterface#setBrtIsActive(boolean)
	 */
	@Override
	public void setBrtIsActive(final boolean brtIsActive) {
		this.brtIsActive = brtIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtBaseRateTypesInterface#getBrtUpdatedBy()
	 */
	@Override
	@Column(name = "brt_updatedBy", nullable = false, length = 35, columnDefinition="char")
	public String getBrtUpdatedBy() {
		return this.brtUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtBaseRateTypesInterface#setBrtUpdatedBy(java.lang.String)
	 */
	@Override
	public void setBrtUpdatedBy(final String brtUpdatedBy) {
		this.brtUpdatedBy = brtUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtBaseRateTypesInterface#getBrtDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "brt_dateCreated", nullable = false, length = 23)
	public LocalDateTime getBrtDateCreated() {
		return this.brtDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtBaseRateTypesInterface#setBrtDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setBrtDateCreated(final LocalDateTime brtDateCreated) {
		this.brtDateCreated = brtDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtBaseRateTypesInterface#getBrtDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "brt_dateUpdated", nullable = false, length = 23)
	public LocalDateTime getBrtDateUpdated() {
		return this.brtDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtBaseRateTypesInterface#setBrtDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setBrtDateUpdated(final LocalDateTime brtDateUpdated) {
		this.brtDateUpdated = brtDateUpdated;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + brtBaseRateTypeKey;
		result = prime * result + ((brtDateCreated == null) ? 0 : brtDateCreated.hashCode());
		result = prime * result + ((brtDateUpdated == null) ? 0 : brtDateUpdated.hashCode());
		result = prime * result + (brtIsActive ? 1231 : 1237);
		result = prime * result + ((brtName == null) ? 0 : brtName.hashCode());
		result = prime * result + ((brtShortName == null) ? 0 : brtShortName.hashCode());
		result = prime * result + ((brtSortOrder == null) ? 0 : brtSortOrder.hashCode());
		result = prime * result + ((brtUpdatedBy == null) ? 0 : brtUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof CgtBaseRateTypesImpl))
			return false;
		final CgtBaseRateTypesImpl other = (CgtBaseRateTypesImpl) obj;
		if (brtBaseRateTypeKey != other.brtBaseRateTypeKey)
			return false;
		if (brtDateCreated == null) {
			if (other.brtDateCreated != null)
				return false;
		} else if (!brtDateCreated.equals(other.brtDateCreated))
			return false;
		if (brtDateUpdated == null) {
			if (other.brtDateUpdated != null)
				return false;
		} else if (!brtDateUpdated.equals(other.brtDateUpdated))
			return false;
		if (brtIsActive != other.brtIsActive)
			return false;
		if (brtName == null) {
			if (other.brtName != null)
				return false;
		} else if (!brtName.equals(other.brtName))
			return false;
		if (brtShortName == null) {
			if (other.brtShortName != null)
				return false;
		} else if (!brtShortName.equals(other.brtShortName))
			return false;
		if (brtSortOrder == null) {
			if (other.brtSortOrder != null)
				return false;
		} else if (!brtSortOrder.equals(other.brtSortOrder))
			return false;
		if (brtUpdatedBy == null) {
			if (other.brtUpdatedBy != null)
				return false;
		} else if (!brtUpdatedBy.equals(other.brtUpdatedBy))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtBaseRateTypes [brtBaseRateTypeKey=%s, brtName=%s, brtShortName=%s, brtSortOrder=%s, brtIsActive=%s, brtUpdatedBy=%s, brtDateCreated=%s, brtDateUpdated=%s]",
				brtBaseRateTypeKey, brtName, brtShortName, brtSortOrder, brtIsActive, brtUpdatedBy, brtDateCreated,
				brtDateUpdated);
	}

	

	public CgtBaseRateTypesDTO toDTO() {
		return new CgtBaseRateTypesDTO(this);
	}
}
