package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtSponsorRoleType;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtSponsorRoleTypeDTO;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_SponsorRoleTypes")
public class CgtSponsorRoleTypeImpl implements CgtSponsorRoleType {

	private static final long serialVersionUID = 6654490044012200167L;

	private int srtSponsorRoleTypeKey;
	private String srtName;
	private String srtShortName;
	private Integer srtSortOrder;
	private boolean srtIsActive;
	private String srtUpdatedBy;
	private LocalDateTime srtDateCreated;
	private LocalDateTime srtDateUpdated;

	public CgtSponsorRoleTypeImpl() {
	}

	public CgtSponsorRoleTypeImpl(final int srtSponsorRoleTypeKey, final String srtName, final boolean srtIsActive, final String srtUpdatedBy,
			final LocalDateTime srtDateCreated, final LocalDateTime srtDateUpdated) {
		this.srtSponsorRoleTypeKey = srtSponsorRoleTypeKey;
		this.srtName = srtName;
		this.srtIsActive = srtIsActive;
		this.srtUpdatedBy = srtUpdatedBy;
		this.srtDateCreated = srtDateCreated;
		this.srtDateUpdated = srtDateUpdated;
	}

	public CgtSponsorRoleTypeImpl(final int srtSponsorRoleTypeKey, final String srtName, final String srtShortName, final Integer srtSortOrder,
			final boolean srtIsActive, final String srtUpdatedBy, final LocalDateTime srtDateCreated, final LocalDateTime srtDateUpdated) {
		this.srtSponsorRoleTypeKey = srtSponsorRoleTypeKey;
		this.srtName = srtName;
		this.srtShortName = srtShortName;
		this.srtSortOrder = srtSortOrder;
		this.srtIsActive = srtIsActive;
		this.srtUpdatedBy = srtUpdatedBy;
		this.srtDateCreated = srtDateCreated;
		this.srtDateUpdated = srtDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.SponosrRole#getSrtSponsorRoleTypeKey()
	 */
	@Override
	@Id
	@Column(name = "srt_sponsorroletypekey", unique = true, nullable = false)
	public int getSrtSponsorRoleTypeKey() {
		return this.srtSponsorRoleTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.SponosrRole#setSrtSponsorRoleTypeKey(int)
	 */
	@Override
	public void setSrtSponsorRoleTypeKey(final int srtSponsorRoleTypeKey) {
		this.srtSponsorRoleTypeKey = srtSponsorRoleTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.SponosrRole#getSrtName()
	 */
	@Override
	@Column(name = "srt_name", nullable = false)
	public String getSrtName() {
		return this.srtName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.SponosrRole#setSrtName(java.lang.String)
	 */
	@Override
	public void setSrtName(final String srtName) {
		this.srtName = srtName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.SponosrRole#getSrtShortName()
	 */
	@Override
	@Column(name = "srt_shortName")
	public String getSrtShortName() {
		return this.srtShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.SponosrRole#setSrtShortName(java.lang.String)
	 */
	@Override
	public void setSrtShortName(final String srtShortName) {
		this.srtShortName = srtShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.SponosrRole#getSrtSortOrder()
	 */
	@Override
	@Column(name = "srt_sortOrder")
	public Integer getSrtSortOrder() {
		return this.srtSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.SponosrRole#setSrtSortOrder(java.lang.Integer)
	 */
	@Override
	public void setSrtSortOrder(final Integer srtSortOrder) {
		this.srtSortOrder = srtSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.SponosrRole#isSrtIsActive()
	 */
	@Override
	@Column(name = "srt_isActive", nullable = false)
	public boolean isSrtIsActive() {
		return this.srtIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.SponosrRole#setSrtIsActive(boolean)
	 */
	@Override
	public void setSrtIsActive(final boolean srtIsActive) {
		this.srtIsActive = srtIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.SponosrRole#getSrtUpdatedBy()
	 */
	@Override
	@Column(name = "srt_updatedBy", nullable = false, length = 35, columnDefinition="char")
	public String getSrtUpdatedBy() {
		return this.srtUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.SponosrRole#setSrtUpdatedBy(java.lang.String)
	 */
	@Override
	public void setSrtUpdatedBy(final String srtUpdatedBy) {
		this.srtUpdatedBy = srtUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.SponosrRole#getSrtDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "srt_dateCreated", nullable = false, length = 23)
	public LocalDateTime getSrtDateCreated() {
		return this.srtDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.SponosrRole#setSrtDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setSrtDateCreated(final LocalDateTime srtDateCreated) {
		this.srtDateCreated = srtDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.SponosrRole#getSrtDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "srt_dateUpdated", nullable = false, length = 23)
	public LocalDateTime getSrtDateUpdated() {
		return this.srtDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.SponosrRole#setSrtDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setSrtDateUpdated(final LocalDateTime srtDateUpdated) {
		this.srtDateUpdated = srtDateUpdated;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((srtDateCreated == null) ? 0 : srtDateCreated.hashCode());
		result = prime * result + ((srtDateUpdated == null) ? 0 : srtDateUpdated.hashCode());
		result = prime * result + (srtIsActive ? 1231 : 1237);
		result = prime * result + ((srtName == null) ? 0 : srtName.hashCode());
		result = prime * result + ((srtShortName == null) ? 0 : srtShortName.hashCode());
		result = prime * result + ((srtSortOrder == null) ? 0 : srtSortOrder.hashCode());
		result = prime * result + srtSponsorRoleTypeKey;
		result = prime * result + ((srtUpdatedBy == null) ? 0 : srtUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtSponsorRoleTypeImpl)) {
			return false;
		}
		CgtSponsorRoleTypeImpl other = (CgtSponsorRoleTypeImpl) obj;
		if (srtDateCreated == null) {
			if (other.srtDateCreated != null) {
				return false;
			}
		} else if (!srtDateCreated.equals(other.srtDateCreated)) {
			return false;
		}
		if (srtDateUpdated == null) {
			if (other.srtDateUpdated != null) {
				return false;
			}
		} else if (!srtDateUpdated.equals(other.srtDateUpdated)) {
			return false;
		}
		if (srtIsActive != other.srtIsActive) {
			return false;
		}
		if (srtName == null) {
			if (other.srtName != null) {
				return false;
			}
		} else if (!srtName.equals(other.srtName)) {
			return false;
		}
		if (srtShortName == null) {
			if (other.srtShortName != null) {
				return false;
			}
		} else if (!srtShortName.equals(other.srtShortName)) {
			return false;
		}
		if (srtSortOrder == null) {
			if (other.srtSortOrder != null) {
				return false;
			}
		} else if (!srtSortOrder.equals(other.srtSortOrder)) {
			return false;
		}
		if (srtSponsorRoleTypeKey != other.srtSponsorRoleTypeKey) {
			return false;
		}
		if (srtUpdatedBy == null) {
			if (other.srtUpdatedBy != null) {
				return false;
			}
		} else if (!srtUpdatedBy.equals(other.srtUpdatedBy)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtSponsorRoleType [srtSponsorRoleTypeKey=%s, srtName=%s, srtShortName=%s, srtSortOrder=%s, srtIsActive=%s, srtUpdatedBy=%s, srtDateCreated=%s, srtDateUpdated=%s]",
				srtSponsorRoleTypeKey, srtName, srtShortName, srtSortOrder, srtIsActive, srtUpdatedBy, srtDateCreated,
				srtDateUpdated);
	}

	



	public CgtSponsorRoleTypeDTO toDTO() {
		return new CgtSponsorRoleTypeDTO(this);
	}

}
