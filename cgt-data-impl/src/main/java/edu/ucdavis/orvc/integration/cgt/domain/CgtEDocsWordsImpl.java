package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEDocsWords;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtEDocsWordsDTO;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_eDocsWords")
public class CgtEDocsWordsImpl implements CgtEDocsWords {

	private static final long serialVersionUID = -7333954682982661190L;
	private int wrdId;
	private int wrdFileKey;
	private String wrdWord;

	public CgtEDocsWordsImpl() {
	}

	public CgtEDocsWordsImpl(final int wrdId, final int wrdFileKey, final String wrdWord) {
		this.wrdId = wrdId;
		this.wrdFileKey = wrdFileKey;
		this.wrdWord = wrdWord;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsWords#getWrdId()
	 */
	@Override
	@Id
	@Column(name = "wrd_id", unique = true, nullable = false)
	public int getWrdId() {
		return this.wrdId;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsWords#setWrdId(int)
	 */
	@Override
	public void setWrdId(final int wrdId) {
		this.wrdId = wrdId;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsWords#getWrdFileKey()
	 */
	@Override
	@Column(name = "wrd_fileKey", nullable = false)
	public int getWrdFileKey() {
		return this.wrdFileKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsWords#setWrdFileKey(int)
	 */
	@Override
	public void setWrdFileKey(final int wrdFileKey) {
		this.wrdFileKey = wrdFileKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsWords#getWrdWord()
	 */
	@Override
	@Column(name = "wrd_word", nullable = false, length = 200)
	public String getWrdWord() {
		return this.wrdWord;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtEDocsWords#setWrdWord(java.lang.String)
	 */
	@Override
	public void setWrdWord(final String wrdWord) {
		this.wrdWord = wrdWord;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + wrdFileKey;
		result = prime * result + wrdId;
		result = prime * result + ((wrdWord == null) ? 0 : wrdWord.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtEDocsWordsImpl)) {
			return false;
		}
		CgtEDocsWordsImpl other = (CgtEDocsWordsImpl) obj;
		if (wrdFileKey != other.wrdFileKey) {
			return false;
		}
		if (wrdId != other.wrdId) {
			return false;
		}
		if (wrdWord == null) {
			if (other.wrdWord != null) {
				return false;
			}
		} else if (!wrdWord.equals(other.wrdWord)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("CgtEDocsWordsImpl [wrdId=%s, wrdFileKey=%s, wrdWord=%s]", wrdId, wrdFileKey, wrdWord);
	}

	public CgtEDocsWordsDTO toDTO() {
		return new CgtEDocsWordsDTO(this);
	}
}
