package edu.ucdavis.orvc.integration.cgt.service.impl;

import java.io.File;
import java.io.IOException;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;

import edu.ucdavis.orvc.integration.cgt.api.domain.service.PdfFileService;

/**
 * @author edstorm
 * Service provides general methods related to file cgt edocs files records
 * and the files themseleves. 
 *
 */
@Service("pdfFileService")
public class PdfFileServiceImpl implements PdfFileService, InitializingBean {

	protected Logger log = LoggerFactory.getLogger(PdfFileServiceImpl.class);
	private static final String PATH_FORMAT = "%s%s";
	
	//@Value("${pdfFileService.rootDirectory}")
	private String rootDirectory;
	private String systemLocalRootPath;
	
	//@Value("${pdfFileService.copyRootDirectory}")
	private String copyRootDirectory;
	private String systemLocalCopyRootDirectory;
	
	private boolean stdSepNeedsTrans = true;
	
	private static final String STD_FILE_SEPARATOR = "/";
	
	public void init() {
		log.info("STD: Service Started");																																																																																																																																																																																																																																								
		log.info("Configured root directory: {}",rootDirectory);
		log.info("Configured copy root directory: {}", copyRootDirectory);
		
		stdSepNeedsTrans = !StringUtils.equals(File.separator, STD_FILE_SEPARATOR);
		systemLocalRootPath = translatePathSeparators(rootDirectory);
		systemLocalCopyRootDirectory = translatePathSeparators(copyRootDirectory);
		log.info("Config:root directory: {}", systemLocalRootPath);
		log.info("Config:copy root directory: {}", systemLocalCopyRootDirectory);
		log.info("Paths need translation: {}", stdSepNeedsTrans);
		if (stdSepNeedsTrans) {
			log.info("File separators will be translated from '{}' to '{}'", STD_FILE_SEPARATOR, File.separator);
		}
		log.info("STD: INIT COMPLETE");
	}
	
	
	protected String determineSeparator(String input) {
		final String result;
		if (StringUtils.contains(input, File.separator) 
				&& !StringUtils.contains(input, STD_FILE_SEPARATOR)) {
			result = File.separator;
		} else if (StringUtils.contains(input, STD_FILE_SEPARATOR) 
				&& !StringUtils.contains(input, File.separator) ) {
			result = STD_FILE_SEPARATOR;
		} else if ( !(StringUtils.contains(input, STD_FILE_SEPARATOR) 
				|| StringUtils.contains(input, File.separator))) {
			result = null;
		} else {
			//in this case the string contains both separators. 
			//we are going to error out.
			throw new IllegalStateException(
					String.format("Path '%s' contains multiple separator characters.", input)
					);
		}
		return result;
	}
	
	/**
	 * This service expects that all paths specified use the forward slash as
	 * the path separator.   It then translates those paths into paths for the
	 * runtime environment as needed. 
	 * 
	 * @param inputString input path using forward slash for path separator
	 * @return path using the path seperators for the local file system.
	 */
	protected String translatePathSeparators(String inputString) {
		final String result;
	
		if (stdSepNeedsTrans && StringUtils.contains(inputString, STD_FILE_SEPARATOR)) {
			result = StringUtils.replace(inputString, STD_FILE_SEPARATOR, File.separator);
		} else {
			result = inputString;
		}
		
		return result;
	}
	
	/**
	 * Trims leading separator (if present) on the given input string. The separator must be the
	 * STD_FILE_SEPARATOR character.
	 * 
	 * @param input
	 * @return
	 */
	
	protected String trimLeadingSep(String input) {
		
		return (input != null)?(
				StringUtils.equals(StringUtils.left(input, 1),STD_FILE_SEPARATOR) ?
				StringUtils.right(input, input.length() - 1)
				:input):null;
	}
	
	/**
	 * Return the full path to the file by prepending the root directory (using correct
	 * system path separator.  This function will also remove leading separator from the relPath
	 * value.
	 * 
	 * @param relPath the path to the file relative to the root directory. 
	 * @return The full path to the file.
	 */
	protected String fullFilePath(final String relPath) {
		Assert.notNull(relPath,"Path must not be null!");
		return String.format(PATH_FORMAT, systemLocalRootPath, translatePathSeparators(trimLeadingSep(relPath)));
	}
	
	protected String copyFullFilePath(final String relPath) {
		Assert.notNull(relPath,"Path must not be null!");
		return String.format(PATH_FORMAT, systemLocalCopyRootDirectory, translatePathSeparators(relPath));
	}
	
	protected PdfReader openPdfReader(final String relPath) throws IOException {
		final String pathToFile = fullFilePath(relPath);
		log.debug("Attempting to open PdfReader on file (full path): {}", pathToFile);
		final PdfReader reader = new PdfReader(pathToFile);
		return reader;
	}
	
	
	
	protected boolean validateRelativeDestPath(String relCopyToPath) {
		final String sepStr = determineSeparator(relCopyToPath);
		
		final String relPath = StringUtils.left(relCopyToPath
											    ,StringUtils.lastIndexOf(relCopyToPath, sepStr)
											    );
		final String fullPath = copyFullFilePath(relPath);
		File f = new File(fullPath);
		f.mkdirs();
		return true;	
	}
	
	protected PdfWriter openPdfWriter(final String relCopyToPath) throws IOException {
		validateRelativeDestPath(relCopyToPath);
		final String pathToFile = copyFullFilePath(relCopyToPath);
		final PdfWriter writer = new PdfWriter(pathToFile);
		return writer;
	}
	
	protected PdfDocument openPdfDocument(PdfReader reader) throws IOException {
		final PdfDocument doc = new PdfDocument(reader);
		return doc;
	}
	
	protected PdfDocument openPdfDocument(String relPath) throws IOException {
		log.debug("Attempting to open PDF file: {}", relPath);
		final PdfReader reader = openPdfReader(relPath);
		final PdfDocument doc = openPdfDocument(reader);
		return doc;
	}
	
	protected void validateSubdirs(String pathToFile) {
		//final String 
	}
	
	protected PdfDocument openStampingModeDoc(final String relPathToSource) throws IOException {
		log.debug("Attempting to open {} as PdfDocument."
				,relPathToSource);

		final PdfReader sourceReader = openPdfReader(relPathToSource);
		final PdfDocument destDocument = new PdfDocument(sourceReader);
		return destDocument;
	}
	
	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.service.impl.PdfFileService#getNumberOfPages(java.lang.String)
	 */
	@Override
	public int getNumberOfPages(final String relPathToSource) {
		final int numberOfPages;
		final PdfDocument document;

		try {
			document = openStampingModeDoc(relPathToSource);
			numberOfPages = document.getNumberOfPages();
			return numberOfPages;
		} catch (IOException e1) {
			throw new RuntimeException(
					String.format("Exception thrown opening %s to count pages: %s", relPathToSource, e1.getMessage()));
		}
	}
		
	
	protected void closePdfDocument(final PdfDocument pdfDocument
								   ,final PdfReader pdfReader) {
		Assert.notNull(pdfDocument,"PdfDocument must not be null!");
	
		//Assert.notNull(pdfReader);
		pdfDocument.close();
		if (pdfReader != null) {
			try {
				pdfReader.close();
			} catch (IOException e) {
				log.warn("IOException while closing PdfReader!",e);
			}
		}
		
	}
	
	
	
	//GETTER AND SETTER LAND
	
	
	public void setSystemLocalRootPath(String systemLocalRootPath) {
		this.systemLocalRootPath = systemLocalRootPath;
	}

	public void setRootDirectory(String rootDirectory) {
		this.rootDirectory = rootDirectory;
	}

	public void setCopyRootDirectory(String copyRootDirectory) {
		this.copyRootDirectory = copyRootDirectory;
	}

	public void setSystemLocalCopyRootDirectory(String systemLocalCopyRootDirectory) {
		this.systemLocalCopyRootDirectory = systemLocalCopyRootDirectory;
	}


	@Override
	public void afterPropertiesSet() throws Exception {
		init();
	}
	
}
