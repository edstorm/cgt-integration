package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtInstrumentType;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtInstrumentTypeDTO;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_InstrumentTypes")
public class CgtInstrumentTypeImpl implements CgtInstrumentType {

	private static final long serialVersionUID = -1943450384768171840L;
	private int ityInstrumentTypeKey;
	private String ityName;
	private String ityShortName;
	private Integer itySortOrder;
	private boolean ityIsActive;
	private String ityUpdatedBy;
	private LocalDateTime ityDateUpdated;
	private LocalDateTime ityDateCreated;

	public CgtInstrumentTypeImpl() {
	}

	public CgtInstrumentTypeImpl(final int ityInstrumentTypeKey, final String ityName, final boolean ityIsActive, final String ityUpdatedBy,
			final LocalDateTime ityDateUpdated, final LocalDateTime ityDateCreated) {
		this.ityInstrumentTypeKey = ityInstrumentTypeKey;
		this.ityName = ityName;
		this.ityIsActive = ityIsActive;
		this.ityUpdatedBy = ityUpdatedBy;
		this.ityDateUpdated = ityDateUpdated;
		this.ityDateCreated = ityDateCreated;
	}

	public CgtInstrumentTypeImpl(final int ityInstrumentTypeKey, final String ityName, final String ityShortName, final Integer itySortOrder,
			final boolean ityIsActive, final String ityUpdatedBy, final LocalDateTime ityDateUpdated, final LocalDateTime ityDateCreated) {
		this.ityInstrumentTypeKey = ityInstrumentTypeKey;
		this.ityName = ityName;
		this.ityShortName = ityShortName;
		this.itySortOrder = itySortOrder;
		this.ityIsActive = ityIsActive;
		this.ityUpdatedBy = ityUpdatedBy;
		this.ityDateUpdated = ityDateUpdated;
		this.ityDateCreated = ityDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtInstrumentType#getItyInstrumentTypeKey()
	 */
	@Override
	@Id
	@Column(name = "ity_instrumentTypeKey", unique = true, nullable = false)
	public int getItyInstrumentTypeKey() {
		return this.ityInstrumentTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtInstrumentType#setItyInstrumentTypeKey(int)
	 */
	@Override
	public void setItyInstrumentTypeKey(final int ityInstrumentTypeKey) {
		this.ityInstrumentTypeKey = ityInstrumentTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtInstrumentType#getItyName()
	 */
	@Override
	@Column(name = "ity_name", nullable = false)
	public String getItyName() {
		return this.ityName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtInstrumentType#setItyName(java.lang.String)
	 */
	@Override
	public void setItyName(final String ityName) {
		this.ityName = ityName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtInstrumentType#getItyShortName()
	 */
	@Override
	@Column(name = "ity_shortName")
	public String getItyShortName() {
		return this.ityShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtInstrumentType#setItyShortName(java.lang.String)
	 */
	@Override
	public void setItyShortName(final String ityShortName) {
		this.ityShortName = ityShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtInstrumentType#getItySortOrder()
	 */
	@Override
	@Column(name = "ity_sortOrder")
	public Integer getItySortOrder() {
		return this.itySortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtInstrumentType#setItySortOrder(java.lang.Integer)
	 */
	@Override
	public void setItySortOrder(final Integer itySortOrder) {
		this.itySortOrder = itySortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtInstrumentType#isItyIsActive()
	 */
	@Override
	@Column(name = "ity_isActive", nullable = false)
	public boolean isItyIsActive() {
		return this.ityIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtInstrumentType#setItyIsActive(boolean)
	 */
	@Override
	public void setItyIsActive(final boolean ityIsActive) {
		this.ityIsActive = ityIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtInstrumentType#getItyUpdatedBy()
	 */
	@Override
	@Column(name = "ity_updatedBy", nullable = false, length = 35, columnDefinition="char")
	public String getItyUpdatedBy() {
		return this.ityUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtInstrumentType#setItyUpdatedBy(java.lang.String)
	 */
	@Override
	public void setItyUpdatedBy(final String ityUpdatedBy) {
		this.ityUpdatedBy = ityUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtInstrumentType#getItyDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "ity_dateUpdated", nullable = false, length = 23)
	public LocalDateTime getItyDateUpdated() {
		return this.ityDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtInstrumentType#setItyDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setItyDateUpdated(final LocalDateTime ityDateUpdated) {
		this.ityDateUpdated = ityDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtInstrumentType#getItyDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "ity_dateCreated", nullable = false, length = 23)
	public LocalDateTime getItyDateCreated() {
		return this.ityDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtInstrumentType#setItyDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setItyDateCreated(final LocalDateTime ityDateCreated) {
		this.ityDateCreated = ityDateCreated;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtInstrumentType [ityInstrumentTypeKey=%s, ityName=%s, ityShortName=%s, itySortOrder=%s, ityIsActive=%s, ityUpdatedBy=%s, ityDateUpdated=%s, ityDateCreated=%s]",
				ityInstrumentTypeKey, ityName, ityShortName, itySortOrder, ityIsActive, ityUpdatedBy, ityDateUpdated,
				ityDateCreated);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((ityDateCreated == null) ? 0 : ityDateCreated.hashCode());
		result = prime * result + ((ityDateUpdated == null) ? 0 : ityDateUpdated.hashCode());
		result = prime * result + ityInstrumentTypeKey;
		result = prime * result + (ityIsActive ? 1231 : 1237);
		result = prime * result + ((ityName == null) ? 0 : ityName.hashCode());
		result = prime * result + ((ityShortName == null) ? 0 : ityShortName.hashCode());
		result = prime * result + ((itySortOrder == null) ? 0 : itySortOrder.hashCode());
		result = prime * result + ((ityUpdatedBy == null) ? 0 : ityUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtInstrumentTypeImpl)) {
			return false;
		}
		CgtInstrumentTypeImpl other = (CgtInstrumentTypeImpl) obj;
		if (ityDateCreated == null) {
			if (other.ityDateCreated != null) {
				return false;
			}
		} else if (!ityDateCreated.equals(other.ityDateCreated)) {
			return false;
		}
		if (ityDateUpdated == null) {
			if (other.ityDateUpdated != null) {
				return false;
			}
		} else if (!ityDateUpdated.equals(other.ityDateUpdated)) {
			return false;
		}
		if (ityInstrumentTypeKey != other.ityInstrumentTypeKey) {
			return false;
		}
		if (ityIsActive != other.ityIsActive) {
			return false;
		}
		if (ityName == null) {
			if (other.ityName != null) {
				return false;
			}
		} else if (!ityName.equals(other.ityName)) {
			return false;
		}
		if (ityShortName == null) {
			if (other.ityShortName != null) {
				return false;
			}
		} else if (!ityShortName.equals(other.ityShortName)) {
			return false;
		}
		if (itySortOrder == null) {
			if (other.itySortOrder != null) {
				return false;
			}
		} else if (!itySortOrder.equals(other.itySortOrder)) {
			return false;
		}
		if (ityUpdatedBy == null) {
			if (other.ityUpdatedBy != null) {
				return false;
			}
		} else if (!ityUpdatedBy.equals(other.ityUpdatedBy)) {
			return false;
		}
		return true;
	}
	


	public CgtInstrumentTypeDTO toDTO() {
		return new CgtInstrumentTypeDTO(this);
	}

}
