package edu.ucdavis.orvc.integration.cgt.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEDocsFiles;
import edu.ucdavis.orvc.integration.cgt.api.domain.service.CgtEDocsFileService;
import edu.ucdavis.orvc.integration.cgt.api.domain.service.CgtFilesDao;

/**
 * @author edstorm
 * Service provides general methods related to file cgt edocs files records
 * and the files themseleves. 
 *
 */
@Service("cgtEDocsFileService")
@Transactional
public class CgtEDocsFileServiceImpl implements CgtEDocsFileService {
	
	protected Logger log = LoggerFactory.getLogger(CgtEDocsFileServiceImpl.class);
	
	@Autowired
	@Qualifier("cgtFilesDao")
	protected CgtFilesDao cgtFilesDao;
	
	@PostConstruct
	public void init() {
		log.info("STD:SERVICE STARTED.");
	}
	
	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.service.impl.CgtEDocsFileService#getByExtension(java.lang.String)
	 */
	@Override
	public List<CgtEDocsFiles> getByExtension(final String fileExtension) {
		final List<CgtEDocsFiles> result;
		result = cgtFilesDao.getDTOByCriteria(fileExtension, null,null);
		return result;
	}
	
	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.service.impl.CgtEDocsFileService#getByExtension(java.lang.String, org.joda.time.LocalDateTime)
	 */
	@Override
	public List<CgtEDocsFiles> getByExtension(final String fileExtension
											, final LocalDateTime createdBefore ) {
		final List<CgtEDocsFiles> result;
		result = cgtFilesDao.getDTOByCriteria(fileExtension, createdBefore,null);
		return result;
	}
	
	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.service.impl.CgtEDocsFileService#getByExtension(java.lang.String, org.joda.time.LocalDateTime)
	 */
	@Override
	public List<CgtEDocsFiles> getByExtension(final String fileExtension
											, final LocalDateTime createdAfter
											, final LocalDateTime createdBefore) {
		final List<CgtEDocsFiles> result;
		result = cgtFilesDao.getDTOByCriteria(fileExtension, createdAfter,createdBefore);
		return result;
	}
	
	
	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.service.impl.CgtEDocsFileService#getByExtension(java.lang.String, java.util.Date)
	 */
	@Override
	public List<CgtEDocsFiles> getByExtension(final String fileExtension
											, final Date createdBefore ) {
		final List<CgtEDocsFiles> result;
		final LocalDateTime pCreatedBefore;
		pCreatedBefore = createdBefore == null ? null : LocalDateTime.fromDateFields(createdBefore);
		result = cgtFilesDao.getDTOByCriteria(fileExtension, pCreatedBefore,null);
		return result;
	}
	
	@Override
	public CgtEDocsFiles getByKey(Integer fileKey) {
		final CgtEDocsFiles result;
		result = cgtFilesDao.findByKey(fileKey);
		return result;
	}
	
	
}
