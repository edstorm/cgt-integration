#FOR UNIVERSITY OF CALIFORNIA INTERNAL USE ONLY. FURTHER DISTRIBUTION IS NOT PERMITTED

#This configuraiton contains the default properties. Y 
#classpath file pps-mq-processor.std.properties and are commented out here. Uncomment and set to the
#Values desired to change the db servers or message servers and queues as needed.


# ------------------------------------------------------------
# Database and Hibernate Configuration for CGT.
# DEFAULT CONNECTION IS TO:KCSQLDEV.UCDAVIS.EDU:1433/CGT_Dev
# 
	cgt.jdbc.driver=net.sourceforge.jtds.jdbc.Driver
	cgt.jdbc.url.server=or-mssql-dev01
	cgt.jdbc.url.port=1433
	cgt.jdbc.url.database=OVC_DEV
	cgt.jdbc.user=
	cgt.jdbc.password=

# JDBC conneciton pool configuration. This is the standard set in the std config file loaded before this one.
#	cgt.c3p0.acquireIncrement=1	
#	cgt.c3p0.minPoolSize=1
#	cgt.c3p0.maxPoolSize=${cgt.taskExec.concurrencyLimit}
#	cgt.c3p0.maxIdleTime=1600

# Standard Hibernate configuration details
# The only one you might need to change is the schema (if it is different name than elk's)
#	cgt.hibernate.dialect=org.hibernate.dialect.SQLServer2008Dialect
#	cgt.hibernate.default_schema=DBO
#	cgt.hibernate.default_catalog=${cgt.jdbc.url.database}
#	cgt.hibernate.show_sql=false
#	cgt.hibernate.hbm2ddl.auto=none
#	cgt.hibernate.jadira.usertype.databaseZone=America/Los_Angeles
#	cgt.hibernate.cache.region.factory_class=org.hibernate.cache.ehcache.EhCacheRegionFactory
#	cgt.hibernate.cache.use_second_level_cache=true
#	cgt.hibernate.cache.use_query_cache=true

# ------------------------------------------------------------
# Concurrency Control
#
# The number of active service processors and 
# The number of request per processor can be set here
# Basic set up contained in std loaded by all profiles
# Tweaking may need to occur here depending on load.
	
#	cgt.taskExec.threadGroupName=CGT-Processor-Threads
#	cgt.taskExec.threadNamePrefix=cgt-processor
#	cgt.taskExec.concurrencyLimit=10
#
#	cgt.rabbit.listener-container.max-concurrency=${cgt.taskExec.concurrencyLimit}
#	cgt.rabbit.listener-container.concurrency=1
#
#	cgt.rabbit.listener-container.min-start-interval=500
#	cgt.rabbit.listener-container.prefetch=1

# Concurrency for JSON service listeners.

#	cgt.taskExec.json.threadGroupName=CGT-Processor-Threads-JSON
#	cgt.taskExec.json.threadNamePrefix=cgt-processor-json
#	cgt.taskExec.json.concurrencyLimit=10
#
#	cgt.rabbit.listener-container.json.max-concurrency=${cgt.taskExec.concurrencyLimit}
#	cgt.rabbit.listener-container.json.concurrency=${cgt.rabbit.listener-container.concurrency}
#
#	cgt.rabbit.listener-container.json.min-start-interval=${cgt.rabbit.listener-container.min-start-interval}
#	cgt.rabbit.listener-container.json.prefetch=${cgt.rabbit.listener-container.prefetch}
	
# ------------------------------------------------------------
# Service configuration
# ------------------------------------------------------------


# PDF File Service properties
# You must set the root to where the files have been located.
#	
# Root directory, should include trailing '/'
# This should ALWAYS use '/', the service will change the seperator 
# to the correct character on start up if nexessary.  If you do not use
# '/' as the seperator here then the service will not be able to update it
# to the correct seperator.
	pdfFileService.rootDirectory=c:/edstorm/cgt_files/
#
# Directory to be used for copying files to, or opening target PDF files to.
# should end with a '/' character.
	pdfFileService.copyRootDirectory=c:/edstorm/cgt_files_target/

# Rabbit - connection details.
# The user and password properties must be set in the override file. 

#	cgt.message.server.host=kctech.ucdavis.edu
#	cgt.message.server.port=5672
#	cgt.message.server.virtual-host=EDSTORM-DEV-DATA.SERVICES
#	cgt.message.server.ssl=false
#	cgt.message.server.requestedHeartbeat=60
#	cgt.message.server.exchange=ORVC.DATA-SERVICES

	cgt.message.server.password=
	cgt.message.server.user=

# ------------------------------------------------------------
# Rabbit - Service Queues and Routing Keys
#
# Names of queues and routing keys for the various services. 
# These are the deafauls, but you can override them if you like.  
# Keep in mind the clients need to know the routing key of the service
# in order to use it, they do not need to know the queue name.  If routing keys
# are changed, and no services are available by using the default key then the 
# clients will need to be updated to use the new key. 


#	cgtServices.cgaFeedTransactionInfoService.name=orvc.remoting.cgt.efaFeedTransactionInfoService
#	cgtServices.cgaFeedTransactionInfoService.routingKey=orvc.remoting.cgt.efaFeedTransactionInfoService
#	cgtServices.cgtProjectInfoService.name=orvc.remoting.cgt.cgtProjectInfoService
#	cgtServices.cgtProjectInfoService.routingKey=orvc.remoting.cgt.cgtProjectInfoService
#
#	cgtServices.cgaFeedTransactionInfoService.json.name=orvc.remoting.cgt.json.efaFeedTransactionInfoService
#	cgtServices.cgaFeedTransactionInfoService.json.routingKey=orvc.remoting.cgt.json.efaFeedTransactionInfoService
#	cgtServices.cgtProjectInfoService.json.name=orvc.remoting.cgt.json.cgtProjectInfoService
#	cgtServices.cgtProjectInfoService.json.routingKey=orvc.remoting.cgt.json.cgtProjectInfoService


# ------------------------------------------------------------
# Zookeeper - Service configuration publishing
# 
# Properties for how to connect to ZK.  The baspath is the same 
# for all of our different packages so all of them will publish their
# connection details as children of the basePath zk node.
#
#	orvc.zk.serverAddress=kctech.ucdavis.edu
#	orvc.zk.serverPort=2181
#	orvc.mqServices.zk.basePath=orvc.mq.services
# 
# Define our 'service name', this will be the node name we use
# to publish our connection info for all of our servces.
#
#	cgt.discoverable.serviceName=cgt.services
#	cgt.discoverable.serviceAddress=${message.server.host}
#	cgt.discoverable.servicePort=${message.server.port}
#	cgt.discoverable.startZkDiscoveryOnBeanInit=true
#	cgt.discoverable.cacheServiceProvidersByName=true
#
# These properties define how a client should connect to our 
# services.  We provide the MQ server information and an account
# to authenticate as.
#
#
#	client.message.server.host=${cgt.message.server.host}
#	client.message.server.port=${cgt.message.server.port}
#	client.message.server.virtual-host=${cgt.message.server.virtual-host}
#	client.message.server.ssl=${cgt.message.server.ssl}
#	client.message.server.exchange=${cgt.message.server.exchange}
#	client.message.server.requestedHeartbeat=${cgt.message.server.requestedHeartbeat}
#	client.message.server.user=${cgt.message.server.user}
#	client.message.server.password=${cgt.message.server.password}

# Routing keys to publish for each of our services

#	cgt.mqServices.cgaFeedTransactionInfoService=${cgtServices.cgaFeedTransactionInfoService.routingKey}
#	cgt.mqServices.cgtProjectInfoService=${orvc.remoting.cgt.cgtProjectInfoService}












