package edu.ucdavis.orvc.integration.cgt;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.opencsv.CSVWriter;

import asg.cliche.Command;
import asg.cliche.Param;
import asg.cliche.Shell;
import asg.cliche.ShellDependent;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEDocsFiles;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtEfaTransactionProcessInfo;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtProject;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTransaction;
import edu.ucdavis.orvc.integration.cgt.api.domain.service.CgtEDocsFileService;
import edu.ucdavis.orvc.integration.cgt.api.domain.service.CgtFeedTransactionInfoService;
import edu.ucdavis.orvc.integration.cgt.api.domain.service.CgtProjectInfoService;
import edu.ucdavis.orvc.integration.cgt.api.domain.service.PdfFileService;
import edu.ucdavis.orvc.integration.cgt.service.impl.CgaFeedTransactionInfoServiceImpl;
import edu.ucdavis.orvc.integration.cgt.service.impl.PdfFileServiceImpl;


public class CommandProcessor implements ShellDependent {

	@SuppressWarnings("unused")
	private Shell shell;
	
	protected CgtProjectInfoService cgtProjectInfoService;
	protected CgtFeedTransactionInfoService cgaFeedTransactionInfoService;
	protected CgtEDocsFileService cgtEDocsFileService;
	protected PdfFileService pdfFileService;
	
	protected Logger log = LoggerFactory.getLogger(CommandProcessor.class);
	
	private static final String DATETIME_FORMAT_STRING = "dd-MM-yyyy";

	protected static Map<String,List<CgtEDocsFiles>> SAVED_RESULTS_MAP;

	
	public List<CgtEDocsFiles> getSavedResults(String resultName) {
		return SAVED_RESULTS_MAP.get(resultName);
	}
	
	public List<CgtEDocsFiles> renmoveSavedResults(String resultName) {
		List<CgtEDocsFiles> results =  SAVED_RESULTS_MAP.get(resultName);
		SAVED_RESULTS_MAP.remove(resultName);
		return results;
	}
	
	public void saveResults(String resultsName, List<CgtEDocsFiles> results) {
		SAVED_RESULTS_MAP.put(resultsName, results);
	}
	
	
	
	
	@SuppressWarnings("unused")
	private static final DateTimeFormatter DT_FORMATTER;
	
	static {
		DT_FORMATTER = DateTimeFormat.forPattern(DATETIME_FORMAT_STRING);
		SAVED_RESULTS_MAP = new HashMap<String, List<CgtEDocsFiles>>();
	}
	
	public CommandProcessor(Shell shell) {
		this();
		this.shell = shell;
	}

	public CommandProcessor(Shell shell, CgtProjectInfoService cgtProjectInfoService,
			CgaFeedTransactionInfoServiceImpl cgaFeedTransactionInfoService) {
		this();
		this.shell = shell;
		this.cgtProjectInfoService = cgtProjectInfoService;
		this.cgaFeedTransactionInfoService = cgaFeedTransactionInfoService;
	}

	public CommandProcessor() {
		super();
	}
	
	
	/**
	 * Parse the string input into LocalDateTime using the DATETIME_FORMAT_STRING 
	 * as the format.  Method will re-throw the IllegalArgument excepiton as a RuntimeException
	 * if the parsing fails because of formatting. 
	 * 
	 * @param input The string to parse, should be in dd-MM-yyyy format.
	 * @return LocalDateTime result from parsing. 
	 */
	private LocalDateTime parseStringToLocalDateTime(String input) {
		final LocalDateTime result;
		
		try {
			result = StringUtils.isEmpty(input) ?
							null
							:DT_FORMATTER.parseLocalDateTime(input);
		} catch (IllegalArgumentException iae) {
			log.error("Illegal Argument - {} cannot be parsed using format {}",input, DATETIME_FORMAT_STRING);
			throw new RuntimeException(String.format("Could not parse %s using format %s",input,DATETIME_FORMAT_STRING),iae);
		}
		
		return result;
	}
	

	protected StringBuffer outputEDocsFile(CgtEDocsFiles file, StringBuffer buffer) {
		StringBuffer resultBuffer = buffer == null?new StringBuffer():buffer;
		return resultBuffer.append(String.format("%s->%s: extenstion/type = %s, %sKB, path=%s\n"
				, file.getFilFileKey(),file.getFilFileName()
				, file.getFilExtension(), file.getFilFileSize()/1024
				, file.getFilFilePath()));
	}
	
	/**
	 * Generate a StringBuffer containing CgtEdocsFile record details, one record per line.
	 * Summary information about the files is also generated. 
	 * 
	 * @param fileList the list of file records to be output to the list.
	 * @return
	 */
	protected StringBuffer outputEDocsFiles(List<CgtEDocsFiles> fileList, StringBuffer buffer) {
		
		StringBuffer resultBuffer = buffer ==  null?new StringBuffer():buffer;
		
		resultBuffer.append("******** START FILE LIST ****************\n");
		Long ttlKB = new Long(0);
		Long ttlCount = new Long(0);
		for (CgtEDocsFiles file : fileList) {
			outputEDocsFile(file,resultBuffer);
			ttlKB += file.getFilFileSize()/1024;
			ttlCount++;
		}
		
		resultBuffer.append("******** END FILE LIST ****************\n");
		resultBuffer.append(String.format("Total number found: %s\n", ttlCount));
		resultBuffer.append(String.format("Total KB of all files: %sKB\n", ttlKB));
	
		return resultBuffer;
		
	}
	

	/**
	 * @param projectNumber
	 * @return
	 * @see edu.ucdavis.orvc.integration.cgt.api.domain.service.CgtProjectInfoService#getEfaProcessInfo(java.lang.String)
	 */
	@Command
	public List<CgtEfaTransactionProcessInfo> getEfaProcessInfo(String projectNumber) {
		return cgtProjectInfoService.getEfaProcessInfo(projectNumber);
	}

	/**
	 * @param projectNumber
	 * @return
	 * @see edu.ucdavis.orvc.integration.cgt.api.domain.service.CgtProjectInfoService#getProject(java.lang.String)
	 */
	@Command
	public CgtProject getProject(String projectNumber) {
		return cgtProjectInfoService.getProject(projectNumber);
	}

	/**
	 * @param projectNumber
	 * @param active
	 * @return
	 * @see edu.ucdavis.orvc.integration.cgt.api.domain.service.CgtProjectInfoService#getProject(java.lang.String, boolean)
	 */
	@Command
	public CgtProject getProject(String projectNumber, boolean active) {
		return cgtProjectInfoService.getProject(projectNumber, active);
	}

	/**
	 * @param maxDaysOld
	 * @return
	 * @see edu.ucdavis.orvc.integration.cgt.service.impl.CgaFeedTransactionInfoServiceImpl#getUnprocessedTransactions(int)
	 */
	@Command
	public List<CgtTransaction> getUnprocessedTransactions(int maxDaysOld) {
		return cgaFeedTransactionInfoService.getUnprocessedTransactions(maxDaysOld);
	}

	@Command(abbrev="init-pdf-s")
	public String initPdfs() {
		((PdfFileServiceImpl)pdfFileService).init();
		return "Called init.";
	}

	@Command(abbrev="gls",description="Get list of edocs files of the provided type.",header="Files of provided type:")
	public String getEDocsFiles(String fileExtension) {
		final List<CgtEDocsFiles> result = cgtEDocsFileService.getByExtension(fileExtension); 
		StringBuffer resultBuffer = outputEDocsFiles(result,null);
		return resultBuffer.toString();
	}
	
	@Command(abbrev="gf", description="Get the file record identified by provided key (int)", header = "Finding File...")
	public String getFile(final int key) {
		final StringBuffer resultBuffer = new StringBuffer();
		CgtEDocsFiles file = cgtEDocsFileService.getByKey(key);
		if (file == null) {
			resultBuffer.append(String.format("No file found using key %s", key));
		} else {
			outputEDocsFile(file, resultBuffer);
		}
		return resultBuffer.toString();
	}
	
	@Command(abbrev="fpc", description="Get the number of pages inthe pdf document at the provided path", header="Page counting...")
	public int getPageCount(
				@Param(name = "filePath", description="complete path to the file you want the count for.") 
				String filePath 
				) {
		final int resultCount;
		resultCount = pdfFileService.getNumberOfPages(filePath);
		return resultCount;
	}

	@Command(abbrev="pc", description="Get the number of pages in the pdf document.", header = "Response:")
	public int getPageCount( int key ) {
		final int result;
		CgtEDocsFiles file = cgtEDocsFileService.getByKey(key);
		if (file!=null) {
			log.info("Found file record for {}, path to file is: {}", key, file.getFilFilePath());
			result = pdfFileService.getNumberOfPages(file.getFilFilePath());
		} else {
			log.info("No file found with key={}", key);
			result = -1;
		}
		return result;
	}
	
	
	@Command(abbrev="qForFiles", description = "Query the database for the file records, the results will remain set in a local object to be used in output commands."
			, header = "QRY FILES:")
	public String qryForFiles(final String fileType
							, final String createdFromDateStr
							, final String createdToDateStr
							, final String saveAs) {
		Assert.isTrue(!StringUtils.isEmpty(fileType), "You must provide a fileType!");
		final StringBuffer resultBuffer = new StringBuffer();
		
		
		final LocalDateTime pCreatedFromDt = StringUtils.isEmpty(createdFromDateStr)?null:parseStringToLocalDateTime(createdFromDateStr);
		final LocalDateTime pCreatedToDt = StringUtils.isEmpty(createdToDateStr)?null:parseStringToLocalDateTime(createdToDateStr);
		
		final List<CgtEDocsFiles> results = cgtEDocsFileService
				.getByExtension(fileType, pCreatedFromDt, pCreatedToDt);
		
		resultBuffer.append(
				String.format("Executed find for type %s, with creation date lower bound: %s and creation date upper bound:%s\n"
						, fileType, pCreatedFromDt==null?"NOT SET":pCreatedFromDt.toString("dd-MM-yyyy HH:mm")
								  , pCreatedToDt == null?"NOT SET":pCreatedToDt.toString("dd-MM-yyyy HH:mm")
						)
				);
		
		if (!StringUtils.isEmpty(saveAs)) {
			saveResults(saveAs,results);
			resultBuffer.append(String.format("Found %s files. Results saved as %s\n"
					,results.size(),saveAs));
		} else {
			outputEDocsFiles(results,resultBuffer);
		}
		
		return resultBuffer.toString();
	}

	@Command(abbrev="osfs", description="Output saved list of files, (if a list exists in the saved map with the given name", header="Outputting saved files...")
	public String outputSavedFiles(String saveName) {
		StringBuffer resultBuffer = new StringBuffer();
		List<CgtEDocsFiles> files = getSavedResults(saveName);
		if (files == null) {
			resultBuffer.append(String.format("Could not find result set with given name %s", saveName));
		} else {
			outputEDocsFiles(files, resultBuffer);
		}
		
		return resultBuffer.toString();
	}
	
	@Command(abbrev="out-csv", description="Output CSV file of file details including total number of pages.", header="I LIKE ME!")
	public String outputCsv(@Param(name = "fileListSaveName", description = "The name the list of files was stored as during a prior operation.")
							String fileListSaveName
						   ,@Param(name = "saveFile", description = "The full path to save the csv file as. Should end with .csv")
							String saveFile
							,@Param(name = "includeNumPages", description = "If true the number of pages in the file will be included. EXPENSIVE OP")
							 boolean includeNumPages
							,@Param(name = "confirmDelete", description = "If the saveFile is an existing file and this parm is true then that file will be deleted. Otherwise we will abort.")
							boolean confirmDelete) {
		Assert.isTrue(!StringUtils.isEmpty(fileListSaveName), "You must supply the name of the saved file list to output.");
		Assert.isTrue(!StringUtils.isEmpty(saveFile), "You must supply the name of the csv file to save.");
		
		
		final StringBuffer resultBuffer = new StringBuffer();
		final List<CgtEDocsFiles> files = getSavedResults(fileListSaveName);
		final FileWriter fileWriter;
		
		final File fileCSV = new File(saveFile);
		if (fileCSV.exists()) {
			if (confirmDelete) {
				fileCSV.delete();
				resultBuffer.append(String.format("Deleted existing file %s", saveFile));
			} else {
				resultBuffer.append(String.format("HALT: %s exists, confirmDelete is false, you must confirm deletion to delete the existing file.", saveFile));
			}
		} else {
			String filePath = fileCSV.getAbsolutePath().
   	    	     substring(0,fileCSV.getAbsolutePath().lastIndexOf(File.separator));
			if(filePath.length() > 0) {
				File fp = new File(filePath);
				fp.mkdirs();
				resultBuffer.append(String.format("Created dirs as necessary for path %s", filePath));
			}
		}
		
		try {
			fileWriter = new FileWriter(fileCSV);
		} catch (IOException e) {
			log.error("IOException opening file {} for csv writing: {}", fileCSV.toString(), e.getMessage());
			throw new RuntimeException(
					String.format("IOException opening file %s for csv writing.", fileCSV)
					,e);
		}
		
		final CSVWriter csvWriter = new CSVWriter(fileWriter,'\t');
		csvWriter.writeNext("filFileKey#filFilePath#numPages".split("#"));
		for (CgtEDocsFiles file : files) {
		
			if (file.getFilFileKey().intValue() < 285515) {
				log.info(String.format("processing %s:%s:%s:%s",file.getFilFileKey(),file.getFilFilePath(),file.getFilDateCreated(),file.getFilDateUpdated()));
				int pageCount = getPageCount(file.getFilFilePath());
				csvWriter.writeNext(String.format("%s#%s#%s"
						,file.getFilFileKey()
						,file.getFilFilePath()
						,pageCount
						).split("#"));
				log.info(String.format("finished %s:%s",file.getFilFileKey(),file.getFilFilePath()));
				csvWriter.flushQuietly();
			} else {
				log.info(String.format("SKIP %s:%s, DO NOT HAVE COPY OF FILE", file.getFilFileKey(),file.getFilDateCreated()));
			}
		}
		
		try {
			csvWriter.close();
		} catch (IOException e) {
			log.error("IOException closing file {} for csv writing: {}", saveFile, e.getMessage());
			resultBuffer.append(
					String.format("IOException closing file %s for csv writing:%s", saveFile, e.getMessage())
					);
		}
		
		return resultBuffer.toString();
	}
	
	
	
	@Override
	public void cliSetShell(Shell theShell) {
		this.shell = theShell;
	}

	/**
	 * @return the cgtProjectInfoService
	 */
	public CgtProjectInfoService getCgtProjectInfoService() {
		return cgtProjectInfoService;
	}

	/**
	 * @param cgtProjectInfoService the cgtProjectInfoService to set
	 */
	public void setCgtProjectInfoService(CgtProjectInfoService cgtProjectInfoService) {
		this.cgtProjectInfoService = cgtProjectInfoService;
	}

	/**
	 * @return the cgaFeedTransactionInfoService
	 */
	public CgtFeedTransactionInfoService getCgaFeedTransactionInfoService() {
		return cgaFeedTransactionInfoService;
	}

	/**
	 * @param cgaFeedTransactionInfoService the cgaFeedTransactionInfoService to set
	 */
	public void setCgaFeedTransactionInfoService(CgtFeedTransactionInfoService cgaFeedTransactionInfoService) {
		this.cgaFeedTransactionInfoService = cgaFeedTransactionInfoService;
	}

	/**
	 * @return the cgtEDocsFileService
	 */
	public CgtEDocsFileService getCgtEDocsFileService() {
		return cgtEDocsFileService;
	}

	/**
	 * @param cgtEDocsFileService the cgtEDocsFileService to set
	 */
	public void setCgtEDocsFileService(CgtEDocsFileService cgtEDocsFileService) {
		this.cgtEDocsFileService = cgtEDocsFileService;
	}

	public void setPdfFileService(PdfFileService pdfFileService) {
		this.pdfFileService = pdfFileService;
	}
}
